package me.wattguy.delivery.listeners;

import lombok.Getter;
import me.wattguy.delivery.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * Класс-абстракция слушателя
 * Дата создания: 15:29 04.09.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public abstract class PluginListener implements Listener {

    /**
     * Зарегистрирован ли слушатель в плагине
     */
    @Getter
    private boolean registered = false;

    /**
     * Пустой конструктор
     */
    public PluginListener(){ }

    /**
     * Зарегистрировать слушатель
     */
    public void register(){
        // Если зарегистрирован - уходим
        if (registered)
            return;

        // Регистрируем
        Bukkit.getPluginManager().registerEvents(this, Main.getInstance());

        // Заносим состояние регистрации
        registered = true;
    }

    /**
     * Дерегистрировать слушатель
     */
    public void unregister(){
        // Если не зарегистрирован - уходим
        if (!registered)
            return;

        // Дерегистрируем
        HandlerList.unregisterAll(this);

        // Заносим состояние дерегистрации
        registered = false;
    }

}
