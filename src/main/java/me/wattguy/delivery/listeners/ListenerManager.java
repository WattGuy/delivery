package me.wattguy.delivery.listeners;

import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Класс-менеджер слушателей плагина
 * Дата создания: 15:31 04.09.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class ListenerManager implements Initializable, Unregistrable {

    /**
     * Карта всех слушателей плагина
     * Формат - Map<класс, экземпляр слушателя>
     */
    public static final Map<Class<?>, PluginListener> listeners = new HashMap<>();

    /**
     * Регистрация всех слушателей в плагине
     */
    @Settings(behaviour = Settings.Behaviour.ONCE)
    public void initialize(){
        unregister();

        var l = System.currentTimeMillis();

        Main.getInstance().getLogger().info("Beginning the Listener initialization...");

        // Получаем все слушатели
        for(var c : getAllListeners()){
            // Если уже зарегистрирован - уходим
            if (listeners.containsKey(c))
                continue;

            PluginListener listener = null;

            // Получаем экземпляр класса по пустому конструктору
            try {
                listener = c.getConstructor().newInstance();
            }catch(Exception ignored) { }

            // Если не получилось - уходим
            if (listener == null)
                continue;

            // Регистрируем слушатель
            listener.register();

            Main.getInstance().getLogger().info("  [X] " + Utils.getClassName(c));

            // Кешируем
            listeners.put(c, listener);
        }

        Main.getInstance().getLogger().info("Listener initialization has been completed! (#" + listeners.size() + ") (" + (System.currentTimeMillis() - l) + "ms)");
    }

    /**
     * Дерегистрация всех слушателей
     */
    @Override
    public void unregister(){

        // Перебираем и дерегистрируем
        for (var listener : listeners.values()) {

            // Дерегистрируем
            listener.unregister();

        }

        // Очищаем кэш
        listeners.clear();

    }

    /**
     * Получить все классы-слушатели с помощью рефлексии
     * @return Список всех слушателей-классов
     */
    public static Set<Class<? extends PluginListener>> getAllListeners(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(ListenerManager.class.getPackage().getName(), 1), PluginListener.class);

    }

}
