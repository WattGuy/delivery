package me.wattguy.delivery;

import lombok.*;
import me.wattguy.delivery.configs.list.Messages;
import me.wattguy.delivery.extensions.PlaceholderAPIExtension;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.chat.hover.content.Content;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.apache.commons.lang.NullArgumentException;
import org.bukkit.*;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.block.banner.Pattern;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.lemar98.regionshops.model.Shop;
import ru.lemar98.regionshops.model.ShopItem;

import java.io.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.jar.JarFile;
import java.util.regex.MatchResult;

/**
 * Класс-утилита, содержащий в себе универсальные методы
 * @author WattGuy
 * @version 1.0
 */
public class Utils implements Initializable {

    private static Class<?> craftMagicNumbers = null;
    private static Class<?> itemClazz = null;
    private static Class<?> localeClazz = null;
    private static boolean isPost1dot18 = false;

    @Override
    public void initialize() {
        final String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];

        var hasRepackagedNms = false;

        if (Material.getMaterial("AMETHYST_CLUSTER") != null) {
            // Bukkit version is 1.17+
            hasRepackagedNms = true;
        }

        if (Material.getMaterial("MUSIC_DISC_OTHERSIDE") != null) {
            // Bukkit version is 1.18+ (for NMS Item#getName)
            isPost1dot18 = true;
        }

        try {
            craftMagicNumbers = Class.forName("org.bukkit.craftbukkit.{v}.util.CraftMagicNumbers".replace("{v}", version));
            if (hasRepackagedNms) {
                itemClazz = Class.forName("net.minecraft.world.item.Item");
                localeClazz = Class.forName("net.minecraft.locale.LocaleLanguage");
            } else {
                itemClazz = Class.forName("net.minecraft.server.{v}.Item".replace("{v}", version));
                localeClazz = Class.forName("net.minecraft.server.{v}.LocaleLanguage".replace("{v}", version));
            }
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks whether the server's Bukkit version is below 1.13.
     *
     * @return true if Bukkit version is at 1.12.2 or below
     */
    public boolean isBelow113() {
        return _isBelow113(Bukkit.getServer().getBukkitVersion().split("-")[0]);
    }

    private boolean _isBelow113(final String bukkitVersion) {
        if (bukkitVersion.matches("^[0-9.]+$")) {
            switch(bukkitVersion) {
                case "1.12.2" :
                case "1.12.1" :
                case "1.12" :
                case "1.11.2" :
                case "1.11.1" :
                case "1.11" :
                case "1.10.2" :
                case "1.10.1" :
                case "1.10" :
                case "1.9.4" :
                case "1.9.3" :
                case "1.9.2" :
                case "1.9.1" :
                case "1.9" :
                case "1.8.9" :
                case "1.8.8" :
                case "1.8.7" :
                case "1.8.6" :
                case "1.8.5" :
                case "1.8.4" :
                case "1.8.3" :
                case "1.8.2" :
                case "1.8.1" :
                case "1.8" :
                case "1.7.10" :
                case "1.7.9" :
                case "1.7.2" :
                    return true;
                default:
                    // Bukkit version is 1.13+ or unsupported
                    return false;
            }
        }
        Bukkit.getLogger().severe("RPMDelivery received invalid Bukkit version " + bukkitVersion);
        return false;
    }

    public static String queryMaterial(Material material, ItemMeta meta) throws IllegalArgumentException {
        if (material == null)
            throw new NullArgumentException("[RPMDelivery] Material cannot be null");

        String matKey = "";

        if (material.isBlock() && material.createBlockData() instanceof Ageable) {
            matKey = "block.minecraft." + material.name().toLowerCase();
        } else {
            try {
                final Method m = craftMagicNumbers.getDeclaredMethod("getItem", material.getClass());
                m.setAccessible(true);
                final Object item = m.invoke(craftMagicNumbers, material);

                if (item == null)
                    throw new IllegalArgumentException(material.name() + " material could not be queried!");

                matKey = (String) itemClazz.getMethod(isPost1dot18 ? "a" : "getName").invoke(item);
                if (meta instanceof PotionMeta) {
                    matKey += ".effect." + ((PotionMeta)meta).getBasePotionData().getType().name().toLowerCase()
                            .replace("regen", "regeneration").replace("speed", "swiftness").replace("jump", "leaping")
                            .replace("instant_heal", "healing").replace("instant_damage", "harming");
                }
            } catch (final Exception ex) {
                throw new IllegalArgumentException("[RPMDelivery] Unable to query Material: " + material.name());
            }
        }

        return matKey;
    }

    public static String getTranslateVariable(ItemStack is){
        String key = null;

        try{
            key = queryMaterial(is.getType(), is.getItemMeta());
        }catch(Exception ignored) { }

        if (key == null)
            return is.getType().toString();

        return "{translate|" + key + "}";
    }

    public static int getStoredAmount(Shop shop, ShopItem item){
        try{
            return shop.getStoredAmount(item);
        }catch(Exception ignored) { }

        return 0;
    }

    public static Integer getCustomModelData(ItemStack is){
        if (!is.hasItemMeta())
            return null;

        var meta = is.getItemMeta();

        if (!meta.hasCustomModelData())
            return null;

        return meta.getCustomModelData();
    }

    public static List<String> getLore(ItemStack itemStack){
        List<String> list = new ArrayList<>();

        if (itemStack == null || !itemStack.hasItemMeta())
            return list;

        var meta = itemStack.getItemMeta();

        if (meta.hasLore())
            list.addAll(meta.getLore());

        return list;
    }

    public static String getName(ItemStack itemStack, String def, boolean strip){
        if (itemStack == null || !itemStack.hasItemMeta())
            return def;

        var meta = itemStack.getItemMeta();

        if (meta.hasDisplayName())
            return strip ? ChatColor.stripColor(meta.getDisplayName()) : meta.getDisplayName();

        return def;
    }

    public static void replaceVariable(List<String> lore, String variable, List<String> replacement){
        String line = null;

        for (var s : lore) {
            if (!(s.toLowerCase().contains(variable)))
                continue;

            line = s;
            break;
        }

        var index = line != null ? lore.indexOf(line) : -1;

        if (index != -1){
            lore.remove(index);
            var reversedReplacement = new ArrayList<>(replacement);
            Collections.reverse(reversedReplacement);

            for (var s : reversedReplacement)
                lore.add(index, s);
        }
    }

    /**
     * Load effects from config to potion meta
     * @param section Configuration section
     * @param pm Potion meta
     */
    public static void potionFromConfig(Map<?, ?> section, PotionMeta pm){
        pm.clearCustomEffects();

        for (var rawEffect : section.keySet()) {
            var effect = (String) rawEffect;

            PotionEffectType type = null;

            try{
                type = PotionEffectType.getByName(effect.toUpperCase());
            }catch(Exception ignored) { }

            if (type == null)
                continue;

            var effectSection = (Map<?, ?>) section.get(effect);

            var duration = (int) (effectSection.containsKey("duration") ? effectSection.get("duration") : 10) * 20;
            var amplifier = (int) (effectSection.containsKey("amplifier") ? effectSection.get("amplifier") : 1) - 1;
            var ambient = (boolean) (effectSection.containsKey("ambient") ? effectSection.get("ambient") : true);
            var particles = (boolean) (effectSection.containsKey("particles") ? effectSection.get("particles") : true);
            var icon = (boolean) (effectSection.containsKey("icon") ? effectSection.get("icon") : true);

            pm.addCustomEffect(new PotionEffect(
                    type,
                    duration,
                    amplifier,
                    ambient,
                    particles,
                    icon
            ), true);
        }
    }

    /**
     * Get enum by name
     * @param name Name (case-insensitive)
     * @param c Class
     * @return Material, if not found - null
     */
    public static <T extends Enum> T getEnumByName(String name, Class<T> c, T def){
        if (name == null)
            return null;

        T instance = null;

        try {
            instance = (T) Enum.valueOf(c, name.toUpperCase());
        }catch(Exception ignored) { }

        if (instance == null && def != null)
            instance = def;

        return instance;
    }

    /**
     * Get enum by name
     * @param name Name (case-insensitive)
     * @param c Class
     * @return Material, if not found - null
     */
    public static <T extends Enum> T getEnumByName(String name, Class<T> c){
        return getEnumByName(name, c, null);
    }

    /**
     * Delete from entries list
     * @param check Check
     * @param list Entries list
     */
    public static <T> void deleteFromEntriesList(Function<T, Boolean> check, List<Map.Entry<T, CompletableFuture<T>>> list){

        for (var entry : new ArrayList<>(list)) {
            if (entry.getKey() == null || !check.apply(entry.getKey()))
                continue;

            list.remove(entry);
        }

    }

    /**
     * Получить приоритет метода в классе
     * @param c Класс
     * @param method Имя метода
     * @return Приоритет, если не найден - NORMAL
     */
    public static Settings.Priority getPriority(Class<?> c, String method){
        try{
            var p = c.getMethod(method).getAnnotation(Settings.class);

            if (p != null)
                return p.priority();
        }catch(Exception ignored) {}

        return Settings.Priority.NORMAL;
    }

    /**
     * Получить поведение метода в классе
     * @param c Класс
     * @param method Имя метода
     * @return Поведение, если не найден - MULTIPLE
     */
    public static Settings.Behaviour getBehaviour(Class<?> c, String method){
        try{
            var p = c.getMethod(method).getAnnotation(Settings.class);

            if (p != null)
                return p.behaviour();
        }catch(Exception ignored) {}

        return Settings.Behaviour.MULTIPLE;
    }

    /**
     * Instance class and invoke method
     * @param c Class
     * @param method Method name
     * @param parameters Method parameters
     * @param args Method arguments
     */
    public static void instanceAndInvoke(Class<?> c, String method, Class<?>[] parameters, Object[] args){

        try{
            var instance = c.getConstructor().newInstance();
            c.getMethod(method, parameters).invoke(instance, args);
        }catch(Exception ignored) {}

    }

    /**
     * Instance class and invoke method without arguments
     * @param c Class
     * @param method Method name
     */
    public static void instanceAndInvoke(Class<?> c, String method){
        instanceAndInvoke(c, method, new Class<?>[0], new Object[0]);
    }

    /**
     * Get instance of class
     * @param c Class
     * @param <T> Type
     * @return Instance, if not found no args constructor - null
     */
    public static <T> T getInstance(Class<? extends T> c){
        try{
            return c.getConstructor().newInstance();
        }catch(Exception ignored) {}

        return null;
    }
    /**
     * Превратить объект в число
     * @param o Объект
     * @return Число
     */
    public static Integer getInteger(Object o, Integer def){
        if (o instanceof Integer)
            return ((Integer) o);
        if (o instanceof String){
            Integer i = null;

            try{
                i = Integer.parseInt((String) o);
            }catch(Exception ignored) { }

            if (i != null)
                return i;
        }
        else if (o instanceof Double)
            return ((Double) o).intValue();

        return def;
    }

    /**
     * Delete directory
     * @param file Directory
     */
    public static void deleteDirectory(File file) {
        if (file == null)
            return;

        var contents = file.listFiles();

        if (contents != null) {

            for (var f : contents) {
                if (Files.isSymbolicLink(f.toPath()))
                    continue;

                deleteDirectory(f);
            }

        }

        file.delete();
    }

    /**
     * Get lower case UUID from UUID info
     * @param uuid UUID
     * @return Lower case UUID
     */
    public static String uuid(UUID uuid){
        return uuid.toString().toLowerCase();
    }

    /**
     * Get lower case UUID from Player
     * @param p Player
     * @return Lower case UUID
     */
    public static String uuid(OfflinePlayer p){
        return uuid(p.getUniqueId());
    }

    /**
     * Получить имя класса
     * @param c Класс
     * @return Формат типа Info.Listener
     */
    public static String getClassName(Class<?> c){
        return c.getName().substring(c.getName().lastIndexOf(".") + 1).replace("$", ".");
    }

    /**
     * Равны ли две локации по миру, X, Y, Z
     * @param l1 Первая локация
     * @param l2 Вторая локация
     * @return Равны или нет
     */
    public static boolean equals(Location l1, Location l2){

        return
                l1.getWorld().getName().equalsIgnoreCase(l2.getWorld().getName())
                &&
                l1.getBlockX() == l2.getBlockX()
                &&
                l1.getBlockY() == l2.getBlockY()
                &&
                l1.getBlockZ() == l2.getBlockZ();

    }

    /**
     * Превратить объект в дабл
     * @param o Объект
     * @return Дабл
     */
    public static Double getDouble(Object o, Double def){
        if (o instanceof Integer)
            return ((Integer) o).doubleValue();
        else if (o instanceof Double)
            return (Double) o;
        else if (o instanceof String) {

            try{
                return Double.parseDouble((String) o);
            }catch(Exception ignored) { }

        }

        return def;
    }

    /**
     * Получить метод по классу, имени поля
     * @param c Класс
     * @param method Имя метода
     * @return Поле, если не найден - null
     */
    public static Method getMethod(Class<?> c, String method, Class<?>... parameterTypes){
        Method result = null;

        try{
            result = c.getMethod(method, parameterTypes);
        }catch (Exception ignored) { }

        return result;
    }

    /**
     * Получить все КОНЕЧНЫЕ (абстракции идут лесом) классы в пакете, наследованные от класса T, с помощью рефлексии
     * @param <T> Класс, от которого будет всё наследовано
     * @param packageName Пакет
     * @param t Класс, от которого будет всё наследовано
     * @return Список всех классов, наследованных от класса T
     */
    public static <T> Set<Class<? extends T>> getAllClassesInPackage(String packageName, Class<T> t){
        Set<Class<? extends T>> classes = new HashSet<>();

        for (var c : Utils.getClasses(Main.getInstance(), packageName)) {
            if (c.equals(t) || Modifier.isAbstract(c.getModifiers()) || !t.isAssignableFrom(c))
                continue;

            classes.add((Class<? extends T>) c);
        }

        return classes;
    }

    /**
     * Сканирует все доступные классы из плагина
     *
     * @param plugin Плагин
     * @param packageName Пакет, где осуществляется поиск
     * @return Найденные классы (если не найдены - 0 элементов)
     */
    public static Set<Class<?>> getClasses(JavaPlugin plugin, String packageName) {
        Set<Class<?>> classes = new HashSet<>();

        try{
            var getFileMethod = JavaPlugin.class.getDeclaredMethod("getFile");
            getFileMethod.setAccessible(true);
            var file = (File) getFileMethod.invoke(plugin);

            var jarFile = new JarFile(file);

            for (var entry = jarFile.entries(); entry.hasMoreElements();) {
                var jarEntry = entry.nextElement();
                var name = jarEntry.getName().replace("/", ".");

                if(name.startsWith(packageName) && name.endsWith(".class"))
                    classes.add(Class.forName(name.substring(0, name.length() - 6)));
            }

            jarFile.close();
        } catch(Exception ignored) {  }

        return classes;
    }

    /**
     * Получить расширение файла
     * @param file Файл
     * @return Расширение файла
     */
    public static Map.Entry<String, String> getFileEntry(File file){
        var splitted = file.getName().split("\\.");

        var sb = new StringBuilder();

        for(int i = 0; i < splitted.length - 1; i++){
            sb.append(splitted[i]);

            if (i != splitted.length - 2)
                sb.append(" ");
        }

        return new AbstractMap.SimpleEntry<>(sb.toString(), splitted[splitted.length - 1]);
    }

    /**
     * Этот метод скопирует все ресурсы с JAR файла текущего потока и извлечёт их в указанную папку
     *
     * @param jarPath JAR папка
     * @param destination Конечная папка извлечения
     * @param replacement Заменять ли существующие файлы
     */
    public static void copyFilesFromJar(String jarPath, File destination, boolean replacement) {
        var url = Utils.class.getResource("/" + jarPath);

        if (url == null)
            return;

        JarURLConnection connection = null;

        try {
            connection = (JarURLConnection) url.openConnection();
        } catch (Exception ignored) { }

        if (connection == null)
            return;

        try {
            var jarFile = connection.getJarFile();

            for (var e = jarFile.entries(); e.hasMoreElements();) {
                var jarEntry = e.nextElement();
                var jarEntryName = jarEntry.getName();
                var jarConnectionEntryName = connection.getEntryName();

                if (jarEntryName.startsWith(jarConnectionEntryName)) {
                    var filename = jarEntryName.startsWith(jarConnectionEntryName) ? jarEntryName.substring(jarConnectionEntryName.length()) : jarEntryName;
                    var currentFile = new File(destination, filename);

                    if (jarEntry.isDirectory()) {
                        currentFile.mkdirs();
                    } else {
                        if (!replacement && currentFile.exists())
                            continue;

                        InputStream is = jarFile.getInputStream(jarEntry);
                        OutputStream out = openOutputStream(currentFile);

                        copy(is, out);

                        is.close();
                        out.close();
                    }
                }
            }
        } catch (Exception ignored) { }

    }

    /**
     * Open output stream
     * @param file File
     * @param append Append or not
     * @return Output stream
     * @throws IOException Write or file is directory exception
     */
    public static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
        if (file.exists()) {
            if (file.isDirectory())
                throw new IOException("File '" + file + "' exists but is a directory");

            if (!file.canWrite())
                throw new IOException("File '" + file + "' cannot be written to");
        } else {
            var parent = file.getParentFile();

            if (parent != null && !parent.mkdirs() && !parent.isDirectory())
                throw new IOException("Directory '" + parent + "' could not be created");
        }

        return new FileOutputStream(file, append);
    }

    /**
     * Open output stream to file
     * @param file File
     * @return Output stream
     * @throws IOException Write or file is directory exception
     */
    public static FileOutputStream openOutputStream(File file) throws IOException {
        return openOutputStream(file, false);
    }

    /**
     * Copy
     * @param input Input stream
     * @param output Output stream
     * @return Count
     * @throws IOException Write exception
     */
    public static int copy(InputStream input, OutputStream output) throws IOException {
        long count = copyLarge(input, output);
        return count > 2147483647L ? -1 : (int)count;
    }

    /**
     * Copy
     * @param input Input stream
     * @param output Output stream
     * @return Count
     * @throws IOException Write exception
     */
    public static long copyLarge(InputStream input, OutputStream output) throws IOException {
        return copy(input, output, 4096);
    }

    /**
     * Copy
     * @param input Input stream
     * @param output Output stream
     * @param bufferSize Buffer size
     * @return Count
     * @throws IOException Write exception
     */
    public static long copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
        return copyLarge(input, output, new byte[bufferSize]);
    }

    /**
     * Copy
     * @param input Input stream
     * @param output Output stream
     * @param buffer Buffer
     * @return Count
     * @throws IOException Write exception
     */
    public static long copyLarge(InputStream input, OutputStream output, byte[] buffer) throws IOException {
        long count;
        int n;
        for(count = 0L; -1 != (n = input.read(buffer)); count += (long)n) {
            output.write(buffer, 0, n);
        }

        return count;
    }

    /**
     * Содержит ли массив строку независимо от регистра
     * @param s Строка
     * @param array Массив
     * @return Содержит или нет
     */
    public static boolean containsStringInArray(String s, String[] array){
        if (s == null || array == null || array.length == 0)
            return false;

        for (var e : array) {
            if (!e.equalsIgnoreCase(s))
                continue;

            return true;
        }

        return false;
    }

    /**
     * Получить главный пакет times раз
     * @param packageName Пакет
     * @param times Сколько раз получить главный пакет
     * @return Конечный результатный пакет
     */
    public static String getParentPackage(String packageName, int times){
        var result = packageName;

        // Выполняем X раз поднятие пакета
        for(int i = 0; i < times; i++){

            result = result.substring(0, result.lastIndexOf('.'));

        }

        return result;
    }

    /**
     * Получить версию сервера
     * @param server Баккит-сервер
     * @return Версия сервера (Формат: 16)
     */
    public static int getVersion(Server server) {
        // Достаём название пакета сервера типа 'org.bukkit.craftbukkit.v1_16_R3'
        var packageName = server.getClass().getPackage().getName();

        // Достаём ласт папку формата 'v1_16_R3', после убираем 'v1_' и имеем '16_R3'
        var temp = packageName.substring(packageName.lastIndexOf('.') + 1).replace("v1_", "");

        // Обрезаем до символа '_', получая в итоге '16'
        temp = temp.substring(0, temp.lastIndexOf("_"));

        // Пытаемся парсить в число
        var result = 0;

        try{
            result = Integer.parseInt(temp);
        }catch(Exception ignored) { }

        // Возвращаем число
        return result;
    }

    /**
     * Поиск указанной строки в массиве строк
     * @param target Какую строку ищем в массиве
     * @param ss Массив строк, в котором ищем строку
     * @return Нашли ли в массиве указанную строку
     */
    public static boolean equals(String target, String... ss){
        for (String s : ss){

            if (target.equalsIgnoreCase(s))
                return true;

        }

        return false;
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public static void addIfStartsWith(List<String> list, String startsWith, List<String> inputs){
        addIfStartsWith(list, startsWith, inputs.toArray(new String[0]));
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public static void addIfStartsWith(List<String> list, String startsWith, String... inputs){

        for(var input : inputs) {

            if (input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length()) {
                list.add(input);
            }

        }

    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param input Строка, в которой ищем начало
     */
    public static void addIfStartsWith(List<String> list, String startsWith, String input){

        if (input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length()){
            list.add(input);
        }

    }

    /**
     * Начинается ли строка с указанной строки
     * @param input Строка, в которой ищем начало
     * @param startsWith Начало строки, которое ищем
     * @return Нашли ли такое начало в строке
     */
    public static boolean isStartsWith(String input, String startsWith){

        return input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length();

    }

    public static boolean hasItems(Inventory inv, Map<ItemStack, Integer> map) {
        Map<ItemStack, Integer> clone = new HashMap<>(map);

        takeItems(inv, clone, false);

        return clone.size() <= 0;
    }

    public static boolean takeItemsIfHas(Inventory inv, Map<ItemStack, Integer> map) {
        Map<ItemStack, Integer> clone = new HashMap<>(map);

        takeItems(inv, clone, false);

        if (clone.size() > 0)
            return false;

        clone = new HashMap<>(map);

        takeItems(inv, clone, true);
        return true;
    }

    public static Map<ItemStack, Integer> takeItems(Inventory inv, Map<ItemStack, Integer> map, boolean modifyAmount){
        for(int i = 0; i < inv.getSize(); i++){
            ItemStack is = inv.getItem(i);
            if (is == null || is.getType() == Material.AIR) continue;

            takeItemInSlot(inv, i, map, modifyAmount);
        }

        return map;
    }

    public static Map<ItemStack, Integer> takeItemInSlot(Inventory inv, int slot, Map<ItemStack, Integer> map, boolean modifyAmount){
        var is = inv.getItem(slot);

        if (is == null) return map;

        for(var entry : map.entrySet()) {

            if (entry.getKey().getType() == is.getType() && Objects.equals(Utils.getCustomModelData(entry.getKey()), Utils.getCustomModelData(is))) {
                int ost;
                int ostMap;

                if (entry.getValue() >= is.getAmount()){

                    ost = 0;
                    ostMap = entry.getValue() - is.getAmount();

                }else{

                    ost = is.getAmount() - entry.getValue();
                    ostMap = 0;

                }

                if (modifyAmount){

                    if (ost <= 0) inv.setItem(slot, new ItemStack(Material.AIR));
                    else is.setAmount(ost);

                }

                if (ostMap <= 0) map.remove(entry.getKey());
                else map.put(entry.getKey(), ostMap);

                return map;
            }

        }

        return map;
    }

    public static void takeItems(Inventory inv, Player player, Function<ItemStack, Boolean> checker){
        for(int i = 0; i < inv.getSize(); i++){
            ItemStack is = inv.getItem(i);
            if (is == null || is.getType() == Material.AIR) continue;

            takeItemInSlot(inv, i, checker);
        }

        if (inv instanceof PlayerInventory) {
            var playerInventory = (PlayerInventory) inv;

            takeItemInSlot(playerInventory, EquipmentSlot.OFF_HAND, checker);
        }

        if (player != null && player.getItemOnCursor() != null && !player.getItemOnCursor().getType().isAir() && checker.apply(player.getItemOnCursor()))
            player.setItemOnCursor(new ItemStack(Material.AIR));
    }

    public static void takeItemInSlot(Inventory inv, int slot, Function<ItemStack, Boolean> checker){
        var is = inv.getItem(slot);

        if (is == null || is.getType().isAir())
            return;

        if (checker.apply(is))
            inv.setItem(slot, new ItemStack(Material.AIR));
    }

    public static void takeItemInSlot(PlayerInventory inv, EquipmentSlot equipmentSlot, Function<ItemStack, Boolean> checker){
        var is = inv.getItem(equipmentSlot);

        if (is == null || is.getType().isAir())
            return;

        if (checker.apply(is))
            inv.setItem(equipmentSlot, new ItemStack(Material.AIR));
    }

    /**
     * Удаление рецепта предмета
     * @param is Предмет
     */
    public static void removeRecipe(ItemStack is){
        List<Recipe> backup = new ArrayList<>();
        {
            Iterator<Recipe> a = Main.getInstance().getServer().recipeIterator();

            while(a.hasNext()){
                Recipe recipe = a.next();
                ItemStack result = recipe.getResult();

                if(!result.isSimilar(is)){
                    backup.add(recipe);
                }
            }
        }

        Main.getInstance().getServer().clearRecipes();

        for (Recipe r : backup)
            Main.getInstance().getServer().addRecipe(r);
    }

    /**
     * Округлить дробное число до x мест после запятой
     * @param value Дробное число
     * @param places Мест после запятой
     * @return Округленное дробное значение
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * Получить запись ключ-значение
     * @param key Ключ
     * @param value Значение
     * @return Запись ключ-значение
     */
    public static Map.Entry<?, ?> getEntry(Object key, Object value){
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    /**
     * Преобразовать строку в баккит-локацию
     * @param s Строка
     * @param yp Содержит ли строка yaw и pitch (положение курсора)
     * @return Локация, отражающая строку
     */
    public static Location stringToLoc(String s, Boolean yp) {
        if (s != null) {
            String[] splitted = s.split(",");

            if ( (splitted.length == 6 && yp) || (splitted.length == 4 && !yp)) {
                World world = Bukkit.getWorld(splitted[0]);

                if (world != null) {

                    double x = Double.parseDouble(splitted[1]);
                    double y = Double.parseDouble(splitted[2]);
                    double z = Double.parseDouble(splitted[3]);

                    if (yp){

                        float yaw = Float.parseFloat(splitted[4]);
                        float pitch = Float.parseFloat(splitted[5]);
                        return new Location(world, x, y, z, yaw, pitch);

                    }else{

                        return new Location(world, x, y, z);

                    }

                }

            }

        }
        return null;
    }

    /**
     * Преобразовать баккит-локацию в строку
     * @param l Локация
     * @param yp Будет ли содержать строка yaw и pitch (положение курсора)
     * @return Строка, отражающая локацию
     */
    public static String locToString(Location l, Boolean yp) {
        if (yp) return String.format("%s, %s, %s, %s, %s, %s", l.getWorld().getName(), l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
        else return String.format("%s, %s, %s, %s", l.getWorld().getName(), l.getBlockX(), l.getBlockY(), l.getBlockZ());
    }

    /**
     * Закрыть специфический инвентарь у игрока
     * @param viewer Игрок
     * @param inventory Инвентарь на проверку (приоритет)
     * @param inventoryChecker Кастомная проверка инвентаря
     */
    public static void closeInventory(HumanEntity viewer, Inventory inventory, Function<Inventory, Boolean> inventoryChecker){
        if (!isValidOpenedInventory(viewer, inventory, inventoryChecker))
            return;

        Main.getFoliaLib().getImpl().runNextTick((task) -> {
            if (!isValidOpenedInventory(viewer, inventory, inventoryChecker))
                return;

            viewer.closeInventory();
        });
    }

    /**
     * Закрыть специфический инвентарь у игрока
     * @param viewer Игрок
     * @param inventoryChecker Кастомная проверка инвентаря
     */
    public static void closeInventory(HumanEntity viewer, Function<Inventory, Boolean> inventoryChecker){
        closeInventory(viewer, null, inventoryChecker);
    }

    /**
     * Закрыть специфический инвентарь у игрока
     * @param viewer Игрок
     * @param inventory Инвентарь на проверку
     */
    public static void closeInventory(HumanEntity viewer, Inventory inventory){
        closeInventory(viewer, inventory, null);
    }

    private static boolean isValidOpenedInventory(HumanEntity viewer, Inventory inventory, Function<Inventory, Boolean> inventoryChecker){
        if (viewer.getOpenInventory() == null || viewer.getOpenInventory().getTopInventory() == null)
            return false;

        if (inventory != null && !viewer.getOpenInventory().getTopInventory().equals(inventory))
            return false;

        if (inventoryChecker != null && !inventoryChecker.apply(viewer.getOpenInventory().getTopInventory()))
            return false;

        return true;
    }

    /**
     * Закрыть инвентарь у игрока
     * @param viewer Игрок
     */
    public static void closeInventory(HumanEntity viewer){
        Main.getFoliaLib().getImpl().runNextTick((task) -> {
            viewer.closeInventory();
        });
    }

    /**
     * Найти квадрат числа
     * @param x Число
     * @return Квадрат данного числа
     */
    private static double square(double x){
        return x * x;
    }

    /**
     * Получение слотов по строкам в инвентаре
     * @param rows - Количество горизонтальных строк
     * @return - Количество слотов
     */
    public static int getSlots(int rows){
        return rows * 9;
    }

    /**
     * Извлечь подсписок по указанным индексам (вкл.)
     * @param from Минимальное значение (отправная точка) (вкл.)
     * @param to Максимальное значение (конечная точка) (вкл.)
     * @param src Список, откуда будут браться элементы
     * @return Подсписок
     */
    public static <T> List<T> getFromTo(int from, int to, List<T> src){
        List<T> list = new ArrayList<>();

        for(int i = from; i <= to; i++){
            if (i > src.size() - 1) break;

            list.add(src.get(i));
        }

        return list;
    }

    /**
     * Получить стактрейс с указанным комментарием
     * @param s Комментарий
     * @return Стактрейс
     */
    public static String getStackTrace(String s){
        var sw = new StringWriter();
        new Throwable("").printStackTrace(new PrintWriter(sw));

        return sw.toString().replace("java.lang.Throwable", s);
    }

    /**
     * Получить информацию о строке, где выполнена эта функция
     * @return Информация о строке, где выполнена эта функция
     */
    public static String getInfoLine(){
        StackTraceElement l = new Exception().getStackTrace()[1];

        return l.getClassName() + "/" + l.getMethodName() + ":" + l.getLineNumber();
    }

    /**
     * Исполнить действие (команду) от указанного отправителя (СИНХРОННО) по секционному ключу ([секция].action) конфигурации, если он есть
     * @param s Отправитель, от которого будет воспроизведена отправка
     * @param section Ключ-секция (конечный итог: [секция].action)
     * @param config Конфигурация, откуда будет извлекаться информация
     */
    public static void action(CommandSender s, String section, FileConfiguration config){
        List<String> actions = new ArrayList<>();

        if (!config.isSet(section + ".action")) return;

        if (config.isList(section + ".action")) actions.addAll(config.getStringList(section + ".action"));
        else actions.add(config.getString(section + ".action"));

        actions.forEach(x -> Bukkit.dispatchCommand(s, x));
    }

    /**
     * Замена амперсанта в строке на колоркод
     * @param s Строка, в которой требуется заменить колоркоды
     * @return Изменённая строка
     */
    public static String toColor(String s){
        if (s == null) return "";

        /*while(true){
            var matcher = hexPattern.matcher(s);

            if (!matcher.find())
                break;

            var color = s.substring(matcher.start(), matcher.end());
            s = s.replace(color, "" + net.md_5.bungee.api.ChatColor(color));
        }*/

        return net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', s);
    }

    /**
     * Случайное число во включаемом диапазоне
     * @param min Минимальное значение (вкл.)
     * @param max Максимальное значение (вкл.)
     * @return Случайное число в указанном диапазоне
     */
    public static int rand(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    /**
     * Десериализировать объект
     * @param stream Поток, который требуется десериализировать
     * @return Десериализированный объект
     */
    public static Object deserialize(InputStream stream) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(stream);

        try {
            return ois.readObject();
        }catch(Exception e){

            e.printStackTrace();

        } finally {
            ois.close();
        }

        return null;
    }

    /**
     * Сериализировать объект
     * @param o Объект, который требуется сериализировать
     * @return Cериализированный набор байтов
     */
    public static byte[] serialize(Object o) throws IOException {
        if (o == null)
            return null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        try {
            oos.writeObject(o);
        }catch(NotSerializableException e){

            System.out.println(o.toString());
            e.printStackTrace();
            return null;

        }

        oos.close();

        return baos.toByteArray();
    }

    /**
     * Сериализировать из ItemStack в Map
     * @param itemStack Баккит-предмет, который требуется сериализировать
     * @return Cериализированный Map
     */
    public static HashMap<Map<String, Object>, Map<String, Object>> serializeItemStack(ItemStack itemStack) {
        HashMap<Map<String, Object>, Map<String, Object>> serializedMap = new HashMap<>();
        Map<String, Object> serializedItemStack, serializedItemMeta;

        if (itemStack == null) itemStack = new ItemStack(Material.AIR);

        if (itemStack.hasItemMeta()) {
            serializedItemMeta = new HashMap<>(itemStack.getItemMeta().serialize());
        }else serializedItemMeta = null;
        itemStack.setItemMeta(null);

        if(serializedItemMeta != null){

            if (serializedItemMeta.containsKey("patterns")){
                List<Map<String, Object>> list = new ArrayList<>();

                for(Pattern p : (List<Pattern>) serializedItemMeta.get("patterns")){

                    list.add(p.serialize());

                }

                serializedItemMeta.put("patterns", list);
            }

            if (serializedItemMeta.containsKey("charged-projectiles")){
                List<Object> list = new ArrayList<>();

                for(ItemStack is : (List<ItemStack>) serializedItemMeta.get("charged-projectiles")){

                    list.add(serializeItemStack(is));

                }

                serializedItemMeta.put("charged-projectiles", list);
            }

            if (serializedItemMeta.containsKey("attribute-modifiers")){
                HashMap<String, List<Map<String, Object>>> map = new HashMap<>();

                for(Map.Entry<String, List<AttributeModifier>> entry : ((HashMap<String, List<AttributeModifier>>) serializedItemMeta.get("attribute-modifiers")).entrySet()){
                    List<Map<String, Object>> list = new ArrayList<>();

                    for(AttributeModifier am : entry.getValue()){

                        list.add(am.serialize());

                    }

                    map.put(entry.getKey(), list);
                }

                serializedItemMeta.put("attribute-modifiers", map);
            }

            if (serializedItemMeta.containsKey("meta-type") && serializedItemMeta.get("meta-type").equals("SUSPICIOUS_STEW") && serializedItemMeta.containsKey("effects")) {
                List<Object> list = new ArrayList<>();

                for(var pe : (List<PotionEffect>) serializedItemMeta.get("effects")){
                    list.add(pe.serialize());
                }

                serializedItemMeta.put("effects", list);
            }

            if (serializedItemMeta.containsKey("firework-effects")){
                List<Map<String, Object>> list = new ArrayList<>();

                for(FireworkEffect fe : (List<FireworkEffect>) serializedItemMeta.get("firework-effects")){
                    Map<String, Object> serialized = new HashMap<>(fe.serialize());

                    if (serialized.containsKey("colors")){
                        List<Map<String, Object>> list2 = new ArrayList<>();

                        for(Color c : (List<Color>) serialized.get("colors")){

                            list2.add(c.serialize());

                        }

                        serialized.put("colors", list2);
                    }

                    if (serialized.containsKey("fadeColors")){
                        List<Map<String, Object>> list2 = new ArrayList<>();

                        for(Color c : (List<Color>) serialized.get("fadeColors")){

                            list2.add(c.serialize());

                        }

                        serialized.put("fadeColors", list2);
                    }


                    list.add(serialized);
                }

                serializedItemMeta.put("firework-effects", list);
            }

            if (serializedItemMeta.containsKey("display-map-color")){

                serializedItemMeta.put("display-map-color", ((Color) serializedItemMeta.get("display-map-color")).serialize());

            }

            if (serializedItemMeta.containsKey("custom-color")){

                serializedItemMeta.put("custom-color", ((Color) serializedItemMeta.get("custom-color")).serialize());

            }

            if (serializedItemMeta.containsKey("color")){

                serializedItemMeta.put("color", ((Color) serializedItemMeta.get("color")).serialize());

            }

        }

        try {
            serializedItemStack = itemStack.serialize();

            serializedMap.put(serializedItemStack, serializedItemMeta);
        }catch(Exception e){

            e.printStackTrace();
            return new HashMap<>();

        }

        return serializedMap;
    }

    /**
     * Десериализировать из Map в ItemStack
     * @param serializedItemStack Сериализированный Map из serializeItemStack(ItemStack)
     * @return Десериализированный баккит-предмет
     */
    public static ItemStack deserializeItemStack(Map<Map<String, Object>, Map<String, Object>> serializedItemStack) {
        Map.Entry<Map<String, Object>, Map<String, Object>> serializedItemStackEntries = serializedItemStack.entrySet().iterator().next();

        if (serializedItemStackEntries.getValue() != null){

            if (serializedItemStackEntries.getValue().containsKey("patterns")){
                List<Pattern> list = new ArrayList<>();
                List<Map<String, Object>> ps = (List<Map<String, Object>>) serializedItemStackEntries.getValue().get("patterns");

                for(Map<String, Object> map : ps){
                    list.add((Pattern) ConfigurationSerialization.deserializeObject(map, ConfigurationSerialization.getClassByAlias("Pattern")));
                }

                serializedItemStackEntries.getValue().put("patterns", list);
            }

            if (serializedItemStackEntries.getValue().containsKey("charged-projectiles")){
                List<ItemStack> list = new ArrayList<>();
                List<Object> is = (List<Object>) serializedItemStackEntries.getValue().get("charged-projectiles");

                for(Object map : is){
                    list.add(deserializeItemStack((Map<Map<String, Object>, Map<String, Object>>) map));
                }

                serializedItemStackEntries.getValue().put("charged-projectiles", list);
            }

            if (serializedItemStackEntries.getValue().containsKey("attribute-modifiers")){
                HashMap<String, List<AttributeModifier>> map = new HashMap<>();

                for(Map.Entry<String, List<Map<String, Object>>> entry : ((HashMap<String, List<Map<String, Object>>>) serializedItemStackEntries.getValue().get("attribute-modifiers")).entrySet()){
                    List<AttributeModifier> list = new ArrayList<>();

                    for(Map<String, Object> am : entry.getValue()){

                        list.add(AttributeModifier.deserialize(am));

                    }

                    map.put(entry.getKey(), list);
                }

                serializedItemStackEntries.getValue().put("attribute-modifiers", map);
            }

            if (serializedItemStackEntries.getValue().containsKey("display-map-color")){

                serializedItemStackEntries.getValue().put("display-map-color", Color.deserialize((Map<String, Object>) serializedItemStackEntries.getValue().get("display-map-color")));

            }

            if (serializedItemStackEntries.getValue().containsKey("custom-color")){

                serializedItemStackEntries.getValue().put("custom-color", Color.deserialize((Map<String, Object>) serializedItemStackEntries.getValue().get("custom-color")));

            }

            if (serializedItemStackEntries.getValue().containsKey("color")){

                serializedItemStackEntries.getValue().put("color", Color.deserialize((Map<String, Object>) serializedItemStackEntries.getValue().get("color")));

            }

            if (serializedItemStackEntries.getValue().containsKey("meta-type") && serializedItemStackEntries.getValue().get("meta-type").equals("SUSPICIOUS_STEW") && serializedItemStackEntries.getValue().containsKey("effects")) {
                List<PotionEffect> list = new ArrayList<>();

                for(var pe : (List<Object>) serializedItemStackEntries.getValue().get("effects")){
                    list.add((PotionEffect) ConfigurationSerialization.deserializeObject((Map<String, ?>) pe, ConfigurationSerialization.getClassByAlias("PotionEffect")));
                }

                serializedItemStackEntries.getValue().put("effects", list);
            }

            if (serializedItemStackEntries.getValue().containsKey("firework-effects")){
                List<FireworkEffect> list = new ArrayList<>();
                List<Map<String, Object>> ps = (List<Map<String, Object>>) serializedItemStackEntries.getValue().get("firework-effects");

                for(Map<String, Object> map : ps){

                    if (map.containsKey("colors")){
                        List<Color> list2 = new ArrayList<>();

                        for(Map<String, Object> c : (List<Map<String, Object>>) map.get("colors")){

                            list2.add((Color) ConfigurationSerialization.deserializeObject(c, ConfigurationSerialization.getClassByAlias("Color")));

                        }

                        map.put("colors", list2);
                    }

                    if (map.containsKey("fadeColors")){
                        List<Color> list2 = new ArrayList<>();

                        for(Map<String, Object> c : (List<Map<String, Object>>) map.get("fadeColors")){

                            list2.add((Color) ConfigurationSerialization.deserializeObject(c, ConfigurationSerialization.getClassByAlias("Color")));

                        }

                        map.put("fadeColors", list2);
                    }

                    list.add((FireworkEffect) FireworkEffect.deserialize(map));
                }

                serializedItemStackEntries.getValue().put("firework-effects", list);
            }

        }

        ItemStack itemStack = new ItemStack(Material.AIR);

        try {
            itemStack = ItemStack.deserialize(serializedItemStackEntries.getKey());
            if (serializedItemStackEntries.getValue() != null) {
                ItemMeta itemMeta = (ItemMeta) ConfigurationSerialization.deserializeObject(serializedItemStackEntries.getValue(), ConfigurationSerialization.getClassByAlias("ItemMeta"));

                itemStack.setItemMeta(itemMeta);
            }
        }catch(Exception e){

            e.printStackTrace();

        }


        return itemStack;
    }

    /**
     * Пропарсить сообщение на кнопки в строку
     * @param s Строка
     * @param ps Переменные
     * @return Сообщения
     */
    public static BaseComponent[] process(String s, P... ps){
        StringBuilder sb = new StringBuilder(replace(s, ps));
        ComponentBuilder cb = new ComponentBuilder();

        var pattern = java.util.regex.Pattern.compile("\\{([^{}]*)\\}");

        var matcher = pattern.matcher(sb.toString());

        boolean was = false;

        while(matcher.find()){
            MatchResult result = matcher.toMatchResult();

            String[] splitted = sb.substring(result.start() + 1, result.end() - 1).split("\\|");

            if (splitted.length < 3) {
                if (splitted.length == 2 && splitted[0].equalsIgnoreCase("translate")){
                    if (result.start() != 0)
                        cb.append(sb.substring(0, result.start()));

                    sb.delete(0, result.end());

                    var tc = new TranslatableComponent();
                    tc.setTranslate(splitted[1]);

                    cb.append(tc);

                    was = true;
                }

                continue;
            }

            if (result.start() != 0)
                cb.append(sb.substring(0, result.start()));

            sb.delete(0, result.end());

            var tc = new TextComponent();

            String[] types = splitted[0].split(",");

            for(int i = 0; i < types.length; i++){
                String type = types[i];
                String value = splitted[1 + i];

                ClickEvent.Action click = null;
                HoverEvent.Action hover = null;

                try {
                    click = ClickEvent.Action.valueOf(type.toUpperCase());
                }catch(Exception ignored) { }

                try {
                    hover = HoverEvent.Action.valueOf(type.toUpperCase());
                }catch(Exception ignored) { }

                if (click != null)
                    tc.setClickEvent(new ClickEvent(click, value));

                else if (hover != null) {
                    List<Content> bcs = new ArrayList<>();
                    String[] lines = value.split("\\\\n");

                    for (int j = 0; j < lines.length; j++) {
                        bcs.add(new Text(((j != 0) ? "\n" : "") + lines[j]));
                    }

                    tc.setHoverEvent(new HoverEvent(hover, bcs));
                }

            }

            tc.setText(splitted[splitted.length - 1]);

            cb.append(tc);

            was = true;
        }

        if (!was)
            cb.append(sb.toString());

        return cb.create();
    }

    /**
     * Получение из баккит-предмета (ItemStack) строки
     * @param is Баккит-предмет
     * @return Строка. Формат - [материал], [дата], [количество]
     */
    public static String itemToString(ItemStack is){

        return String.format("%s,%d,%d", is.getType().toString(), is.getDurability(), is.getAmount());

    }

    /**
     * Получение из строки баккит-предмета (ItemStack)
     * @param s Строка. Форматы - [материал] | [материал], [количество] | [материал], [дата], [количество]
     * @return Баккит-предмет, преобразованный из строки
     */
    public static ItemStack itemFromString(String s){
        String[] args = null;

        try{
            args = s.split(",");
        }catch(Exception ex) {
            ex.printStackTrace();
        }

        if (args == null)
            return new ItemStack(Material.STONE);

        ItemStack is = null;

        try {
            Material m = null;

            if (args.length >= 1)
                m = Material.matchMaterial(args[0]);

            if (m == null)
                return new ItemStack(Material.STONE);

            if (args.length == 3)
                is = new ItemStack(m, Integer.parseInt(args[2]), Short.parseShort(args[1]));
            else if (args.length == 2)
                is = new ItemStack(m, Integer.parseInt(args[1]));
            else if (args.length == 1)
                is = new ItemStack(m, 1);

        }catch(Exception ex){
            ex.printStackTrace();
        }

        if (is == null)
            return null;

        return is;
    }

    /**
     * Получение массива строк из конфигурации по ключу
     * @param config Конфигурация
     * @param s Ключ
     * @return Массив строк
     */
    public static List<String> getList(FileConfiguration config, String s){

        if (config.isList(s)){

            return config.getStringList(s);

        }else{

            return new ArrayList<String>(){{ add(config.getString(s)); }};

        }

    }

    /**
     * Замена в строках амперсантов на параграфы (колоркоды)
     * @param ss Строка, где требуется заменить переменные
     * @return Измененная строка с колоркодами
     */
    public static List<String> toColor(List<String> ss){
        List<String> list = new ArrayList<>();
        
        for(String s : ss){
            
            list.add(Utils.toColor(s));
            
        }
        
        return list;
    }

    /**
     * Замена в строках переменных на данные
     * @param ss Строка, где требуется заменить переменные
     * @param ps Переменные
     * @return Массив с изменёнными строками
     */
    public static List<String> replace(List<String> ss, List<P> ps){
        return replace(ss, ps.toArray(new P[0]));
    }

    /**
     * Замена в строках переменных на данные
     * @param ss Строка, где требуется заменить переменные
     * @param ps Переменные
     * @return Массив с изменёнными строками
     */
    public static List<String> replace(List<String> ss, P... ps){
        List<String> list = new ArrayList<>();
        
        for(String s : ss){
            
            list.add(replace(s, ps));
            
        }
        
        return list;
    }

    /**
     * Замена в строке переменных на данные
     * @param s Строка, где требуется заменить переменные
     * @param ps Переменные
     * @return Изменённая строка
     */
    public static String replace(String s, List<P> ps){
        if (ps == null)
            return s;

        return replace(s, ps.toArray(new P[0]));
    }

    /**
     * Замена в строке переменных на данные
     * @param s Строка, где требуется заменить переменные
     * @param ps Переменные
     * @return Изменённая строка
     */
    public static String replace(String s, P... ps){

        if (ps != null){

            for (P p : ps){

                s = s.replace(p.getKey(), String.valueOf(p.getValue()));

            }

        }

        return Utils.toColor(s);

    }

    /**
     * Отправить заголовок на экран баккит-игроку по ключу из конфига сообщений с переменными
     * @param p Баккит-игрок, которому отправляется тайтл
     * @param path Ключ конфигурации
     * @param ps Переменные
     */
    public static void sendTitle(Player p, String path, P... ps){

        String title = "";
        if(Messages.get().isSet(path + ".title")) title = Messages.get().getString(path + ".title");

        String subtitle = "";
        if(Messages.get().isSet(path + ".subtitle")) subtitle = Messages.get().getString(path + ".subtitle");

        if (!title.isEmpty() || !subtitle.isEmpty()){

            p.sendTitle(replace(title, ps), replace(subtitle, ps));

        }

    }

    /**
     * Преобразует дробное число в строку формата #.##
     * @param d Дробное число
     * @return Строка содержащая дробное число в формате #.##
     */
    public static String doubleToString(double d){
        if (d == (int) d)
            return String.valueOf((int) d);

        return new DecimalFormat("#.##").format(d);
    }

    /**
     * Отправить броадкаст всем игрокам (ВЫПОЛНЯЕТСЯ СИНХРОННО) посредством ключа из конфига сообщений с опциональными переменными
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void broadcast(String path, P... ps){
        if (!Messages.get().isSet(path)) return;

        if (Messages.get().isList(path)){

            for (String s : Messages.get().getStringList(path)){

                Bukkit.broadcastMessage(replace(s, ps));

            }

        }else{

            Bukkit.broadcastMessage(replace(Messages.get().getString(path), ps));

        }

    }

    /**
     * Отправить сообщение игроку по имени (ВЫПОЛНЯЕТСЯ СИНХРОННО) посредством ключа из конфига сообщений с опциональными переменными
     * @param name Имя игрока
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void sendSync(String name, String path, P... ps){

        sendSync(name, Messages.get(), path, ps);

    }

    /**
     * Отправить сообщение игроку по имени (ВЫПОЛНЯЕТСЯ СИНХРОННО) посредством ключа из конфига с опциональными переменными
     * @param name Имя игрока
     * @param config Конфигурация
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void sendSync(String name, FileConfiguration config, String path, P... ps){
        Main.getFoliaLib().getImpl().runNextTick((task) -> {
            send(name, config, path, null, ps);
        });
    }

    /**
     * Отправить сообщение игроку по имени (ДОЛЖНО БЫТЬ ВЫПОЛНЕНО СИНХРОННО) посредством ключа из конфига сообщений с опциональными переменными
     * @param name Имя игрока
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void send(String name, String path, P... ps){

        send(name, Messages.get(), path, null, ps);

    }

    /**
     * Отправить сообщение игроку по имени (ДОЛЖНО БЫТЬ ВЫПОЛНЕНО СИНХРОННО) посредством ключа из конфига сообщений с опциональными переменными
     * @param name Имя игрока
     * @param path Ключ в конфигурации
     * @param placeholder_target Игрок для плейсхолдеров
     * @param ps Переменные
     */
    public static void send(String name, String path, Player placeholder_target, P... ps){

        send(name, Messages.get(), path, placeholder_target, ps);

    }

    /**
     * Отправить сообщение игроку по имени (ДОЛЖНО БЫТЬ ВЫПОЛНЕНО СИНХРОННО) посредством ключа из конфига с опциональными переменными
     * @param name Имя игрока
     * @param config Конфигурация
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void send(String name, FileConfiguration config, String path, Player placeholder_target, P... ps){
        if (name == null) return;

        Player p = Bukkit.getPlayer(name);

        if (p == null || !p.isOnline()) return;

        send(p, config, path, placeholder_target, ps);
    }

    /**
     * Отправить сообщение отправителю команд посредством ключа из конфига сообщений с опциональными переменными
     * @param sender Игрок
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void send(CommandSender sender, String path, P... ps){

        send(sender, Messages.get(), path, ps);

    }

    /**
     * Отправить сообщение отправителю команд посредством ключа из конфига сообщений с опциональными переменными
     * @param sender Игрок
     * @param path Ключ в конфигурации
     * @param placeholder_target Игрок для плейсхолдеров
     * @param ps Переменные
     */
    public static void send(CommandSender sender, String path, Player placeholder_target, P... ps){

        send(sender, Messages.get(), path, placeholder_target, ps);

    }

    /**
     * Отправить сообщение отправителю команд посредством ключа из конфига с опциональными переменными
     * @param sender Игрок
     * @param config Конфигурация
     * @param path Ключ в конфигурации
     * @param placeholder_target Игрок для плейсхолдеров
     * @param ps Переменные
     */
    public static void send(CommandSender sender, FileConfiguration config, String path, Player placeholder_target, P... ps){
        if (sender == null || !config.isSet(path))
            return;

        if (placeholder_target == null && sender instanceof Player)
            placeholder_target = (Player) sender;

        if (config.isList(path)){

            for (String s : config.getStringList(path)){

                if (ps != null){
                    var was = false;

                    for (var p : ps) {
                        if (!p.isListVariable() || !s.toLowerCase().contains(p.getKey().toLowerCase()) || !(p.getValue() instanceof List))
                            continue;

                        for (var s2 : (List<String>) p.getValue())
                            sender.spigot().sendMessage(process(placeholder_target != null ? PlaceholderAPIExtension.replace(placeholder_target, s2) : s2, ps));

                        was = true;
                        continue;
                    }

                    if (was)
                        continue;
                }

                sender.spigot().sendMessage(process(placeholder_target != null ? PlaceholderAPIExtension.replace(placeholder_target, s) : s, ps));
            }

        }else{
            var s = config.getString(path);

            if (ps != null && s != null){
                for (var p : ps) {
                    if (!p.isListVariable() || !s.toLowerCase().contains(p.getKey().toLowerCase()) || !(p.getValue() instanceof List))
                        continue;

                    for (var s2 : (List<String>) p.getValue())
                        sender.spigot().sendMessage(process(placeholder_target != null ? PlaceholderAPIExtension.replace(placeholder_target, s2) : s2, ps));

                    return;
                }
            }

            sender.spigot().sendMessage(process(placeholder_target != null ? PlaceholderAPIExtension.replace(placeholder_target, s) : s, ps));

        }

    }

    /**
     * Отправить сообщение отправителю команд посредством ключа из конфига с опциональными переменными
     * @param sender Игрок
     * @param config Конфигурация
     * @param path Ключ в конфигурации
     * @param ps Переменные
     */
    public static void send(CommandSender sender, FileConfiguration config, String path, P... ps){
        send(sender, config, path, null, ps);
    }

    /**
     * Отправить сообщение игроку с опциональными переменными
     * @param sender Игрок
     * @param message Сообщение
     * @param ps Переменные
     */
    public static void sendMessage(CommandSender sender, String message, List<P> ps){

        sender.sendMessage(Utils.replace(message, ps));

    }

    /**
     * Отправить сообщение игроку с опциональными переменными
     * @param sender Игрок
     * @param message Сообщение
     * @param ps Переменные
     */
    public static void sendMessage(CommandSender sender, String message, P... ps){

        sender.sendMessage(Utils.replace(message, ps));

    }

    /**
     * Включить звук игроку по локации, конфигу и пути до звука (Формат: [sound], [volume], [pitch])
     * @param p Игрок
     * @param location Локация
     * @param config Конфигурация
     * @param path Путь
     */
    public static void play(Player p, Location location, FileConfiguration config, String path){
        if (p == null || config == null || path == null || !config.isSet(path) || !config.isString(path))
            return;

        play(p, location, config.getString(path));
    }

    /**
     * Включить звук игроку по конфигу и пути до звука (Формат: [sound], [volume], [pitch])
     * @param p Игрок
     * @param config Конфигурация
     * @param path Путь
     */
    public static void play(Player p, FileConfiguration config, String path){
        if (p == null)
            return;

        play(p, p.getLocation(), config, path);
    }

    /**
     * Включить звук игроку по локации и строке (Формат: [sound], [volume], [pitch])
     * @param p Игрок
     * @param location Локация
     * @param rawSound Строка по формату, описывающий звук
     */
    public static void play(Player p, Location location, String rawSound){
        if (p == null || rawSound == null)
            return;

        var split = rawSound.split(",");

        if (split.length < 3)
            return;

        Sound sound = null;

        try{
            sound = Sound.valueOf(split[0].trim().toUpperCase());
        }catch(Exception ignored) { }

        Float volume = null;

        try{
            volume = Float.parseFloat(split[1].trim().toUpperCase());
        }catch(Exception ignored) { }

        Float pitch = null;

        try{
            pitch = Float.parseFloat(split[2].trim().toUpperCase());
        }catch(Exception ignored) { }

        play(p, location, sound, volume, pitch);
    }


    /**
     * Включить звук игроку по строке (Формат: [sound], [volume], [pitch])
     * @param p Игрок
     * @param rawSound Строка по формату, описывающий звук
     */
    public static void play(Player p, String rawSound){
        if (p == null)
            return;

        play(p, p.getLocation(), rawSound);
    }

    /**
     * Включить звук игроку по локации, звуку, громкости и тону
     * @param p Игрок
     * @param location Локация
     * @param sound Звук
     * @param volume Громкость
     * @param pitch Тон
     */
    public static void play(Player p, Location location, Sound sound, Float volume, Float pitch){
        if (p == null || location == null || sound == null)
            return;

        if (volume == null)
            volume = 1f;

        if (pitch == null)
            pitch = 1f;

        p.playSound(location, sound, volume, pitch);
    }

    /**
     * Включить звук игроку по звуку, громкости и тону
     * @param p Игрок
     * @param sound Звук
     * @param volume Громкость
     * @param pitch Тон
     */
    public static void play(Player p, Sound sound, Float volume, Float pitch){
        if (p == null)
            return;

        play(p, p.getLocation(), sound, volume, pitch);
    }

    /**
     * Класс ключ-значение
     * @author WattGuy
     * @version 1.0
     */
    @AllArgsConstructor
    @RequiredArgsConstructor
    public static class P {

        /**
         * Pattern for parser
         */
        public static final java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("\\{([^{}]*)\\}");

        /**
         * Ключ-строка
         */
        @Getter
        private final String key;

        /**
         * Значение-объект
         */
        @Getter
        private final Object value;

        @Getter
        @Setter
        private boolean listVariable = false;

        /**
         * Содержит ли массив переменную по ключу
         * @param key Ключ
         * @param ps Массив
         * @return Если содержит - информация, нет - null
         */
        public static P get(String key, List<P> ps){
            for (var p : ps) {

                if (p.getKey().equalsIgnoreCase(key))
                    return p;

            }

            return null;
        }

        /**
         * Parse variables in string
         * @param text Text
         * @return List of variables (without '{' and '}')
         */
        public static List<String> parseVariables(String text){
            var list = new ArrayList<String>();
            var m = pattern.matcher(text);

            while (m.find()) {
                list.add(m.group(1));
            }

            return list;
        }

        /**
         * Wrap variable name to braces
         * @param s Variable name
         * @return Variable name wrapped to braces
         */
        public static String wrap(String s){
            return "{" + s + "}";
        }

    }

}
