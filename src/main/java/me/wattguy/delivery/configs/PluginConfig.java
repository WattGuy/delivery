package me.wattguy.delivery.configs;

import lombok.Getter;
import lombok.var;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс-абстракция для конфигов-экземпляров
 * @author WattGuy
 * @version 1.0
 */
public abstract class PluginConfig {

    /**
     * Статичный экземпляр
     */
    public static Map<String, PluginConfig> instances = new HashMap<>();

    /**
     * Название конфига (без .yml)
     */
    @Getter
    private String name = null;

    @Getter
    private boolean defaultConfig = true;

    /**
     * Пустой конструктор для рефлексии
     */
    public PluginConfig(){ }

    /**
     * Конструктор для того, чтобы в имплементации можно было указать имя конфига
     * @param name - Имя конфига (без .yml)
     */
    public PluginConfig(String name){
        this.name = name;
    }

    /**
     * Конструктор для того, чтобы в имплементации можно было указать имя конфига
     * @param name - Имя конфига (без .yml)
     * @param defaultConfig - Имеет ли конфиг по умолчанию в .jar
     */
    public PluginConfig(String name, boolean defaultConfig){
        this.name = name;
        this.defaultConfig = false;
    }

    /**
     * Получение (если не существует - создание) экземпляра имплементации
     * @return - Экземпляр имплементации
     */
    public static PluginConfig getInstanceByClass(Class<?> c){
        var name = c.getSimpleName().toLowerCase();
        var instance = instances.getOrDefault(name, null);

        if (instance == null) {
            try {
                instance = (PluginConfig) c.getConstructor().newInstance();

                instances.put(name, instance);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return instance;
    }

    /**
     * Получение конфигурации имплементации
     * @return - Конфигурация имплементации
     */
    public static FileConfiguration getByClass(Class<?> c){
        var pc = getInstanceByClass(c);

        if (pc == null)
            return new YamlConfiguration();

        return ConfigManager.get(pc.getName()).getConfig();
    }

    /**
     * Сохранение конфигурации имплементации (АСИНХРОННО)
     */
    public static void saveAsynchronouslyByClass(Class<?> c){
        ConfigManager.getSavePool().add(c);
    }

    /**
     * Сохранение конфигурации имплементации
     */
    public static void saveByClass(Class<?> c){
        if (c == null)
            return;

        var pc = getInstanceByClass(c);

        if (pc == null)
            return;

        ConfigManager.save(pc.getName());
    }

    /**
     * Перезагрузка конфигурации имплементации
     */
    public static void reloadByClass(Class<?> c){
        var pc = getInstanceByClass(c);

        if (pc == null)
            return;

        ConfigManager.reload(pc.getName(), pc.isDefaultConfig());
    }

}
