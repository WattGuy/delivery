package me.wattguy.delivery.configs;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.extensions.PlaceholderAPIExtension;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;

/**
 * Конфигурационный предмет
 * Дата создания: 04:25 24.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
@ToString
public class ConfigItemStack extends ConfigItem {

    /**
     * Материал предмета
     */
    @Getter
    @Setter
    private Material material = Material.STONE;

    /**
     * Имя предмета (с переменными, с переведенными в параграф колоркодами)
     */
    @Getter
    @Setter
    private String name = null;

    /**
     * Описание предмета (с переменными, с переведенными в параграф колоркодами)
     */
    @Getter
    private final List<String> lore = new ArrayList<>();

    /**
     * Кастомная модель для предмета
     */
    @Getter
    @Setter
    private Integer customModelData = null;

    /**
     * Cached variables
     */
    private List<String> cachedVariables = null;

    /**
     * Get used variables in lore and name without '{' and '}'
     * @param force Force parsing without cache
     * @return List of variables, not null, if not found - size = 0
     */
    public List<String> getVariables(boolean force){
        if (!force && cachedVariables != null)
            return cachedVariables;

        cachedVariables = Utils.P.parseVariables((name != null ? name + " " : "") + String.join(" ", lore));

        return cachedVariables;
    }

    /**
     * Get used variables in lore and name without '{' and '}'
     * @return List of variables, not null, if not found - size = 0
     */
    public List<String> getVariables(){
        return getVariables(false);
    }

    /**
     * Преобразование в предмет
     * @param amount Количество предмета
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param inv Инвентарь (для последующей замены к примеру головы) (может быть null)
     * @param slot Слот в инвентаре (для последующей замены к примеру головы) (может быть null (0))
     * @param ps Переменные
     * @return Предмет
     */
    public ItemStack itemStack(int amount, Player p, Inventory inv, int slot, List<Utils.P> ps){
        var is = new ItemStack(material, amount);

        extendedItemStackInitialization(p, is);

        modify(is, amount, p, inv, slot, ps);

        return is;
    }

    /**
     * Преобразование в предмет
     * @param amount Количество предмета
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param inv Инвентарь (для последующей замены к примеру головы) (может быть null)
     * @param slot Слот в инвентаре (для последующей замены к примеру головы) (может быть null (0))
     * @param ps Переменные
     * @return Предмет
     */
    public ItemStack itemStack(int amount, Player p, Inventory inv, int slot, Utils.P... ps){
        var list = new ArrayList<Utils.P>();

        if (ps != null)
            list.addAll(Arrays.asList(ps));

        return itemStack(amount, p, inv, slot, list);
    }

    /**
     * Преобразование в предмет
     * @param amount Количество предмета
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param ps Переменные
     * @return Предмет
     */
    public ItemStack itemStack(int amount, Player p, List<Utils.P> ps){
        return itemStack(amount, p, null, 0, ps);
    }

    /**
     * Преобразование в предмет
     * @param amount Количество предмета
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param ps Переменные
     * @return Предмет
     */
    public ItemStack itemStack(int amount, Player p, Utils.P... ps){
        return itemStack(amount, p, null, 0, ps);
    }

    /**
     * Модифицировать уже имеющийся ItemStack до новой версии
     * @param is Айтемстак
     * @param amount Количество
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param inv Инвентарь (для последующей замены к примеру головы) (может быть null)
     * @param slot Слот в инвентаре (для последующей замены к примеру головы) (может быть null (0))
     * @param ps Переменные
     */
    public void modify(ItemStack is, int amount, Player p, Inventory inv, int slot, List<Utils.P> ps){
        var psExtension = extendedVariables(p);

        if (psExtension != null && psExtension.size() > 0)
            ps.addAll(psExtension);

        var replace_material = Utils.P.get("{replace_material}", ps);

        if (replace_material != null)
            ps.remove(replace_material);

        var customModelData = getCustomModelData();

        var replace_customModelData = Utils.P.get("{replace_cmd}", ps);

        if (replace_customModelData != null) {
            ps.remove(replace_customModelData);

            customModelData = (Integer) replace_customModelData.getValue();
        }

        var target_material = this.material;

        if (replace_material != null) {
            Material material = null;

            if (replace_material.getValue() instanceof String && !((String) replace_material.getValue()).trim().isEmpty())
                material = Utils.getEnumByName(((String) replace_material.getValue()).trim(), Material.class);
            else if (replace_material.getValue() instanceof Material)
                material = (Material) replace_material.getValue();

            if (material != null)
                target_material = material;
        }

        if (is.getType() != target_material)
            is.setType(target_material);

        if (has("amount"))
            amount = getInt("amount");

        var replace_amount = Utils.P.get("{replace_amount}", ps);

        if (replace_amount != null && replace_amount.getValue() instanceof Integer) {
            ps.remove(replace_amount);

            amount = (Integer) replace_amount.getValue();
        }

        if (is.getAmount() != amount)
            is.setAmount(amount);

        extendedItemStackModification(p, is);

        var meta = is.getItemMeta();

        if (meta != null) {
            extendedItemMetaModification(p, meta);

            psExtension = extendedVariables(p, meta);

            if (psExtension != null && psExtension.size() > 0)
                ps.addAll(psExtension);

            var name = this.name != null ? PlaceholderAPIExtension.replace(p, Utils.replace(this.name, ps)) : null;
            var rawLore = new ArrayList<>(ConfigItemStack.this.lore);

            for (var variable : new ArrayList<>(ps)) {
                if (!variable.isListVariable() || !(variable.getValue() instanceof List))
                    continue;

                Utils.replaceVariable(rawLore, variable.getKey(), (List<String>) variable.getValue());
            }

            var extendedLore = Utils.P.get("{extended_lore}", ps);

            if (extendedLore != null)
                ps.remove(extendedLore);

            var lore = PlaceholderAPIExtension.replace(p, Utils.replace(new ArrayList<String>(){{

                addAll(rawLore);

                if (extendedLore != null && extendedLore.getValue() instanceof List)
                    addAll((List<String>) extendedLore.getValue());

            }}, ps));

            if (name != null && !name.trim().isEmpty())
                meta.setDisplayName(name);
            else
                meta.setDisplayName(null);

            if (lore != null && lore.size() > 0 && !(lore.size() == 1 && lore.get(0).trim().isEmpty()))
                meta.setLore(lore);
            else
                meta.setLore(null);

            if (meta instanceof PotionMeta && has("potion_effects")){
                var pm = (PotionMeta) meta;

                Utils.potionFromConfig(getMapList("potion_effects"), pm);
            }

            for (var rawFlag : ItemFlag.values()){
                var flag = rawFlag.toString().toLowerCase();

                if (!has(flag) || !getBoolean(flag) || meta.hasItemFlag(rawFlag))
                    continue;

                meta.addItemFlags(rawFlag);
            }

            if (customModelData != null && (!meta.hasCustomModelData() || meta.getCustomModelData() != customModelData))
                meta.setCustomModelData(customModelData);

            if (has("player_head") && getBoolean("player_head") && meta instanceof SkullMeta && is.getType() == Material.PLAYER_HEAD && p != null)
                ((SkullMeta) meta).setOwningPlayer(p);

            if (has("texture") && meta instanceof SkullMeta && is.getType() == Material.PLAYER_HEAD){
                var profile = new GameProfile(UUID.randomUUID(), null);
                var propertyMap = profile.getProperties();
                var skinData = new Property("textures", getString("texture"));
                propertyMap.get("textures").clear();
                propertyMap.put("textures", skinData);

                try {
                    var profileField = meta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(meta, profile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (has("custom_head") && inv != null && meta instanceof SkullMeta && is.getType() == Material.PLAYER_HEAD) {
                var custom_name = Utils.replace(getString("custom_head"), ps);

                if (custom_name != null && !custom_name.trim().isEmpty()) {

                    Main.getSyncPool().add(() -> {
                        OfflinePlayer op = null;

                        try {
                            op = Bukkit.getOfflinePlayer(custom_name);
                        } catch (Exception ignored) { }

                        if (op != null) {
                            ((SkullMeta) meta).setOwningPlayer(op);

                            is.setItemMeta(meta);

                            inv.setItem(slot, is);
                        }
                    });

                }
            }

            is.setItemMeta(meta);
        }
    }

    /**
     * Модифицировать уже имеющийся предмет до новой версии
     * @param is Предмет
     * @param amount Количество
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param inv Инвентарь (для последующей замены к примеру головы) (может быть null)
     * @param slot Слот в инвентаре (для последующей замены к примеру головы) (может быть null (0))
     * @param ps Переменные
     */
    public void modify(ItemStack is, int amount, Player p, Inventory inv, int slot, Utils.P... ps){
        var list = new ArrayList<Utils.P>();

        if (ps != null)
            list.addAll(Arrays.asList(ps));

        modify(is, amount, p, inv, slot, list);
    }

    /**
     * Модифицировать уже имеющийся предмет до новой версии
     * @param is Предмет
     * @param amount Количество
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param ps Переменные
     */
    public void modify(ItemStack is, int amount, Player p, Utils.P... ps){
        modify(is, amount, p, null, 0, ps);
    }

    /**
     * Модифицировать уже имеющийся ItemStack до новой версии
     * @param is Айтемстак
     * @param amount Количество
     * @param p Игрок (если null, то не будет работать голова открывающего)
     * @param ps Переменные
     */
    public void modify(ItemStack is, int amount, Player p, List<Utils.P> ps){
        modify(is, amount, p, null, 0, ps);
    }

    /**
     * Расширенные переменные при модификации предмета
     * @param player Игрок
     * @return Переменные (возможен null)
     */
    public List<Utils.P> extendedVariables(Player player) {
        return null;
    }

    /**
     * Расширенные переменные при модификации предмета с метадатой
     * @param player Игрок
     * @param meta Метадата
     * @return Переменные (возможен null)
     */
    public List<Utils.P> extendedVariables(Player player, ItemMeta meta) {
        return null;
    }

    /**
     * Расширенная инициализация предмета
     * @param player Игрок
     * @param item Предмет
     */
    public void extendedItemStackInitialization(Player player, ItemStack item) {}

    /**
     * Расширенная модификация предмета
     * @param player Игрок
     * @param item Предмет
     */
    public void extendedItemStackModification(Player player, ItemStack item) {}

    /**
     * Расширенная модификация метадаты
     * @param player Игрок
     * @param meta Метадата
     */
    public void extendedItemMetaModification(Player player, ItemMeta meta) {}

    /**
     * Расширенная инициализация
     * @param map Карта значений
     * @return Перечисленные использованные значения (может быть null)
     */
    public List<String> extendedInitialization(Map<?, ?> map){
        return null;
    }

    @Override
    public List<String> initialization(Map<?, ?> map) {
        // Материал
        if (map.containsKey("material")) {
            Material material = null;

            try{
                material = Material.valueOf(((String) map.get("material")).toUpperCase());
            }catch(Exception ignored) { }

            if (material != null)
                setMaterial(material);
        }

        // Имя
        if (map.containsKey("name"))
            setName(Utils.toColor((String) map.get("name")));

        // Описание
        if (map.containsKey("lore")) {
            var lore = map.get("lore");

            if (lore instanceof List)
                getLore().addAll(Utils.toColor((List<String>) lore));
            else
                getLore().add(Utils.toColor((String) lore));

        }

        // Кастомная модель предмета
        if (map.containsKey("customModelData"))
            setCustomModelData((Integer) map.get("customModelData"));

        var list = new ArrayList<String>(){{

            add("material");
            add("name");
            add("lore");
            add("customModelData");

        }};

        var extendedInitialization = extendedInitialization(map);

        if (extendedInitialization != null && extendedInitialization.size() > 0)
            list.addAll(extendedInitialization);

        return list;
    }

}
