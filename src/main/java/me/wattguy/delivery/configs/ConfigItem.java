package me.wattguy.delivery.configs;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.var;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Основа конфигурационного предмета
 * Дата создания: 04:25 24.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
@ToString
public abstract class ConfigItem {

    /**
     * Секция предмета
     */
    @Getter
    @Setter
    private Map<?, ?> values = null;

    /**
     * Имеется ли в наличии значение
     * @param key Ключ
     * @return Имеется или нет
     */
    public boolean has(Object key){
        return values != null && values.containsKey(key);
    }

    /**
     * Получить объект по ключу (если нет, то null)
     * @param key Ключ
     * @return Объект
     */
    public Object get(Object key){
        if (values == null)
            return null;

        return values.getOrDefault(key, null);
    }

    /**
     * Достать секцию по ключу
     * @param key Ключ
     * @return Секция
     */
    public Map<?, ?> getMapList(Object key){
        return (Map<?, ?>) get(key);
    }

    /**
     * Достать строку по ключу
     * @param key Ключ
     * @return Строка
     */
    public String getString(Object key){
        return (String) get(key);
    }

    /**
     * Достать boolean по ключу
     * @param key Ключ
     * @return Boolean
     */
    public boolean getBoolean(Object key){
        return (boolean) get(key);
    }

    /**
     * Достать число по ключу
     * @param key Ключ
     * @return Число
     */
    public int getInt(Object key){
        return (int) get(key);
    }

    /**
     * Достать float по ключу
     * @param key Ключ
     * @return Float
     */
    public float getFloat(Object key){
        return (float) get(key);
    }

    /**
     * Достать double по ключу
     * @param key Ключ
     * @return Double
     */
    public double getDouble(Object key){
        return (double) get(key);
    }

    /**
     * Достать массив строк по ключу
     * @param key Ключ
     * @return Массив строк
     */
    public List<String> getStringList(Object key){
        return (List<String>) get(key);
    }

    /**
     * Инициализация
     * @param map Карта значений
     * @return Перечисленные использованные значения (может быть null)
     */
    public abstract List<String> initialization(Map<?, ?> map);

    /**
     * Выгрузить конфигурационный предмет из конфигурационной секции
     * @param section Конфигурационная секция
     * @return Измененный конфигурационный предмет
     */
    public ConfigItem fromConfig(ConfigurationSection section){
        Map<String, Object> map = new HashMap<>();

        for (var key : section.getKeys(true)) {
            if (section.isConfigurationSection(key))
                continue;

            map.put(key, section.get(key));
        }

        return fromConfig(map);
    }

    /**
     * Выгрузить конфигурационный предмет из карты значений
     * @param originalMap Карта значений
     * @return Измененный конфигурационный предмет
     */
    public ConfigItem fromConfig(Map<?, ?> originalMap){
        if (originalMap == null || originalMap.size() <= 0)
            return this;

        var map = new HashMap<>(originalMap);

        List<String> usedFields = new ArrayList<>();

        var usedInitialization = initialization(map);

        if (usedInitialization != null && usedInitialization.size() > 0)
            usedFields.addAll(usedInitialization);

        // Удаляем всё лишнее для оптимизации пространства
        for (var s : usedFields){

            map.remove(s);

        }

        // Секция
        setValues(map);

        return this;
    }

}
