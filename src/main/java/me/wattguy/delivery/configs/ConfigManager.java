package me.wattguy.delivery.configs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;
import me.wattguy.delivery.utils.Pool;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Класс, проводящий операции с конфигами в плагине
 * @author WattGuy
 * @version 1.0
 */
public class ConfigManager implements Initializable, Unregistrable {

    /**
     * Список всех конфигов (в том числе сохраняемых)
     * Формат - Map<[название конфига без .yml], ConfigurationInfo (полная информация о файле)>
     */
    private static final Map<String, ConfigInfo> configs = new HashMap<>();

    /**
     * Пул асинхронных сохранений конфигураций
     */
    @Getter
    private static final Pool<Class<?>> savePool = new Pool<>(10L, 1L, (configs) -> {

        // Перебор конфигураций
        for (var c : configs) {

            // Сохранение конфигурации
            PluginConfig.saveByClass(c);

        }

    });

    /**
     * Инициализация всех конфигов
     */
    @Override
    @Settings(priority = Settings.Priority.LOWEST)
    public void initialize(){
        configs.clear();

        // Инициализируем все конфиги
        for (var config : getAllConfigs()) {

            // Имя конфига (без .yml)
            String name = null;

            // Имеет ли дефолтный конфиг
            boolean defaultConfig = true;

            // Достаём переменные с помощью рефлексии
            try {
                var instance = config.getConstructor().newInstance();

                name = (String) config.getMethod("getName").invoke(instance);
                defaultConfig = (boolean) config.getMethod("isDefaultConfig").invoke(instance);
            }catch(Exception ignored) { }

            // Если не указано имя - уходим
            if (name == null)
                continue;

            // Перезагружаем конфиг (то же что и загрузить)
            reload(name, defaultConfig);
        }
    }

    @Override
    public void unregister() {

        // Выключаем пул сохранений конфигов
        if (ConfigManager.getSavePool() != null)
            ConfigManager.getSavePool().stop();

    }

    /**
     * Получить все классы-конфиги с помощью рефлексии
     * @return Список всех конфигов-классов
     */
    public static Set<Class<? extends PluginConfig>> getAllConfigs(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(ConfigManager.class.getPackage().getName(), 1), PluginConfig.class);

    }

    /**
     * Получить конфигурацию по имени конфига
     * @param config Имя файла (без .yml)
     * @return Информация о конфиге
     */
    public static ConfigInfo get(String config){
        return configs.get(config);
    }

    /**
     * Сохранение конфигурации
     * @param config Имя файла (без .yml)
     */
    public static void save(String config){
        var info = configs.getOrDefault(config, null);

        if (info == null)
            return;

        save(info.getConfig(), info.getName() + ".yml");
    }

    /**
     * Перезагрузка конфигурации
     * @param config Имя файла (без .yml)
     * @param defaultConfig Имеет ли конфиг по умолчанию в .jar
     */
    public static void reload(String config, boolean defaultConfig){
        var entry = load(config, defaultConfig);

        if (entry != null)
            configs.put(config, new ConfigInfo(config, entry.getKey(), entry.getValue()));
    }

    /**
     * Сохранение конфигурации
     * @param config Конфигурация
     * @param name Имя файла (с .yml)
     */
    public static void save(FileConfiguration config, String name){
        if (name.equalsIgnoreCase("config.yml")){

            Main.getInstance().saveConfig();
            return;

        }

        var f = getFile(name);

        try {
            // Создаём директории
            f.getParentFile().mkdirs();

            // Сохраняем
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e){

            // Создаём пустой файл
            try {
                var writer = new PrintWriter(f);
                writer.print("");
                writer.close();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

        }
    }

    /**
     * Получение файла из папки плагина по имени
     * @param name Имя файла (с .yml)
     */
    private static File getFile(String name){
        return new File(Main.getInstance().getDataFolder() + File.separator + name);
    }

    /**
     * Загрузка конфига из (.jar | файла в папке плагина)
     * @param name Имя файла (.yml добавляется автоматически, если отсутствует)
     * @param defaultConfig Имеет ли конфиг по умолчанию в .jar
     */
    private static Map.Entry<File, FileConfiguration> load(String name, boolean defaultConfig){
        if (!name.contains(".yml")){
            name += ".yml";
        }

        var file = getFile(name);
        FileConfiguration config;

        if (name.equalsIgnoreCase("config.yml")){
            if (defaultConfig && !file.exists()) {
                Main.getInstance().getConfig().options().copyDefaults();
                Main.getInstance().saveDefaultConfig();
            }

            Main.getInstance().reloadConfig();

            config = Main.getInstance().getConfig();
        }else {
            if (defaultConfig && !file.exists()){
                try {
                    Main.getInstance().saveResource(name, false);
                } catch (Exception e) {
                    Main.getInstance().getLogger().warning("Failed to save resource \"" + name + "\"");
                    e.printStackTrace();

                    return null;
                }
            }

            config = YamlConfiguration.loadConfiguration(file);
        }

        return new AbstractMap.SimpleEntry<>(file, config);
    }

    /**
     * Класс-информация о конфиге
     */
    @AllArgsConstructor
    public static class ConfigInfo {

        /**
         * Имя конфига (без .yml)
         */
        @Getter
        private final String name;

        /**
         * Файл конфигурации
         */
        @Getter
        private final File file;

        /**
         * Файл
         */
        @Getter
        private final FileConfiguration config;

    }

}
