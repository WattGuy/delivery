package me.wattguy.delivery.configs;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Массив конфигов по директории
 * Дата создания: 18:40 16.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
@NoArgsConstructor
public class ConfigList implements Initializable {

    /**
     * All menus configurations
     */
    @Getter
    private static ConfigList menus = null;

    @Settings(priority = Settings.Priority.LOW)
    @Override
    public void initialize() {
        if (menus == null)
            menus = new ConfigList("menus");

        menus.copyFilesFromJar("menus");
        menus.initialize(new String[0]);
    }

    /**
     * Директория, в которой нужно собирать данные о конфигурациях
     */
    @Getter
    private File directory;

    /**
     * Карта всех конфигов
     */
    private Map<String, FileConfiguration> configs = new HashMap<>();

    /**
     * Конструктор
     * @param path Директория в папке плагина
     */
    public ConfigList(String path){
        this.directory = new File(Main.getInstance().getDataFolder().getAbsolutePath() + File.separator + path + File.separator);

        directory.mkdirs();
    }

    /**
     * Скопировать файлы, если они не существуют из JAR
     * @param jarPath Директория в JAR
     */
    public void copyFilesFromJar(String jarPath){
        if (jarPath == null || directory == null)
            return;

        Utils.copyFilesFromJar(jarPath, directory, false);
    }

    /**
     * Инициализация конфигов
     */
    public void initialize(String... excludes){
        configs.clear();

        if (directory == null)
            return;

        var files = directory.listFiles();

        if (files == null || files.length == 0)
            return;

        for (var f : files) {
            if (f.isDirectory())
                continue;

            var entry = Utils.getFileEntry(f);

            if (!(entry.getValue().equalsIgnoreCase("yaml") || entry.getValue().equalsIgnoreCase("yml")))
                continue;

            if (Utils.containsStringInArray(entry.getKey(), excludes))
                continue;

            configs.put(entry.getKey().toLowerCase(), YamlConfiguration.loadConfiguration(f));
        }
    }

    /**
     * Получить список всех конфигов
     * @return Список <Имя конфига, Конфигурация>
     */
    public Set<Map.Entry<String, FileConfiguration>> list(){
        return configs.entrySet();
    }

    /**
     * Получить конфиг по имени файла
     * @param name Имя конфига
     * @return Конфигурация
     */
    public FileConfiguration config(String name){
        return configs.getOrDefault(name.toLowerCase(), null);
    }

}
