package me.wattguy.delivery.configs.list;

import me.wattguy.delivery.configs.PluginConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.lang.invoke.MethodHandles;

/**
 * Реализация конфига - config.yml
 *
 * @author WattGuy
 * @version 1.0
 */
public class Config extends PluginConfig {

    /**
     * Инициализация конфига с именем config (.yml)
     */
    public Config() {
        super("config");
    }

    // <!-- ПЕРЕИМПЛЕМЕНТАЦИЯ РЕФЛЕКСИИ --!>

    public static FileConfiguration get(){
        return getByClass(MethodHandles.lookup().lookupClass());
    }

    public static void save(){
        saveByClass(MethodHandles.lookup().lookupClass());
    }

    public static void saveAsynchronously(){
        saveAsynchronouslyByClass(MethodHandles.lookup().lookupClass());
    }

    public static void reload(){
        reloadByClass(MethodHandles.lookup().lookupClass());
    }

    // <!-- ПЕРЕИМПЛЕМЕНТАЦИЯ РЕФЛЕКСИИ --!>

}
