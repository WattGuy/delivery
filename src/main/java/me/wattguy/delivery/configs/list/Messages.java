package me.wattguy.delivery.configs.list;

import me.wattguy.delivery.configs.PluginConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.lang.invoke.MethodHandles;

/**
 * Реализация конфига - messages.yml
 *
 * @author WattGuy
 * @version 1.0
 */
public class Messages extends PluginConfig {

    /**
     * Инициализация конфига с именем messages (.yml)
     */
    public Messages() {
        super("messages");
    }

    // <!-- ПЕРЕИМПЛЕМЕНТАЦИЯ РЕФЛЕКСИИ --!>

    public static FileConfiguration get(){
        return getByClass(MethodHandles.lookup().lookupClass());
    }

    public static void save(){
        saveByClass(MethodHandles.lookup().lookupClass());
    }

    public static void saveAsynchronously(){
        saveAsynchronouslyByClass(MethodHandles.lookup().lookupClass());
    }

    public static void reload(){
        reloadByClass(MethodHandles.lookup().lookupClass());
    }

    // <!-- ПЕРЕИМПЛЕМЕНТАЦИЯ РЕФЛЕКСИИ --!>

}
