package me.wattguy.delivery.guis;

import lombok.*;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.ConfigItemStack;
import me.wattguy.delivery.guis.list.GUI;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.listeners.PluginListener;
import me.wattguy.delivery.utils.CommandEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Класс-абстракция для внутриигровых инвентарных интерфейсов
 * Дата создания: 14:48 17.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public abstract class PluginGUI extends PluginListener implements InventoryHolder {

    /**
     * Супер инвентарь
     */
    @Getter
    private SuperInventory superInventory;

    /**
     * Конфигурация GUI
     */
    @Getter
    private Configuration configuration = null;

    /**
     * Inventory modifiers
     */
    @Getter
    private final List<InventoryModifier> inventoryModifiers = new ArrayList<>();

    /**
     * Inventory interceptors
     */
    @Getter
    private final List<InventoryInterceptor> interceptors = new ArrayList<>();

    /**
     * Особенности по игрокам
     */
    private final Map<String, Map<String, Object>> features = new HashMap<>();

    /**
     * Конструктор получения GUI по конфигурации
     * @param config Конфигурация GUI
     * @param playerOriented Прикреплен ли инвентарь к игрокам (1 инвентарь = 1 игрок)
     */
    public PluginGUI(Configuration config, boolean playerOriented){
        initialize(config, playerOriented);
    }

    /**
     * Конструктор получения GUI по файлу конфигурации и пути до описания GUI
     * @param config Файл конфигурации
     * @param path Путь
     * @param playerOriented Прикреплен ли инвентарь к игрокам (1 инвентарь = 1 игрок)
     */
    public PluginGUI(FileConfiguration config, String path, boolean playerOriented){
        initialize(Configuration.fromConfig(config, path), playerOriented);
    }

    /**
     * Конструктор получения GUI по файлу конфигурации в корне
     * @param config Файл конфигурации
     * @param playerOriented Прикреплен ли инвентарь к игрокам (1 инвентарь = 1 игрок)
     */
    public PluginGUI(FileConfiguration config, boolean playerOriented){
        initialize(Configuration.fromConfig(config), playerOriented);
    }

    /**
     * Инициализатор по конфигурации GUI
     * @param config Конфигурация GUI
     */
    private void initialize(Configuration config, boolean playerOriented){
        this.configuration = config;
        this.superInventory = new SuperInventory(this, playerOriented);
    }

    /**
     * Обновление всех слотов исходя из конфигурации
     * @param p Игрок
     */
    public void update(Player p){
        awakeUpdateSync(p);

        update(superInventory.get(p), p);
    }

    /**
     * Обновление всех слотов исходя из конфигурации
     * @param inv Инвентарь
     * @param p Игрок
     */
    public void update(Inventory inv, Player p){
        Set<Integer> affected = new HashSet<>();

        var globalPS = new ArrayList<Utils.P>();

        for (var modifier : inventoryModifiers) {
            var ps = modifier.itemVariables(p);

            if (ps == null || ps.size() <= 0)
                continue;

            globalPS.addAll(ps);
        }

        for (var interceptor : interceptors) {
            var ps = interceptor.itemVariables(p);

            if (ps == null || ps.size() <= 0)
                continue;

            globalPS.addAll(ps);
        }

        var itemVariables = itemVariables(p);

        if (itemVariables != null && itemVariables.size() > 0)
            globalPS.addAll(itemVariables);

        var itemList = itemList(p);

        for (var modifier : inventoryModifiers)
            modifier.itemList(this, itemList);

        for (var interceptor : interceptors)
            interceptor.itemList(itemList);

        for (var i : itemList) {
            var totalPS = new ArrayList<>(globalPS);
            var specificPS = itemVariables(p, i);

            if (specificPS != null)
                totalPS.addAll(specificPS);

            for (var modifier : inventoryModifiers) {
                var ps = modifier.itemVariables(p, i);

                if (ps == null || ps.size() <= 0)
                    continue;

                totalPS.addAll(ps);
            }

            for (var interceptor : interceptors) {
                var ps = interceptor.itemVariables(p, i);

                if (ps == null || ps.size() <= 0)
                    continue;

                totalPS.addAll(ps);
            }

            var amount = amount(p, i);

            if (amount <= 0)
                amount = 1;

            if (inventoryModifiers.size() > 0 || interceptors.size() > 0) {
                var uploading = new UploadingInfo(i, inv, amount, p, new int[]{ i.getSlot() }, totalPS);
                var cancel = false;

                for (var modifier : inventoryModifiers) {
                    if (!modifier.uploading(this, uploading, affected))
                        continue;

                    cancel = true;
                }

                for (var interceptor : interceptors) {
                    if (!interceptor.uploading(uploading, affected))
                        continue;

                    cancel = true;
                }

                if (cancel)
                    continue;

                for (var slot : uploading.getSlots()) {
                    var already = inv.getItem(slot);

                    if (already == null)
                        inv.setItem(slot, i.itemStack(uploading.getAmount(), p, inv, slot, uploading.getTotalParameters()));
                    else
                        i.modify(already, uploading.getAmount(), p, inv, slot, uploading.getTotalParameters());
                }
            }else{
                var already = inv.getItem(i.getSlot());

                if (already == null)
                    inv.setItem(i.getSlot(), i.itemStack(amount, p, inv, i.getSlot(), totalPS));
                else
                    i.modify(already, amount, p, inv, i.getSlot(), totalPS);
            }

            affected.add(i.getSlot());
        }

        for (var modifier : inventoryModifiers) {
            var modifierAffected = modifier.postUpdate(this, inv, p);

            if (modifierAffected != null && modifierAffected.size() > 0)
                affected.addAll(modifierAffected);
        }

        for (var interceptor : interceptors) {
            var modifierAffected = interceptor.postUpdate(inv, p);

            if (modifierAffected != null && modifierAffected.size() > 0)
                affected.addAll(modifierAffected);
        }

        var postAffected = postUpdate(inv, p);

        if (postAffected != null && postAffected.size() > 0)
            affected.addAll(postAffected);

        for(int i = 0; i < inv.getSize(); i++){
            if (affected.contains(i))
                continue;

            inv.setItem(i, new ItemStack(Material.AIR));
        }
    }

    public void updateAll(){
        var already = new HashSet<String>();

        for (var inv : new ArrayList<>(superInventory.getInventories())) {
            if (inv == null || inv.getViewers().size() <= 0)
                continue;

            for (var viewer : inv.getViewers()) {
                if (!(viewer instanceof Player) || already.contains(viewer.getName().toLowerCase()))
                    continue;

                updateAsync((Player) viewer);
                already.add(viewer.getName().toLowerCase());
            }
        }
    }

    /**
     * Uploading item info
     */
    @Data
    @AllArgsConstructor
    public static class UploadingInfo {

        private Item item;

        private Inventory inventory;

        private int amount;

        private Player player;

        private int[] slots;

        private List<Utils.P> totalParameters;

    }

    /**
     * Открыть инвентарь игроку
     * @param p Игрок
     */
    public void open(Player p){

        updateAsync(p).whenComplete((inv, throwable) -> {

            Main.getFoliaLib().getImpl().runNextTick((task) -> {
                p.openInventory(inv);
            });

        });

    }

    /**
     * Закрыть инвентарь у игрока
     * @param viewer Игрок
     */
    public void close(HumanEntity viewer){

        Utils.closeInventory(
                viewer,
                inv -> inv != null && inv.getHolder() instanceof PluginGUI && inv.getHolder().equals(PluginGUI.this)
        );

    }

    /**
     * Закрыть все инвентари
     */
    public void closeAll(){
        if (superInventory == null)
            return;

        for (var inv : superInventory.list()) {
            if (inv == null || inv.getViewers().size() == 0)
                continue;

            for (var viewer : inv.getViewers()) {
                close(viewer);
            }
        }
    }

    /**
     * Обновление всех слотов исходя из конфигурации асинхронно
     * @param inv Инвентарь
     * @param p Игрок
     */
    public CompletableFuture<Inventory> updateAsync(Inventory inv, Player p){
        var task = new CompletableFuture<Inventory>();

        Main.getFoliaLib().getImpl().runAsync((task2) -> {
            update(inv, p);

            task.complete(inv);
        });

        return task;
    }

    /**
     * Включено ли постороннее свойство (boolean)
     * @param property Имя свойства
     * @return Включено или нет
     */
    public boolean isPropertyEnabled(Item item, String property){
        if (item == null)
            return false;

        return item.has(property) && item.getBoolean(property);
    }

    /**
     * Все включенные посторонние свойства (boolean)
     * @return Свойства, если нет - Set, где size = 0
     */
    public Set<String> getPropertiesEnabled(Item item){
        Set<String> result = new HashSet<>();

        if (item == null)
            return result;

        for (var entry : item.getValues().entrySet()) {
            if (!(entry.getKey() instanceof String) || !(entry.getValue() instanceof Boolean))
                continue;

            if (!(Boolean) entry.getValue())
                continue;

            result.add((String) entry.getKey());
        }

        return result;
    }

    /**
     * Обновление всех слотов исходя из конфигурации асинхронно
     * @param p Игрок
     */
    public CompletableFuture<Inventory> updateAsync(Player p){
        awakeUpdateSync(p);

        return updateAsync(superInventory.get(p), p);
    }

    /**
     * Установить новое имя инвентарю
     * @param p Игрок
     * @param newTitle Новое имя
     */
    public Inventory renameSync(Player p, String newTitle){
        return getSuperInventory().rename(p, newTitle);
    }

    /**
     * Перезагрузить имя инвентарю (для установки новых переменных)
     * @param p Игрок
     */
    public Inventory refreshNameSync(Player p){
        return renameSync(p, configuration.getTitle());
    }

    /**
     * Список предметов для игрока при обновлении
     * @param p Игрок
     * @return Список предметов
     */
    public Collection<Item> itemList(Player p){ return new HashSet<>(configuration.items); }

    /**
     * Ивент, срабатываемый при запуске апдейта до образования инвентаря
     * @param p Игрок
     */
    public void awakeUpdateSync(Player p) { }

    /**
     * Ивент, срабатываемый при нажатии
     * @param e Ивент
     * @param p Игрок, который нажал
     * @param itemStack Баккит-предмет
     * @param item GUI предмет
     * @param slot Слот
     */
    public void click(InventoryInteractEvent e, Player p, @Nullable ItemStack itemStack, Item item, int slot) { }

    /**
     * Ивент, срабатываемый при отправке команд
     * @param p Игрок
     * @param item Предмет
     * @param e Ивент командный
     */
    public void command(Player p, Item item, CommandEvent e){ }

    /**
     * Ивент, срабатываемый после обновления
     * @param inv Обновляемый инвентарь
     * @param p Игрок
     */
    public List<Integer> postUpdate(Inventory inv, Player p) { return null; }

    /**
     * Переменные по конкретному предмету и игроку
     * @param p Игрок
     * @param item Предмет
     * @return Список переменных (возможен null)
     */
    public List<Utils.P> itemVariables(Player p, Item item) { return null; }

    /**
     * Переменные ко всем предметам и игроку
     * @param p Игрок
     * @return Список переменных (возможен null)
     */
    public List<Utils.P> itemVariables(Player p) { return null; }

    /**
     * Переменные по заголовку инвентаря и игроку
     * @param p Игрок
     * @return Список переменных (возможен null)
     */
    public List<Utils.P> inventoryTitleVariables(Player p) { return null; }

    /**
     * Переменные по команде и предмету
     * @param p Игрок
     * @param prefix Префикс
     * @param command Команда
     * @param item Предмет
     * @return Список переменных (возможен null)
     */
    public List<Utils.P> commandVariables(Player p, CommandEvent.Prefix prefix, String command, Item item) { return null; }

    /**
     * Количество предмета по предмету и игроку
     */
    public int amount(Player p, Item item) { return 1; }

    /**
     * Обработка ивента клика на то, наш ли это инвентарь, если да то вызываем метод.
     */
    @EventHandler
    public void click(InventoryClickEvent e){

        // Вызываем обработчик
        clickEventHandler(e);

    }

    /**
     * Реализация обработки ивента клика по предмету
     * @param e Ивент клика
     */
    public void clickEventHandler(InventoryClickEvent e){
        var r = isOur(e);

        if (!r.isAlive())
            return;

        e.setCancelled(true);

        var p = (Player) e.getWhoClicked();

        ItemStack itemStack = null;

        try {

            if (e.getClickedInventory() != null)
                itemStack = e.getClickedInventory().getItem(e.getRawSlot());

        }catch(Exception ignored) { }

        if (!r.isOur()) {
            notOurClicks(e,
                    p,
                    itemStack,
                    e.getRawSlot()
            );

            return;
        }

        Item item = null;

        for(var i : itemList(p)){
            if (i.getSlot() != e.getRawSlot())
                continue;

            item = i;
            break;
        }

        simulateClick(
                e,
                p,
                itemStack,
                item,
                e.getRawSlot()
        );
    }

    /**
     * Обработка ивента перетаскивания на то, наш ли это инвентарь, если да то вызываем метод.
     */
    @EventHandler
    public void onDrag(InventoryDragEvent e){

        // Обработчик переноса
        dragEventHandler(e);

    }

    /**
     * Реализация обработки ивента переноса предметов (интерпретация идет в обычный клик)
     * @param e Ивент переноса
     */
    public void dragEventHandler(InventoryDragEvent e){
        var r = isOur(e);

        if (!r.isAlive())
            return;

        e.setCancelled(true);

        if (e.getRawSlots().size() <= 0)
            return;

        var rawSlot = e.getRawSlots().stream().findFirst().orElse(null);

        if (rawSlot == null)
            return;

        var p = (Player) e.getWhoClicked();

        if (!r.isOur()){
            notOurClicks(e,
                    (Player) e.getWhoClicked(),
                    e.getView().getBottomInventory().getItem(rawSlot),
                    rawSlot
            );

            return;
        }

        Item item = null;

        for(var i : itemList(p)){
            if (i.getSlot() != rawSlot)
                continue;

            item = i;
            break;
        }

        simulateClick(
                e,
                (Player) e.getWhoClicked(),
                e.getInventory().getItem(rawSlot),
                item,
                rawSlot
        );
    }

    /**
     * Execute click modifiers for not our click
     * @param e Event
     * @param p Player
     * @param itemStack Bukkit item
     * @param slot Raw slot
     */
    private void notOurClicks(InventoryInteractEvent e, Player p, ItemStack itemStack, int slot){

        for(int i = 0; i < Settings.Priority.values().length; i++){
            var list = GUIManager.getClickModifiers().getOrDefault(Settings.Priority.values()[i], null);

            if (list == null || list.size() <= 0)
                continue;

            var was = false;

            for (var instance : list) {
                if (instance instanceof InventoryModifier || !instance.notOurClick(this, e, p, itemStack, slot))
                    continue;

                was = true;
                break;
            }

            if (was)
                break;
        }

        for (var modifier : inventoryModifiers) {
            if (!modifier.notOurClick(this, e, p, itemStack, slot))
                continue;

            break;
        }

        for (var interceptor : interceptors) {
            if (!interceptor.notOurClick(e, p, itemStack, slot))
                continue;

            break;
        }

    }

    /**
     * Set items
     * @param player Player
     * @param inventory Inventory
     * @param items Items
     * @param affected Affected (can be null)
     */
    public void set(Player player, Inventory inventory, Collection<Item> items, List<Integer> affected){

        for (var i : items) {
            var item = inventory.getItem(i.getSlot());

            if (item == null || item.getType() == Material.AIR) {
                item = i.itemStack(1, player);

                inventory.setItem(i.getSlot(), item);
            } else
                i.modify(item, 1, player);

            if (affected != null)
                affected.add(i.getSlot());
        }

    }

    /**
     * Set items
     * @param player Player
     * @param inventory Inventory
     * @param items Items
     */
    public void set(Player player, Inventory inventory, Collection<Item> items){
        set(player, inventory, items, null);
    }

    /**
     * Set item
     * @param player Player
     * @param inventory Inventory
     * @param item Item
     * @param affected Affected slots (can be null)
     */
    public void set(Player player, Inventory inventory, Item item, List<Integer> affected){
        set(player, inventory, Collections.singletonList(item), affected);
    }

    /**
     * Set item
     * @param player Player
     * @param inventory Inventory
     * @param item Item
     */
    public void set(Player player, Inventory inventory, Item item){
        set(player, inventory, item, null);
    }

    /**
     * Обработка логики клика
     * @param e Ивент
     * @param p Игрок
     * @param itemStack Предмет Bukkit
     * @param item Предмет GUI
     * @param slot Слот
     */
    public void simulateClick(InventoryInteractEvent e, Player p, ItemStack itemStack, Item item, int slot){
        if (item != null && item.has("permission") && !p.hasPermission(item.getString("permission"))) {
            Utils.send(p, "not_enough_permission");
            return;
        }

        click(e, p, itemStack, item, slot);

        for(int i = 0; i < Settings.Priority.values().length; i++){
            var list = GUIManager.getClickModifiers().getOrDefault(Settings.Priority.values()[i], null);

            if (list == null || list.size() <= 0)
                continue;

            var was = false;

            for (var instance : list) {
                if (instance instanceof InventoryModifier || !instance.click(this, e, p, itemStack, item, slot))
                    continue;

                was = true;
                break;
            }

            if (was)
                break;
        }

        for (var modifier : inventoryModifiers) {
            if (!modifier.click(this, e, p, itemStack, item, slot))
                continue;

            break;
        }

        for (var interceptor : interceptors) {
            if (!interceptor.click(e, p, itemStack, item, slot))
                continue;

            break;
        }

        if (item == null)
            return;

        if (item.has("open"))
            GUI.open(p, item.getString("open"));

        if (isPropertyEnabled(item, "close"))
            close(p);

        List<String> commands = null;

        if (item.has("command"))
            commands = new ArrayList<String>() {{
                add(item.getString("command"));
            }};
        else if (item.has("commands"))
            commands = item.getStringList("commands");

        if (commands == null)
            return;

        var commandEvent = new CommandEvent(commands, this, item);

        command(p, item, commandEvent);

        if (commandEvent.isCancelled())
            return;

        commandEvent.execute(p);
    }

    /**
     * Имплементация InventoryHolder метода
     * @return null
     */
    public Inventory getInventory() {
        return null;
    }

    /**
     * Наш ли инвентарь по Click ивенту
     * @param e Click ивент
     * @return Наш или нет
     */
    public Response isOur(InventoryClickEvent e){
        var r = isOur(e.getClickedInventory());

        if (r)
            return new Response(true);
        else if (e.isShiftClick()){
            var sr = new Response(isOur(e.getInventory()));

            sr.setOur(Objects.equals(e.getClickedInventory(), e.getInventory()));

            if (sr.isAlive())
                return sr;
        }

        return new Response(false);
    }

    /**
     * Наш ли инвентарь по Drag ивенту
     * @param e Drag ивент
     * @return Наш или нет
     */
    public Response isOur(InventoryDragEvent e){
        var r = isOur(e.getInventory());

        if (r)
            return new Response(true, mineByClick(e.getInventory().getSize(), e.getRawSlots()));

        return new Response(false, false);
    }

    /**
     * Наш ли инвентарь по заденутым слотам
     * @param size Размер инвентаря
     * @param raw Сырые заденутые слоты
     * @return Наш или нет
     */
    public boolean mineByClick(int size, Set<Integer> raw){
        for(var i : raw){

            if (i < size)
                return true;

        }

        return false;
    }

    /**
     * Наш ли инвентарь
     * @param inv Инвентарь на проверку
     * @return Наш или нет
     */
    public boolean isOur(Inventory inv){
        return inv != null && inv.getHolder() instanceof PluginGUI && inv.getHolder().equals(this);
    }

    /**
     * Получение реализации по имени многогранности
     * @param c Класс реализации
     * @param name Имя многогранности
     * @return Реализация
     */
    public static PluginGUI getByClass(Class<?> c, String name){
        var map = GUIManager.guis.getOrDefault(c, null);

        if (map == null) {
            map = new HashMap<>();
            GUIManager.guis.put(c, map);
        }

        if (!map.containsKey(name.toLowerCase())){
            Constructor constructor = null;

            try{
                constructor = c.getConstructor(String.class);
            }catch(Exception ignored) { }

            if (constructor == null)
                return null;

            PluginGUI instance = null;

            try {
                instance = (PluginGUI) constructor.newInstance(name);
            }catch(Exception ignored) { }

            if (instance == null)
                return null;

            GUIManager.addInventoryModifiers(instance);

            instance.register();
            map.put(name.toLowerCase(), instance);

            return instance;
        }

        return map.get(name.toLowerCase());
    }

    /**
     * Получение реализации по имени многогранности
     * @param className Имя класса
     * @param name Имя многогранности
     * @return Реализация
     */
    public static PluginGUI getByClassName(String className, String name){
        Map<String, PluginGUI> map = null;

        for (var entry : GUIManager.guis.entrySet()) {
            if (entry.getKey() == null || !entry.getKey().getSimpleName().equalsIgnoreCase(className))
                continue;

            map = entry.getValue();
            break;
        }

        if (map == null || map.size() <= 0)
            return null;

        return map.getOrDefault(name.toLowerCase(), null);
    }

    /**
     * Получение реализации
     * @param c Класс реализации
     * @return Реализация
     */
    public static PluginGUI getByClass(Class<?> c){
        return getByClass(c, "standard");
    }

    /**
     * Особенности по имени игрока
     * @param name Имя игрока
     * @return Особенности
     */
    public Map<String, Object> getFeatures(String name){
        return features.getOrDefault(name.toLowerCase(), new HashMap<>());
    }

    /**
     * Установить особенности по имени игрока
     * @param name Имя игрока
     */
    public void setFeatures(String name, Map<String, Object> features){
        this.features.put(name.toLowerCase(), features);
    }

    /**
     * Имеет ли особенности по имени игрока
     * @param name Имя игрока
     * @return true - если имеет, иначе - false
     */
    public boolean hasFeatures(String name){
        return this.features.containsKey(name.toLowerCase());
    }

    /**
     * Удалить особенности по имени игрока
     * @param name Имя игрока
     */
    public void removeFeatures(String name){
        this.features.remove(name.toLowerCase());
    }

    /**
     * Установить особенность по имени игрока
     * @param name Имя игрока
     * @param featureName Имя особенности
     * @param featureValue Значение особенности
     */
    public void setFeature(String name, String featureName, Object featureValue){
        var features = getFeatures(name);

        features.put(featureName.toLowerCase(), featureValue);

        setFeatures(name, features);
    }

    /**
     * Установить особенность по игроку
     * @param p Игрок
     * @param featureName Имя особенности
     * @param featureValue Значение особенности
     */
    public void setFeature(HumanEntity p, String featureName, Object featureValue){
        setFeature(p.getName(), featureName, featureValue);
    }

    /**
     * Получить особенность по имени игрока и имени особенности
     * @param name Имя игрока
     * @param featureName Имя особенности
     */
    public <T> T getFeature(String name, String featureName){
        return (T) getFeatures(name).getOrDefault(featureName.toLowerCase(), null);
    }

    /**
     * Получить особенность по игроку и имени особенности
     * @param p Игрок
     * @param featureName Имя особенности
     */
    public <T> T getFeature(HumanEntity p, String featureName){
        return (T) getFeature(p.getName(), featureName);
    }

    /**
     * Имеется ли особенность по имени игрока и имени особенности
     * @param name Имя игрока
     * @param featureName Имя особенности
     */
    public boolean hasFeature(String name, String featureName){
        return getFeatures(name).containsKey(featureName.toLowerCase());
    }

    /**
     * Имеется ли особенность по игроку и имени особенности
     * @param p Игрок
     * @param featureName Имя особенности
     */
    public boolean hasFeature(HumanEntity p, String featureName){
        return hasFeature(p.getName(), featureName);
    }

    /**
     * Удалить особенность по имени игрока и имени особенности
     * @param name Имя игрока
     * @param featureName Имя особенности
     */
    public void removeFeature(String name, String featureName){
        var features = getFeatures(name);

        features.remove(featureName.toLowerCase());

        setFeatures(name, features);
    }

    /**
     * Удалить особенность по игроку и имени особенности
     * @param p Игрок
     * @param featureName Имя особенности
     */
    public void removeFeature(HumanEntity p, String featureName){
        removeFeature(p.getName(), featureName);
    }

    /**
     * Ориентированные инвентари
     */
    public static class SuperInventory {

        /**
         * GUI, к которому прикреплены инвентари
         */
        @Getter
        private final PluginGUI gui;

        /**
         * Прикреплен ли каждый игрок к своему инвентарю? (позволяет делать персональные переменные)
         */
        @Getter
        private boolean playerOriented = false;

        /**
         * При ориентированном - карта,
         * При статичном - инвентарь
         */
        private Object object = null;

        /**
         * Конструктор супер инвентаря
         * @param gui GUI
         * @param playerOriented Ориентированные ли инвентари
         */
        public SuperInventory(PluginGUI gui, boolean playerOriented){
            this.gui = gui;
            setPlayerOriented(playerOriented);
        }

        /**
         * Set Player oriented
         * @param playerOriented Player oriented or not
         */
        public void setPlayerOriented(boolean playerOriented) {
            this.playerOriented = playerOriented;

            if (playerOriented)
                object = new HashMap<String, Inventory>();
        }

        public Collection<Inventory> getInventories(){
            if (playerOriented)
                return ((Map<String, Inventory>) object).values();
            else
                return new ArrayList<Inventory>(){{
                    add((Inventory) object);
                }};
        }

        /**
         * Получить список всех инвентарей
         * @return Все инвентари
         */
        public List<Inventory> list(){
            List<Inventory> result = new ArrayList<>();

            if (object == null)
                return result;

            if (playerOriented)
                result.addAll(((Map<String, Inventory>) object).values());
            else
                result.add((Inventory) object);

            return result;
        }

        /**
         * Получить инвентарь по игроку
         * @param p Игрок
         * @return Ориентированный инвентарь
         */
        public Inventory get(Player p){
            Inventory result;

            if (playerOriented){
                var map = (Map<String, Inventory>) object;
                result = map.getOrDefault(p.getName().toLowerCase(), null);

                if (result == null){
                    result = create(p);

                    map.put(p.getName().toLowerCase(), result);
                }
            }else{
                if (object == null)
                    object = create(p);

                result = (Inventory) object;
            }

            return result;
        }

        /**
         * Создать инвентарь по конфигурации по игроку и кастомному заголовку
         * @param p Игрок
         * @param title Кастомный заголовок инвентаря
         * @return Инвентарь
         */
        private Inventory create(Player p, String title){

            return Bukkit.createInventory(
                    gui,
                    gui.getConfiguration().getSize(),
                    Utils.replace(
                            title,
                            getInventoryTitleVariables(p)
                    )
            );

        }

        /**
         * Get inventory title variables
         * @param p Player
         * @return List of variables
         */
        private List<Utils.P> getInventoryTitleVariables(Player p){
            var ps = new ArrayList<Utils.P>();

            var local = gui.inventoryTitleVariables(p);

            if (local != null && local.size() > 0)
                ps.addAll(local);

            for (var interceptor : gui.getInterceptors()) {
                local = interceptor.inventoryTitleVariables(p);

                if (local != null && local.size() > 0)
                    ps.addAll(local);
            }

            return ps;
        }

        /**
         * Создать инвентарь по конфигурации по игроку
         * @param p Игрок
         * @return Инвентарь
         */
        private Inventory create(Player p){

            return create(
                    p,
                    gui.getConfiguration().getTitle()
            );

        }

        /**
         * Переименовать инвентарь
         * @param p Игрок
         * @param title Заголовок
         * @return Новый инвентарь (уже занесён в процесс)
         */
        private Inventory rename(Player p, String title){
            Inventory was;

            // Получаем прежний инвентарь для последующего переноса содержимого
            if (playerOriented)
                was = ((Map<String, Inventory>) object).getOrDefault(p.getName().toLowerCase(), null);
            else
                was = (Inventory) object;

            if (was != null && p.getOpenInventory().getTopInventory().equals(was)){
                var view = p.getOpenInventory();

                if (view.getTitle().equals(
                        Utils.replace(
                            title,
                            getInventoryTitleVariables(p)
                        )
                    ))
                    return was;
            }

            // Делаем инвентарь с новым заголовком
            var inv = create(p, title);

            // Устанавливаем новый инвентарь в обиход
            if (playerOriented)
                ((Map<String, Inventory>) object).put(p.getName().toLowerCase(), inv);
            else
                object = inv;

            // Если нечего переносить - уходим
            if (was == null)
                return inv;

            // Переносим вещи
            for(int slot = 0; slot < inv.getSize(); slot++){
                if (slot >= was.getSize() - 1)
                    break;

                var item = was.getItem(slot);

                if (item == null || item.getType() == Material.AIR)
                    continue;

                inv.setItem(slot, item);
            }

            return inv;
        }

    }

    /**
     * Отдача при проверке
     */
    @RequiredArgsConstructor
    @AllArgsConstructor
    public static class Response {

        /**
         * Найден ли в ивенте наш инвентарь
         */
        @Getter
        private final boolean alive;

        /**
         * Наш ли инвентарь был задействован
         */
        @Getter
        @Setter
        private boolean our = true;

    }

    /**
     * Элемент инвентаря
     */
    @ToString
    public static class Item extends ConfigItemStack {

        /**
         * Слот предмета
         */
        @Getter
        @Setter
        private int slot = 0;

        @Override
        public List<String> extendedInitialization(Map<?, ?> map) {
            // Слот
            if (map.containsKey("slot"))
                setSlot(((Integer) map.get("slot")) - 1);
            // Альтернативное написание через x и y
            else if (map.containsKey("x") && map.containsKey("y"))
                setSlot(
                        ((((Integer) map.get("y")) - 1) * 9)
                                +
                        (((Integer) map.get("x")) - 1)
                );

            return new ArrayList<String>() {{

                add("slot");
                add("x");
                add("y");

            }};
        }

    }

    /**
     * Конфигурация GUI
     */
    @ToString
    public static class Configuration {

        /**
         * Used to initialize configuration section
         */
        @Getter
        @Setter
        private ConfigurationSection section = null;

        /**
         * Название инвентаря
         */
        @Getter
        @Setter
        private String title = null;

        /**
         * Количество слотов
         */
        @Getter
        @Setter
        private int size = 9;

        /**
         * Предметы
         */
        @Getter
        @Setter
        private Set<Item> items = new HashSet<>();

        /**
         * Получить конфигурацию GUI по конфигу и пути до неё
         * @param config Конфигурация
         * @param path Путь до конфигурации
         * @return Конфигурация GUI
         */
        public static Configuration fromConfig(FileConfiguration config, String path){
            var c = new Configuration();
            var section = config.getConfigurationSection(path);

            c.setSection(section);

            if (section == null)
                return c;

            // Имя
            if (section.isSet("name"))
                c.setTitle(Utils.toColor(section.getString("name")));

            // Размер инвентаря
            if (section.isSet("size"))
                c.setSize(section.getInt("size"));

            // Строки
            if (section.isSet("rows"))
                c.setSize(section.getInt("rows") * 9);

            // Предметы
            if(section.isSet("items")){

                for(var map : section.getMapList("items")){
                    var i = (Item) new Item().fromConfig(map);

                    if (i == null)
                        continue;

                    c.getItems().add(i);
                }

            }

            return c;
        }

        /**
         * Получить конфигурацию GUI по конфигурации в корне
         * @param config Конфигурация
         * @return Конфигурация GUI
         */
        public static Configuration fromConfig(FileConfiguration config){
            return fromConfig(config, "");
        }

    }

}
