package me.wattguy.delivery.guis;

import me.wattguy.delivery.Utils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Inventory modifier
 * Creation date: 03:19 06.02.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public interface InventoryModifier extends ClickModifier {

    /**
     * Is satisfied by gui or not
     * @param gui GUI
     * @return true - satisfied, false - not satisfied
     */
    default boolean isSatisfied(PluginGUI gui){
        return false;
    }

    /**
     * Parameters for all items by player
     * @param p Player
     * @return List of parameters (can be null)
     */
    default List<Utils.P> itemVariables(Player p) {
        return null;
    }

    /**
     * Parameters for item by player
     * @param p Player
     * @param item Item
     * @return List of parameters (can be null)
     */
    default List<Utils.P> itemVariables(Player p, PluginGUI.Item item) {
        return null;
    }

    /**
     * Item list
     * @param gui GUI
     * @param itemList Item list
     */
    default void itemList(PluginGUI gui, Collection<PluginGUI.Item> itemList) { }

    /**
     * Uploading to inventory
     * @param gui GUI
     * @param uploading Uploading info
     * @param affected Affected slots
     * @return Cancel item or not
     */
    default boolean uploading(PluginGUI gui, PluginGUI.UploadingInfo uploading, Set<Integer> affected) {
        return false;
    }

    /**
     * Post update method
     * @param gui GUI
     * @param inv Inventory
     * @param p Player
     */
    default List<Integer> postUpdate(PluginGUI gui, Inventory inv, Player p) {
        return null;
    }

}
