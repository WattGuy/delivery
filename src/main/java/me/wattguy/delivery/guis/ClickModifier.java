package me.wattguy.delivery.guis;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * All GUI's click listener
 * Creation date: 14:03 18.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public interface ClickModifier {

    /**
     * Click
     * @param gui GUI
     * @param e Event
     * @param p Player
     * @param itemStack Bukkit item
     * @param item GUI item
     * @param slot Slot
     * @return Cancel next listeners
     */
    default boolean click(PluginGUI gui, InventoryInteractEvent e, Player p, ItemStack itemStack, PluginGUI.Item item, int slot){
        return false;
    }

    /**
     * Not GUI click
     * @param gui GUI
     * @param e Event
     * @param p Player
     * @param itemStack Bukkit item
     * @param slot Slot
     * @return Cancel next listeners
     */
    default boolean notOurClick(PluginGUI gui, InventoryInteractEvent e, Player p, ItemStack itemStack, int slot){
        return false;
    }

}
