package me.wattguy.delivery.guis.interceptors;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.ConfigItemStack;
import me.wattguy.delivery.guis.InventoryInterceptor;
import me.wattguy.delivery.guis.PluginGUI;
import me.wattguy.delivery.guis.list.GUI;
import me.wattguy.delivery.managers.CustomersManager;
import me.wattguy.delivery.managers.OrdersManager;
import me.wattguy.delivery.managers.SearchManager;
import me.wattguy.delivery.utils.Order;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.lemar98.regionshops.model.ShopItem;

import java.util.*;

public class Shop extends InventoryInterceptor {

    @Getter
    private static final Map<String, ru.lemar98.regionshops.model.Shop> shops = new HashMap<>();

    private final List<Integer> slots = new ArrayList<>();

    private final Map<String, List<PluginGUI.Item>> items = new HashMap<>();

    private List<PluginGUI.Item> getItems(String s){
        var list = items.getOrDefault(s.toLowerCase(), null);

        if (list == null){
            list = new ArrayList<>();
            items.put(s.toLowerCase(), list);
        }

        return list;
    }

    private ConfigItemStack template = null;

    // INVENTORY HANDLER

    @Override
    public boolean click(InventoryInteractEvent e, Player p, ItemStack itemStack, PluginGUI.Item item, int slot) {
        if (generatedHandler(e, p, slot) || item == null || itemStack == null || itemStack.getType() == Material.AIR)
            return false;

        if (getGui().isPropertyEnabled(item, "next_page")){

            setPage(p, getPage(p) + 1);
            getGui().updateAsync(p);

        }else if (getGui().isPropertyEnabled(item, "previous_page")){

            setPage(p, Math.max(1, getPage(p) - 1));
            getGui().updateAsync(p);

        }else if (getGui().isPropertyEnabled(item, "confirm_order") || getGui().isPropertyEnabled(item, "deny_order")) {
            var order = OrdersManager.get(p, true).orElse(null);

            if (order == null || order.getState() == Order.State.BASKET)
                OrdersManager.confirmOrder(p);
            else
                OrdersManager.denyOrder(p);
        }else if (getGui().isPropertyEnabled(item, "search"))
            SearchManager.input(p);
        else if (getGui().isPropertyEnabled(item, "order_info"))
            GUI.open(p, "basket");

        return false;
    }

    private boolean generatedHandler(InventoryInteractEvent e, Player p, int slot){
        if (!getGui().hasFeature(p, "items"))
            return false;

        var items = (Map<Integer, ShopItem>) getGui().getFeature(p, "items");
        var item = items.getOrDefault(slot, null);
        var shop = getShop(p).orElse(null);

        if (item == null || shop == null)
            return false;

        var customer = CustomersManager.get(p, true).orElse(null);

        if (customer == null)
            return true;

        var order = OrdersManager.get(p, true).orElse(null);

        if (order.getState() != Order.State.BASKET){
            Utils.send(p, "order.already_confirmed");
            return true;
        }

        if (order.getShopName() != null && !order.getShopName().equalsIgnoreCase(shop.getName())){
            Utils.send(p, "order.another_shop");
            return true;
        }

        var orderItem = order.getItem(item, true).orElse(null);

        int amount = 1;

        if (e instanceof InventoryClickEvent && ((InventoryClickEvent) e).isRightClick())
            amount = item.getItemStack().getType().getMaxStackSize();

        orderItem.setAmount(orderItem.getAmount() + amount);

        if (order.getShopName() == null)
            order.setShopName(shop.getName());

        getGui().updateAsync(p);
        Utils.send(p, "order.added", new Utils.P("{name}", orderItem.getName(Utils.getTranslateVariable(orderItem.getItem().getItemStack()), true)), new Utils.P("{amount}", orderItem.getAmount() + ""));

        return true;
    }

    @Override
    public void itemList(Collection<PluginGUI.Item> itemList){

        itemList.removeIf(i ->
                        getGui().isPropertyEnabled(i, "empty")
                        ||
                        getGui().isPropertyEnabled(i, "previous_page")
                        ||
                        getGui().isPropertyEnabled(i, "next_page")
                        ||
                        getGui().isPropertyEnabled(i, "confirm_order")
                        ||
                        getGui().isPropertyEnabled(i, "deny_order")
        );

    }

    @Override
    public List<Integer> postUpdate(Inventory inv, Player p) {
        var affected = new ArrayList<Integer>();

        var has = getGui().hasFeature(p, "items");
        var empty = false;

        Map<Integer, ShopItem> storage = has ? getGui().getFeature(p, "items") : new HashMap<>();
        var page = getPage(p);
        List<ShopItem> all = new ArrayList<>();

        var shop = getShop(p).orElse(null);

        if (shop != null)
            all.addAll(shop.getSellItems());

        var items = getItemsByPage(all, page);

        if (items.size() <= 0){
            if (has)
                getGui().removeFeature(p, "items");
            else
                has = true;

            empty = true;
        }

        storage.clear();

        if (!empty) {
            var cachedSlotCursor = 0;

            for (var item : items) {
                if (template == null)
                    continue;

                Integer slot = null;

                for (var slotCursor = cachedSlotCursor; slotCursor < slots.size(); slotCursor++) {
                    cachedSlotCursor = slotCursor + 1;
                    var cachedSlot = slots.get(slotCursor);

                    if (cachedSlot < 0 || cachedSlot >= inv.getSize())
                        continue;

                    slot = cachedSlot;
                    break;
                }

                if (slot == null)
                    break;

                var ps = new Utils.P[] {
                        new Utils.P("{name}", Utils.getName(item.getItemStack(), "", false)),
                        new Utils.P("{lore}", new ArrayList<>(item.getLore()), true),
                        new Utils.P("{price}", item.getPrice()),
                        new Utils.P("{replace_material}", item.getItemStack().getType()),
                        new Utils.P("{replace_cmd}", Utils.getCustomModelData(item.getItemStack())),
                        new Utils.P("{stack_size}", item.getItemStack().getType().getMaxStackSize() + ""),
                        new Utils.P("{stored_amount}", Utils.getStoredAmount(shop, item) + "")
                };

                var is = inv.getItem(slot);

                if (is != null && is.getType() != Material.AIR) {
                    template.modify(is, 1, p, inv, slot, ps);
                } else {
                    is = template.itemStack(1, p, inv, slot, ps);

                    inv.setItem(slot, is);
                }

                storage.put(slot, item);
                affected.add(slot);
            }

        }

        if (page > 1)
            getGui().set(
                    p,
                    inv,
                    getItems("previous_page"),
                    affected
            );

        if (all.size() > page * slots.size())
            getGui().set(
                    p,
                    inv,
                    getItems("next_page"),
                    affected
            );

        if (storage.size() == 0)
            getGui().set(
                    p,
                    inv,
                    getItems("empty"),
                    affected
            );

        var order = OrdersManager.get(p, true).orElse(null);

        if (order == null || order.getState() == Order.State.BASKET){
            getGui().set(
                    p,
                    inv,
                    getItems("confirm_order"),
                    affected
            );
        }else{
            getGui().set(
                    p,
                    inv,
                    getItems("deny_order"),
                    affected
            );
        }

        if (!has)
            getGui().setFeature(p, "items", storage);

        return affected;
    }

    // / INVENTORY HANDLER

    /**
     * Constructor
     *
     * @param gui GUI
     */
    public Shop(PluginGUI gui) {
        super(gui);
    }

    @Override
    public boolean isSatisfied() {
        if (getGui() == null || getGui().getConfiguration() == null || getGui().getConfiguration().getSection() == null)
            return false;

        return byConfigName("shop");
    }

    /**
     * Initializing
     */
    public void initialize(){
        getGui().getSuperInventory().setPlayerOriented(true);

        if (getGui().getConfiguration() == null || getGui().getConfiguration().getSection() == null)
            return;

        for (var item : getGui().getConfiguration().getItems()) {

            for (var s : new String[]{ "empty", "next_page", "previous_page", "confirm_order", "deny_order" }) {
                if (!getGui().isPropertyEnabled(item, s))
                    continue;

                getItems(s).add(item);
            }

        }

        var section = getGui().getConfiguration().getSection();

        // INITIALIZE

        slots.clear();

        if (section.isSet("slots")) {

            if (section.isList("slots"))
                for (var i : section.getIntegerList("slots"))
                    slots.add(i - 1);
            else if (section.isString("slots")){
                var splitted = section.getString("slots").trim().split("-");

                if (splitted.length >= 2) {
                    var from = Utils.getInteger(splitted[0], null);
                    var to = Utils.getInteger(splitted[1], null);

                    if (from != null && to != null)
                        for (var i = (int) from; i <= to; i++)
                            slots.add(i - 1);
                }
            }

        }

        if (section.isSet("template"))
            template = (ConfigItemStack) new ConfigItemStack().fromConfig(section.getConfigurationSection("template"));
    }

    public List<ShopItem> getItemsByPage(List<ShopItem> sorted, int page){
        var start = (page - 1) * slots.size();
        var end = start + slots.size();

        var result = new ArrayList<ShopItem>();

        for(var i = start; i < end; i++) {
            if (sorted.size() <= i)
                break;

            result.add(sorted.get(i));
        }

        return result;
    }

    private int getPage(Player player){
        return !getGui().hasFeature(player, "page") ? 1 : getGui().getFeature(player, "page");
    }

    private void setPage(Player player, int page){
        getGui().setFeature(player, "page", page);
    }

    @Override
    public List<Utils.P> inventoryTitleVariables(Player p) {
        var shop = getShop(p).orElse(null);

        return Arrays.asList(
                new Utils.P("{page}", getPage(p)),
                new Utils.P("{shop}", shop != null ? shop.getName() : "")
        );
    }

    @Override
    public List<Utils.P> itemVariables(Player p) {
        var shop = getShop(p).orElse(null);

        return Arrays.asList(
                new Utils.P("{page}", getPage(p)),
                new Utils.P("{shop}", shop != null ? shop.getName() : "")
        );
    }

    @Override
    public List<Utils.P> itemVariables(Player p, PluginGUI.Item item) {
        if (item != null && getGui().isPropertyEnabled(item, "order_info")){
            return Arrays.asList(OrdersManager.get(p, true).orElse(null).getVariables(p.getLocation()));
        }

        return null;
    }

    private static Optional<ru.lemar98.regionshops.model.Shop> getShop(String playerName){
        return Optional.ofNullable(shops.getOrDefault(playerName.toLowerCase(), null));
    }

    private static Optional<ru.lemar98.regionshops.model.Shop> getShop(Player player){
        return getShop(player.getName().toLowerCase());
    }

    public static void open(Player player, ru.lemar98.regionshops.model.Shop shop){
        shops.put(player.getName().toLowerCase(), shop);

        GUI.get("shop").refreshNameSync(player);
        GUI.open(player, "shop");
    }

}
