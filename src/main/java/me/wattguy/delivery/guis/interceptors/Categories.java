package me.wattguy.delivery.guis.interceptors;

import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.extensions.LevelSystemExtension;
import me.wattguy.delivery.guis.InventoryInterceptor;
import me.wattguy.delivery.guis.PluginGUI;
import me.wattguy.delivery.guis.list.GUI;
import me.wattguy.delivery.managers.CouriersManager;
import me.wattguy.delivery.managers.ItemsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Categories extends InventoryInterceptor {

    // INVENTORY HANDLER

    @Override
    public boolean click(InventoryInteractEvent e, Player p, ItemStack itemStack, PluginGUI.Item item, int slot) {
        if (item == null)
            return false;

        if (getGui().isPropertyEnabled(item, "order")){
            GUI.open(p, "shops");
        }else if (getGui().isPropertyEnabled(item, "deliver")){
            var courier = CouriersManager.get(p, false).orElse(null);

            if (courier != null){
                Utils.send(p, "deliver.already");
                return true;
            }

            if (Config.get().isSet("delivery.level") && LevelSystemExtension.getLevel(p, 0) < Config.get().getInt("delivery.level", 1)){
                Utils.send(p, "deliver.no_level", new Utils.P("{level}", Config.get().getInt("delivery.level", 1) + ""));
                return true;
            }

            if (!ItemsManager.can(p)){
                Utils.send(p, "deliver.has_items");
                return true;
            }

            CouriersManager.add(p);
        }

        return true;
    }

    // / INVENTORY HANDLER

    /**
     * Constructor
     *
     * @param gui GUI
     */
    public Categories(PluginGUI gui) {
        super(gui);
    }

    @Override
    public boolean isSatisfied() {
        if (getGui() == null || getGui().getConfiguration() == null || getGui().getConfiguration().getSection() == null)
            return false;

        return byConfigName("categories");
    }

}
