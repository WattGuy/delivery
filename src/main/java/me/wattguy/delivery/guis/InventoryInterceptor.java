package me.wattguy.delivery.guis;

import lombok.Getter;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.guis.list.GUI;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Inventory interceptor
 * Creation date: 22:56 19.03.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public abstract class InventoryInterceptor {

    /**
     * GUI
     */
    @Getter
    private PluginGUI gui = null;

    /**
     * Constructor
     * @param gui GUI
     */
    public InventoryInterceptor(PluginGUI gui){
        this.gui = gui;
    }

    /**
     * Is satisfied or not
     * @return Satisfied or not
     */
    public abstract boolean isSatisfied();

    /**
     * Initialize values
     */
    public void initialize(){}

    /**
     * Parameters for all items by player
     * @param p Player
     * @return List of parameters (can be null)
     */
    public List<Utils.P> itemVariables(Player p) {
        return null;
    }

    /**
     * Parameters for item by player
     * @param p Player
     * @param item Item
     * @return List of parameters (can be null)
     */
    public List<Utils.P> itemVariables(Player p, PluginGUI.Item item) {
        return null;
    }

    /**
     * Item list
     * @param itemList Item list
     */
    public void itemList(Collection<PluginGUI.Item> itemList) { }

    /**
     * Uploading to inventory
     * @param uploading Uploading info
     * @param affected Affected slots
     * @return Cancel item or not
     */
    public boolean uploading(PluginGUI.UploadingInfo uploading, Set<Integer> affected) {
        return false;
    }

    /**
     * Post update method
     * @param inv Inventory
     * @param p Player
     */
    public List<Integer> postUpdate(Inventory inv, Player p) {
        return null;
    }

    /**
     * Parameters for title by player
     * @param p Player
     * @return List of parameters (can be null)
     */
    public List<Utils.P> inventoryTitleVariables(Player p) {
        return null;
    }

    /**
     * Click
     * @param e Event
     * @param p Player
     * @param itemStack Bukkit item
     * @param item GUI item
     * @param slot Slot
     * @return Cancel next listeners
     */
    public boolean click(InventoryInteractEvent e, Player p, ItemStack itemStack, PluginGUI.Item item, int slot){
        return false;
    }

    /**
     * Not GUI click
     * @param e Event
     * @param p Player
     * @param itemStack Bukkit item
     * @param slot Slot
     * @return Cancel next listeners
     */
    public boolean notOurClick(InventoryInteractEvent e, Player p, ItemStack itemStack, int slot){
        return false;
    }

    public boolean byConfigName(String s){
        return getGui() != null && getGui().getConfiguration() != null && getGui().getConfiguration().getSection() != null && getGui() instanceof GUI && s.equalsIgnoreCase(((GUI) getGui()).getConfigName());
    }

}
