package me.wattguy.delivery.guis.list;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.configs.ConfigList;
import me.wattguy.delivery.guis.ClickModifier;
import me.wattguy.delivery.guis.PluginGUI;
import me.wattguy.delivery.listeners.PluginListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * GUIs
 *
 * @author WattGuy
 * @version 1.0
 */
public class GUI extends PluginGUI {

    /**
     * Last closed GUI
     */
    public static final Map<Player, GUI> closed = new HashMap<>();

    /**
     * Configuration blacklist
     */
    public static final Set<String> BLACKLIST = new HashSet<String>() {{



    }};

    /**
     * Back to last closed
     * @param p Player
     */
    public static void back(Player p){
        if (!closed.containsKey(p))
            return;

        closed.get(p).open(p);
    }

    /**
     * Enable all allowed guis from "/menus"
     * @return Set of file names
     */
    public static Set<String> multiple(){
        var set = new HashSet<String>();

        for (var entry : ConfigList.getMenus().list()) {
            var name = entry.getKey();

            if (BLACKLIST.contains(name))
                continue;

            set.add(name);
        }

        return set;
    }

    /**
     * Config name
     */
    @Getter
    private final String configName;

    /**
     * Constructor
     * @param configName File name from "/menus" path
     */
    public GUI(String configName) {
        super(ConfigList.getMenus().config(configName), ConfigList.getMenus().config(configName).getBoolean("playerOriented", false));

        this.configName = configName;
    }

    // <!-- РЕАЛИЗАЦИЯ РЕФЛЕКСИИ --!>

    public static GUI get(String name){
        return (GUI) getByClass(MethodHandles.lookup().lookupClass(), name);
    }

    // <!-- РЕАЛИЗАЦИЯ РЕФЛЕКСИИ --!>

    /**
     * Is it config GUIs
     * @param inventory GUI
     * @param configNames Config names
     * @return Is it or not
     */
    public static boolean is(PluginGUI gui, String... configNames){
        if (!(gui instanceof GUI))
            return false;

        for (var configName : configNames) {
            if (!((GUI) gui).getConfigName().equalsIgnoreCase(configName))
                continue;

            return true;
        }

        return false;
    }

    /**
     * Is it config GUIs
     * @param inventory GUI
     * @param configNames Config names
     * @return Is it or not
     */
    public static boolean is(Inventory inventory, String... configNames){
        if (inventory == null || !(inventory.getHolder() instanceof GUI))
            return false;

        return is((GUI) inventory.getHolder(), configNames);
    }

    /**
     * Open player GUI
     * @param player Player
     * @param gui GUI name
     * @return Opened or not
     */
    public static boolean open(Player player, String gui){
        if (player == null || gui == null || gui.trim().isEmpty())
            return false;

        var instance = get(gui);

        if (instance == null)
            return false;

        instance.open(player);

        return true;
    }

    /**
     * Close player GUI
     * @param player Player
     * @param gui GUI name
     * @return Closed or not
     */
    public static boolean close(Player player, String gui){
        if (player == null || gui == null || gui.trim().isEmpty())
            return false;

        var instance = get(gui);

        if (instance == null)
            return false;

        instance.close(player);
        return true;
    }

    /**
     * Back click modifier
     */
    public static class BackModifier implements ClickModifier {

        @Override
        public boolean click(PluginGUI gui, InventoryInteractEvent e, Player p, ItemStack itemStack, Item item, int slot) {
            if (!(gui instanceof GUI) || p == null || item == null || !gui.isPropertyEnabled(item, "back"))
                return false;

            back(p);
            return false;
        }

    }

    /**
     * Listener
     */
    public static class Listener extends PluginListener {

        @EventHandler(priority = EventPriority.MONITOR)
        public void onClose(InventoryCloseEvent e){
            if (!(e.getPlayer() instanceof Player))
                return;

            var p = (Player) e.getPlayer();
            closed.remove(p);

            if (e.getInventory() == null || e.getReason() != InventoryCloseEvent.Reason.OPEN_NEW || !(e.getInventory().getHolder() instanceof GUI))
                return;

            closed.put(p, (GUI) e.getInventory().getHolder());
        }

    }

}
