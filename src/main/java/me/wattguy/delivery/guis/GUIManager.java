package me.wattguy.delivery.guis;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;

import java.util.*;

/**
 * Менеджер игровых интерфейсов
 * Дата создания: 14:49 17.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class GUIManager implements Initializable, Unregistrable {

    /**
     * Список всех инициализированных GUI
     */
    public static final Map<Class<?>, Map<String, PluginGUI>> guis = new HashMap<>();

    /**
     * List of all initialized click listeners
     */
    @Getter
    private static final Map<Settings.Priority, List<ClickModifier>> clickModifiers = new HashMap<>();

    /**
     * List of all initialized inventory modifiers
     */
    @Getter
    private static final List<InventoryModifier> inventoryModifiers = new ArrayList<>();

    /**
     * List of all classes inventory interceptors
     */
    @Getter
    private static final Set<Class<? extends InventoryInterceptor>> interceptors = new HashSet<>();

    /**
     * Инициализировать все интерфейсы в плагине
     */
    public void initialize(){
        // Дерегистрируем прошлые и очищаем
        unregisterAll();
        closeAll();

        inventoryModifiers.clear();

        for (var c : getAllInventoryModifiers()) {
            var i = Utils.getInstance(c);

            if (i == null)
                continue;

            inventoryModifiers.add(i);
        }

        interceptors.clear();
        interceptors.addAll(getAllInventoryInterceptors());

        var number = 0;
        var l = System.currentTimeMillis();

        Main.getInstance().getLogger().info("Beginning the GUI initialization...");

        guis.clear();

        // Получаем все слушатели
        for(var c : getAllGUIs()){
            Map<String, PluginGUI> map = new HashMap<>();

            // Получаем экземпляр класса по пустому конструктору
            try {
                var multiple = Utils.getMethod(c, "multiple");

                if (multiple != null){
                    var set = (Set<String>) multiple.invoke(null);

                    for (var s : set) {
                        var instance = c.getConstructor(String.class).newInstance(s);
                        addInventoryModifiers(instance);
                        addInventoryInterceptors(instance);

                        map.put(s.toLowerCase(), instance);
                        Main.getInstance().getLogger().info("  [X] " + Utils.getClassName(c) + "." + s);

                        number++;
                    }
                }else {
                    var instance = c.getConstructor().newInstance();
                    addInventoryModifiers(instance);
                    addInventoryInterceptors(instance);

                    map.put("standard", instance);
                    Main.getInstance().getLogger().info("  [X] " + Utils.getClassName(c));

                    number++;
                }

            }catch(Exception ignored) { }

            // Если не получилось - уходим
            if (map.size() <= 0)
                continue;

            // Регистрируем
            for (var gui : map.values()) {
                gui.register();
            }

            // Кешируем
            guis.put(c, map);
        }

        Main.getInstance().getLogger().info("GUI initialization has been completed! (#" + number + ") (" + (System.currentTimeMillis() - l) + "ms)");

        clickModifiers.clear();

        for (var c : getAllClickModifiers()) {
            var i = Utils.getInstance(c);

            if (i == null)
                continue;

            var priority = Utils.getPriority(c, "click");

            if (clickModifiers.containsKey(priority)){
                var list = clickModifiers.get(priority);

                if (!list.contains(i))
                    list.add(i);
            }else{
                var list = new ArrayList<ClickModifier>();

                list.add(i);

                clickModifiers.put(priority, list);
            }
        }
    }

    /**
     * Add inventory modifiers
     * @param gui GUI instance
     */
    public static void addInventoryModifiers(PluginGUI gui){
        if (gui == null)
            return;

        gui.getInventoryModifiers().clear();

        for (var modifier : inventoryModifiers) {
            if (!modifier.isSatisfied(gui))
                continue;

            gui.getInventoryModifiers().add(modifier);
        }
    }

    /**
     * Add inventory interceptors
     * @param gui GUI instance
     */
    public static void addInventoryInterceptors(PluginGUI gui){
        if (gui == null)
            return;

        gui.getInterceptors().clear();

        for (var interceptor : interceptors) {
            InventoryInterceptor instance = null;

            try {
                instance = interceptor.getConstructor(PluginGUI.class).newInstance(gui);
            } catch (Exception ignored) { }

            if (instance == null || !instance.isSatisfied())
                continue;

            instance.initialize();
            gui.getInterceptors().add(instance);
        }
    }

    /**
     * Автоматическая дерегистрация
     */
    @Override
    public void unregister() {
        // Дерегистрируем все GUI
        unregisterAll();

        // Закрываем все GUI
        closeAll();
    }

    /**
     * Дерегистрировать все слушатели
     */
    public static void unregisterAll(){
        // Дерегистрируем GUI
        for (var guis : guis.values()) {

            for (var gui : guis.values()) {
                gui.unregister();
            }

        }
    }

    public static void closeAll(){
        // Закрываем все GUI
        for(var guis : guis.values()){

            for (var gui : guis.values()) {
                gui.closeAll();
            }

        }
    }

    /**
     * Получить все классы-GUI с помощью рефлексии
     * @return Список всех GUI-классов
     */
    public static Set<Class<? extends PluginGUI>> getAllGUIs(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(GUIManager.class.getPackage().getName(), 1), PluginGUI.class);

    }

    /**
     * Get all click listeners classes
     * @return List of all click listeners classes
     */
    public static Set<Class<? extends ClickModifier>> getAllClickModifiers(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(ClickModifier.class.getPackage().getName(), 1), ClickModifier.class);

    }

    /**
     * Get all inventory modifiers classes
     * @return List of all inventory modifiers classes
     */
    public static Set<Class<? extends InventoryModifier>> getAllInventoryModifiers(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(InventoryModifier.class.getPackage().getName(), 1), InventoryModifier.class);

    }

    /**
     * Get all inventory interceptors classes
     * @return List of all inventory interceptors classes
     */
    public static Set<Class<? extends InventoryInterceptor>> getAllInventoryInterceptors(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(InventoryInterceptor.class.getPackage().getName(), 1), InventoryInterceptor.class);

    }

}
