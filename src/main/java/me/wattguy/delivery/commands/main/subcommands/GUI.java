package me.wattguy.delivery.commands.main.subcommands;

import me.wattguy.delivery.commands.PluginSubCommand;
import me.wattguy.delivery.initialize.Settings;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@Settings(priority = Settings.Priority.HIGHEST)
public class GUI extends PluginSubCommand {

    /**
     * /rpmdelivery|delivery - rpmdelivery.usage - Открыть GUI
     */
    public GUI() {
        super(
                (String) null,
                0,
                SenderType.PLAYER,
                "rpmdelivery.usage"
        );
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        me.wattguy.delivery.guis.list.GUI.get("categories").open((Player) sender);
        return true;
    }

    @Override
    public void tab(CommandSender sender, String alias, String[] args, List<String> list) { }

}
