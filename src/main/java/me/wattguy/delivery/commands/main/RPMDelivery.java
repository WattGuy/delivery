package me.wattguy.delivery.commands.main;

import me.wattguy.delivery.commands.PluginCommand;

/**
 * Реализация команды
 *
 * @author WattGuy
 * @version 1.0
 */
public class RPMDelivery extends PluginCommand {

    /**
     * Конструктор-инициализатор команды
     */
    public RPMDelivery() {
        super("rpmdelivery", "RPMDelivery main command", new String[] { "delivery" });
    }

}
