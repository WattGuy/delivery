package me.wattguy.delivery.commands.main.subcommands;

import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.commands.PluginSubCommand;
import me.wattguy.delivery.commands.Trigger;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.managers.CouriersManager;
import me.wattguy.delivery.managers.ItemsManager;
import me.wattguy.delivery.managers.OrdersManager;
import me.wattguy.delivery.utils.Order;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@Settings(priority = Settings.Priority.HIGHEST)
public class Cancel extends PluginSubCommand {

    /**
     * /rpmdelivery|delivery cancel (номер заказа) - Отменить заказ
     * /rpmdelivery|delivery cancel - Уволиться с работы курьером
     */
    public Cancel() {
        super(
                new Trigger(0, "cancel"),
                new int[] { 1, 2 },
                SenderType.PLAYER,
                null
        );
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        var player = (Player) sender;

        if (args.length == 2){
            var orderId = args[1];
            var order = OrdersManager.get(orderId).orElse(null);

            if (order == null || !order.getCustomer().getPlayer().getName().equalsIgnoreCase(player.getName())){
                Utils.send(player, "cancel.customer.not_found", new Utils.P("{order}", orderId));
                return true;
            }

            OrdersManager.denyOrder(player);
        }else{
            var courier = CouriersManager.get(player, false).orElse(null);

            if (courier == null){
                Utils.send(player, "cancel.courier.already");
                return true;
            }

            if (courier.getOrder() != null){
                courier.getOrder().sendCourier("cancel.courier.cant", true, true);
                return true;
            }

            CouriersManager.remove(player.getName());
            ItemsManager.take(player);

            Utils.send(player, "cancel.courier.successfully");
        }

        return true;
    }

    @Override
    public void tab(CommandSender sender, String alias, String[] args, List<String> list) {
        var trigger = getTriggers().get(0)[0];

        trigger.addIfStartsWith(list, args);

        if (!trigger.isSatisfies(args) || !(sender instanceof Player))
            return;

        var player = (Player) sender;
        var order = OrdersManager.get(player, false).orElse(null);

        if (order == null || order.getState() == Order.State.BASKET)
            return;

        addIfStartsWith(list, args, 1, order.getId());
    }

}
