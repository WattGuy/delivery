package me.wattguy.delivery.commands.main.subcommands;

import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.commands.PluginSubCommand;
import me.wattguy.delivery.commands.Trigger;
import me.wattguy.delivery.extensions.BankExtension;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.managers.CouriersManager;
import me.wattguy.delivery.managers.OrdersManager;
import me.wattguy.delivery.utils.Order;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

@Settings(priority = Settings.Priority.HIGHEST)
public class Accept extends PluginSubCommand {

    /**
     * /rpmdelivery|delivery accept [номер заказа] - Принять заказ
     */
    public Accept() {
        super(
                new Trigger(0, "accept"),
                2,
                SenderType.PLAYER,
                null
        );
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        var player = (Player) sender;

        if (CouriersManager.isBlocked(player.getName())){
            Utils.send(sender, "blocked");
            return true;
        }

        var courier = CouriersManager.get(player, false).orElse(null);

        if (courier == null){
            Utils.send(sender, "accept.no_courier");
            return true;
        }

        if (courier.getOrder() != null){
            courier.getOrder().sendCourier("accept.cant", true, true);
            return true;
        }

        var orderId = args[1];
        var order = OrdersManager.get(orderId).orElse(null);

        if (order == null || !order.getCustomer().getPlayer().isOnline()){
            Utils.send(sender, "accept.not_found", new Utils.P("{order}", orderId));
            return true;
        }

        if (order.getCustomer().getPlayer().getName().equalsIgnoreCase(player.getName())){
            return true;
        }

        if (order.getState() != Order.State.SEARCH_COURIER || order.getCourier() != null) {
            Utils.send(sender, "accept.already", new Utils.P("{order}", orderId));
            return true;
        }

        var price = order.getPrice();

        if (!Main.getEconomy().has(player, price) && !BankExtension.has(player, price)){
            Utils.send(sender, "deliver.not_enough_money", new Utils.P("{price}", price + ""));
            return true;
        }

        OrdersManager.addCourierToOrder(order, courier);
        order.setState(Order.State.COURIER_GOES_TO_STORE);

        courier.gpsToStore();

        order.sendCustomer("order.found", true, true);
        order.sendCourier("deliver.accepted", true, true);
        return true;
    }

    @Override
    public void tab(CommandSender sender, String alias, String[] args, List<String> list) {
        var trigger = getTriggers().get(0)[0];

        trigger.addIfStartsWith(list, args);

        if (!trigger.isSatisfies(args))
            return;

        addIfStartsWith(list, args, 1, OrdersManager.getSearchOrders().stream().map(Order::getId).collect(Collectors.toList()));
    }

}
