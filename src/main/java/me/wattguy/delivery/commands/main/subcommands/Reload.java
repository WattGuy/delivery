package me.wattguy.delivery.commands.main.subcommands;

import me.wattguy.delivery.Utils;
import me.wattguy.delivery.commands.PluginSubCommand;
import me.wattguy.delivery.initialize.InitializeManager;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * @author WattGuy
 * @version 1.0
 */
public class Reload extends PluginSubCommand {

    /**
     * /rpmdelivery|delivery reload - rpmdelivery.reload - Перезагрузка плагина
     */
    public Reload() {
        super(
                "reload",
                1,
                SenderType.ALL,
                "rpmdelivery.reload"
        );
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        InitializeManager.initialize();

        Utils.send(sender, "reloaded");
        return true;
    }

    @Override
    public void tab(CommandSender sender, String alias, String[] args, List<String> list) {

        addIfStartsWith(list, args, 0, "reload");

    }

}
