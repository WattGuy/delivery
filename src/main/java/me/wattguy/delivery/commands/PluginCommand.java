package me.wattguy.delivery.commands;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Settings;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.*;

/**
 * Класс-абстракция для команд-экземпляров
 * Дата создания: 15:00 25.08.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public abstract class PluginCommand extends Command {

    /**
     * Субкоманды
     */
    @Getter
    private final Map<Settings.Priority, List<PluginSubCommand>> subcommands = new HashMap<>();

    /**
     * Конструктор команды
     * @param name Главное имя команды
     */
    public PluginCommand(String name){
        super(name);

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param description Описание команды
     */
    public PluginCommand(String name, String description){
        super(name, description, "/" + name, new ArrayList<>());

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param aliases Алиасы команды
     */
    public PluginCommand(String name, String[] aliases){
        super(name, "", "", Arrays.asList(aliases));

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param aliases Алиасы команды
     */
    public PluginCommand(String name, List<String> aliases){
        super(name, "", "", aliases);

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param description Описание команды
     * @param usage Использование команды
     */
    public PluginCommand(String name, String description, String usage){
        super(name, description, usage, new ArrayList<>());

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param description Описание команды
     * @param aliases Алиасы команды
     */
    public PluginCommand(String name, String description, List<String> aliases){
        super(name, description, "", aliases);

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param description Описание команды
     * @param aliases Алиасы команды
     */
    public PluginCommand(String name, String description, String[] aliases){
        super(name, description, "", Arrays.asList(aliases));

        initialize();
    }

    /**
     * Конструктор команды
     * @param name Главное имя команды
     * @param description Описание команды
     * @param usage Использование команды
     * @param aliases Алиасы команды
     */
    public PluginCommand(String name, String description, String usage, List<String> aliases){
        super(name, description, usage, aliases);

        initialize();
    }

    /**
     * Инициализация полей
     */
    private void initialize(){
        subcommands.clear();

        for (var instance : getAllSubCommandsInstances()) {
            var priority = getPriority(instance.getClass());

            if (subcommands.containsKey(priority)){
                var list = subcommands.get(priority);

                if (!list.contains(instance))
                    list.add(instance);
            }else{
                var list = new ArrayList<PluginSubCommand>();

                list.add(instance);

                subcommands.put(priority, list);
            }
        }
    }

    /**
     * Get subcommand priority
     * @param c Subcommand class
     * @return Priority, if not found - NORMAL
     */
    private Settings.Priority getPriority(Class<? extends PluginSubCommand> c){
        try{
            var settings = c.getAnnotation(Settings.class);

            if (settings != null)
                return settings.priority();
        }catch(Exception ignored) {}

        return Settings.Priority.NORMAL;
    }

    /**
     * Реализация команды интерфейса
     * @param sender Отправитель
     * @param alias Использованный алиас
     * @param args Аргументы команды
     * @return Булева-успешность
     */
    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        // Проходим по субкомандам исходя из приоритета
        for(int i = 0; i < Settings.Priority.values().length; i++){
            var list = subcommands.getOrDefault(Settings.Priority.values()[i], null);

            if (list == null)
                continue;

            for (var sc : list) {
                if (!sc.isSatisfy(sender, args))
                    continue;

                if (sc.execute(sender, alias, args))
                    return true;
            }
        }

        return command(sender, alias, args);
    }

    /**
     * Абстрактная реализация команды
     * @param sender Отправитель
     * @param alias Использованный алиас
     * @param args Аргументы команды
     * @return Булева-успешность
     */
    public boolean command(CommandSender sender, String alias, String[] args) {
        return true;
    }

    /**
     * Реализация таба интерфейса
     * @param sender Отправитель
     * @param alias Использованный алиас
     * @param args Аргументы команды
     * @return Таб-строки
     */
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
        var list = new ArrayList<String>();

        // Проходим по субкомандам
        for(var scs : subcommands.values()){

            for (var sc : scs) {
                if (sc.hasPermission() && !sc.hasPermission(sender))
                    continue;

                try {
                    sc.tab(sender, alias, args, list);
                }catch(Exception ignored) { }
            }

        }

        try {
            tab(sender, alias, args, list);
        }catch(Exception ignored) { }

        return list;
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String startsWith, List<String> inputs){
        return addIfStartsWith(list, startsWith, inputs.toArray(new String[0]));
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String startsWith, String... inputs){
        var was = false;

        for(var input : inputs) {

            if (input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length()) {
                list.add(input);
                was = true;
            }

        }

        return was;
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String[] args, int index, List<String> inputs){
        return addIfStartsWith(list, args, index, inputs.toArray(new String[0]));
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String[] args, int index, String... inputs){
        if (args.length != index + 1)
            return false;

        return addIfStartsWith(list, args[index], inputs);
    }

    /**
     * Начинается ли строка с указанной строки
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которой ищем начало
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String[] args, int index, String... inputs){
        return isStartsWith(args, index, false, inputs);
    }

    /**
     * Начинается ли строка с указанной строки
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param ignoreIndex Игнорируем ли длину по индексу
     * @param inputs Строки, в которой ищем начало
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String[] args, int index, boolean ignoreIndex, String... inputs){
        if (!ignoreIndex && args.length != index + 1)
            return false;

        var was = false;

        for (var input : inputs) {
            if (!isStartsWith(input, args[index]))
                continue;

            was = true;
        }

        return was;
    }

    /**
     * Начинается ли строка с указанной строки
     * @param input Строка, в которой ищем начало
     * @param startsWith Начало строки, которое ищем
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String input, String startsWith){

        return input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length();

    }

    /**
     * Имеет ли право нужное отправитель
     * @param sender Отправитель
     * @param permission Право
     * @return Если имеет - true, иначе - false
     */
    public boolean hasPermission(CommandSender sender, String permission){
        return permission != null && !permission.trim().isEmpty() && (sender.isOp() || sender.hasPermission(permission));
    }

    /**
     * Реализация таба интерфейса
     * @param sender Отправитель
     * @param alias Использованный алиас
     * @param args Аргументы команды
     * @param location Локация
     * @return Таб-строки
     */
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args, Location location) {
        return tabComplete(sender, alias, args);
    }

    /**
     * Абстрактная реализация таба
     * @param sender Отправитель
     * @param alias Использованный алиас
     * @param args Аргументы команды
     * @return Таб-строки
     */
    public void tab(CommandSender sender, String alias, String[] args, List<String> list) {}

    /**
     * Получить все классы-субкоманды с помощью рефлексии
     * @param p Пакет
     * @return Список всех субкоманд-классов
     */
    public Set<Class<? extends PluginSubCommand>> getAllSubCommands(String p){

        return Utils.getAllClassesInPackage(p, PluginSubCommand.class);

    }

    /**
     * Получить все инстансы-субкоманды с помощью рефлексии
     * @param p Пакет
     * @return Список всех субкоманд-инстансов
     */
    public Set<PluginSubCommand> getAllSubCommandsInstances(String p){
        HashSet<PluginSubCommand> set = new HashSet<>();

        for (var c : getAllSubCommands(p)){
            PluginSubCommand instance = null;

            try{
                instance = c.getConstructor().newInstance();
            }catch(Exception ignored) { }

            if (instance == null)
                continue;

            set.add(instance);
        }

        return set;
    }

    /**
     * Получить все инстансы-субкоманды с помощью рефлексии по дефолт пакету (package.subcommands)
     * @return Список всех субкоманд-инстансов
     */
    public Set<PluginSubCommand> getAllSubCommandsInstances(){
        return getAllSubCommandsInstances(this.getClass().getPackage().getName() + ".subcommands");
    }

}