package me.wattguy.delivery.commands;

import lombok.Getter;
import lombok.var;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Подкоманда
 * Дата создания: 21:12 07.11.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public abstract class PluginSubCommand {

    /**
     * Триггеры начинающиеся с length = 0
     */
    @Getter
    private final List<Trigger[]> triggers = new ArrayList<>();

    /**
     * Размеры субкоманды
     */
    @Getter
    private final List<Integer> lengths;

    /**
     * Тип отправителя
     */
    @Getter
    private final SenderType senderType;

    /**
     * Право на использование (если не нужно - null)
     */
    @Getter
    private final String permission;

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды
     * @param length Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger[][] triggers, int length, SenderType senderType, String permission){

        if (triggers == null)
            this.triggers.add(new Trigger[0]);
        else {
            for (var array : triggers) {

                if (array == null)
                    this.triggers.add(new Trigger[0]);
                else
                    this.triggers.add(array);

            }
        }

        this.lengths = new ArrayList<Integer>() {{ add(length); }};
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды
     * @param lengths Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger[][] triggers, int[] lengths, SenderType senderType, String permission){

        if (triggers == null)
            this.triggers.add(new Trigger[0]);
        else {
            for (var array : triggers) {

                if (array == null)
                    this.triggers.add(new Trigger[0]);
                else
                    this.triggers.add(array);

            }
        }

        this.lengths = Arrays.stream(lengths).boxed().collect(Collectors.toList());
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param trigger Триггер субкоманды
     * @param length Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger trigger, int length, SenderType senderType, String permission){
        if (trigger == null)
            this.triggers.add(new Trigger[0]);
        else
            this.triggers.add(new Trigger[] { trigger });

        this.lengths = new ArrayList<Integer>() {{ add(length); }};
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param trigger Триггер субкоманды
     * @param lengths Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger trigger, int[] lengths, SenderType senderType, String permission){
        if (trigger == null)
            this.triggers.add(new Trigger[0]);
        else
            this.triggers.add(new Trigger[] { trigger });

        this.lengths = Arrays.stream(lengths).boxed().collect(Collectors.toList());
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды
     * @param lengths Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger[] triggers, int[] lengths, SenderType senderType, String permission){
        if (triggers == null)
            this.triggers.add(new Trigger[0]);
        else
            this.triggers.add(triggers);

        this.lengths = Arrays.stream(lengths).boxed().collect(Collectors.toList());
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды
     * @param length Размер субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(Trigger[] triggers, int length, SenderType senderType, String permission){
        if (triggers == null)
            this.triggers.add(new Trigger[0]);
        else
            this.triggers.add(triggers);

        this.lengths = new ArrayList<Integer>() {{ add(length); }};
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param trigger Триггер субкоманды (уникальные начальные строки)
     * @param length Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(String trigger, int length, SenderType senderType, String permission){
        this.triggers.add(triggers(trigger));
        this.lengths = new ArrayList<Integer>() {{ add(length); }};
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды (уникальные начальные строки)
     * @param lengths Размеры субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(String[] triggers, int[] lengths, SenderType senderType, String permission){
        this.triggers.add(triggers(triggers));
        this.lengths = Arrays.stream(lengths).boxed().collect(Collectors.toList());
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Конструктор
     * @param triggers Триггеры субкоманды (уникальные начальные строки)
     * @param length Размер субкоманды
     * @param senderType Тип отправителя
     * @param permission Право на использование (если не нужно - null)
     */
    public PluginSubCommand(String[] triggers, int length, SenderType senderType, String permission){
        this.triggers.add(triggers(triggers));
        this.lengths = new ArrayList<Integer>() {{ add(length); }};
        this.senderType = senderType;
        this.permission = permission;
    }

    /**
     * Convert string triggers to trigger infos
     * @param triggers String triggers
     * @return Trigger infos
     */
    private Trigger[] triggers(String... triggers){
        if (triggers == null)
            return new Trigger[0];

        List<Trigger> list = new ArrayList<>();

        for (var i = 0; i < triggers.length; i++) {
            var trigger = triggers[i];

            if (trigger == null || trigger.trim().isEmpty())
                continue;

            list.add(new Trigger(i, trigger));
        }

        return list.toArray(new Trigger[0]);
    }

    /**
     * Удовлетворяет ли субкоманда условиям
     * @param sender Отправитель
     * @param args Аргументы
     * @return Если удовлетворяет - true, иначе - false
     */
    public boolean isSatisfy(CommandSender sender, String[] args){
        // Не та длина команды
        if (lengths.size() > 0 && !lengths.contains(args.length))
            return false;

        var satisfaction = false;

        // Проверка на соответствие уникальных триггеров
        for(var index = 0; index < triggers.size(); index++){
            var satisfied = true;
            var array = triggers.get(index);

            for (var trigger : array) {
                if (trigger.isSatisfies(args))
                    continue;

                satisfied = false;
                break;
            }

            if (!satisfied)
                continue;

            satisfaction = true;
            break;
        }

        if (!satisfaction)
            return false;

        // Не имеет нужного права
        if (hasPermission() && !hasPermission(sender))
            return false;

        // Не тот тип отправителя
        if (!senderType.isSatisfy(sender)) {
            sender.sendMessage("This command only for " + senderType);
            return false;
        }

        return true;
    }

    /**
     * Обработка команды, если удовлетворяет - возвращаем true
     * @param sender Отправитель
     * @param alias Алиас
     * @param args Аргументы
     */
    public abstract boolean execute(CommandSender sender, String alias, String[] args);

    /**
     * Обработка предложений
     * @param sender Отправитель
     * @param alias Алиас
     * @param args Аргументы
     * @param list Предложения
     */
    public abstract void tab(CommandSender sender, String alias, String[] args, List<String> list);

    /**
     * Имеет ли право нужное отправитель
     * @param sender Отправитель
     * @return Если имеет - true, иначе - false
     */
    public boolean hasPermission(CommandSender sender){
        return sender.isOp() || sender.hasPermission(permission);
    }

    /**
     * Has command permission
     * @return Has permission or not
     */
    public boolean hasPermission(){
        return permission != null && !permission.trim().isEmpty();
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String startsWith, Collection<String> inputs){
        return addIfStartsWith(list, startsWith, inputs.toArray(new String[0]));
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param startsWith Начало строки, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String startsWith, String... inputs){
        var was = false;

        for(var input : inputs) {

            if (input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length()) {
                list.add(input);
                was = true;
            }

        }

        return was;
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String[] args, int index, Collection<String> inputs){
        return addIfStartsWith(list, args, index, inputs.toArray(new String[0]));
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которых ищем начало
     */
    public boolean addIfStartsWith(List<String> list, String[] args, int index, String... inputs){
        if (args.length != index + 1)
            return false;

        return addIfStartsWith(list, args[index], inputs);
    }

    /**
     * Начинается ли строка с указанной строки
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param inputs Строки, в которой ищем начало
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String[] args, int index, String... inputs){
        return isStartsWith(args, index, false, inputs);
    }

    /**
     * Начинается ли строка с указанной строки
     * @param args Аргументы начал строк, которое ищем
     * @param index Индекс аргументов начал строк, которое ищем
     * @param ignoreIndex Игнорируем ли длину по индексу
     * @param inputs Строки, в которой ищем начало
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String[] args, int index, boolean ignoreIndex, String... inputs){
        if (!ignoreIndex && args.length != index + 1)
            return false;

        var was = false;

        for (var input : inputs) {
            if (!isStartsWith(input, args[index]))
                continue;

            was = true;
        }

        return was;
    }

    /**
     * Начинается ли строка с указанной строки
     * @param input Строка, в которой ищем начало
     * @param startsWith Начало строки, которое ищем
     * @return Нашли ли такое начало в строке
     */
    public boolean isStartsWith(String input, String startsWith){

        return input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length();

    }

    /**
     * Тип отправителя
     */
    public enum SenderType {
        PLAYER, CONSOLE, RCON, CONSOLE_AND_RCON, ALL;

        /**
         * Удовлетворяет ли типу отправитель
         * @param sender Отправитель
         * @return Если удовлетворяет - true, иначе - false
         */
        public boolean isSatisfy(CommandSender sender){
            var type = values()[ordinal()];

            switch(type){

                case PLAYER:
                    return sender instanceof Player;

                case CONSOLE:
                    return sender instanceof ConsoleCommandSender;

                case RCON:
                    return sender instanceof RemoteConsoleCommandSender;

                case CONSOLE_AND_RCON:
                    return sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender;

                default:
                    return true;

            }
        }

    }

}