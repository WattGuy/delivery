package me.wattguy.delivery.commands;

import lombok.Getter;
import lombok.Setter;
import lombok.var;

import java.util.List;

/**
 * Trigger info
 * Creation date: 13:38 17.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public class Trigger {

    /**
     * Argument index
     */
    @Getter
    @Setter
    private int index = -1;

    /**
     * Aliases
     */
    @Getter
    private final String[] aliases;

    /**
     * Constructor
     * @param aliases Triggers for one argument
     */
    public Trigger(String... aliases){
        if (aliases == null || aliases.length <= 0)
            aliases = new String[0];

        this.aliases = aliases;
    }

    /**
     * Constructor
     * @param index Argument index
     * @param aliases Triggers for one argument
     */
    public Trigger(int index, String... aliases){
        this(aliases);

        this.index = index;
    }

    /**
     * Whether the string satisfies the needs by arguments and given index
     * @param args Arguments
     * @return Satisfies or not
     */
    public boolean isSatisfies(String[] args){
        if (args.length < index + 1)
            return false;

        return isSatisfies(args[index]);
    }

    /**
     * Whether the string satisfies the needs
     * @param input Input
     * @return Satisfies or not
     */
    public boolean isSatisfies(String input){
        for (var alias : aliases) {
            if (!alias.equalsIgnoreCase(input))
                continue;

            return true;
        }

        return false;
    }

    /**
     * Добавляем в массив строку, если нашли указанное начало
     * @param list Массив строк, куда будет по итогу добавляться строка при соответствии
     * @param args Аргументы
     * @return Добавлено или нет
     */
    public boolean addIfStartsWith(List<String> list, String[] args){
        if (index + 1 != args.length)
            return false;

        var startsWith = args[index];
        var was = false;

        for(var input : aliases) {
            if (!(input.toLowerCase().startsWith(startsWith) && startsWith.length() <= input.length()))
                continue;

            list.add(input);
            was = true;
        }

        return was;
    }

    /**
     * Is trigger situated in arguments
     * @param args Arguments
     * @return Is situated or not
     */
    public boolean isSituated(String[] args){
        if (index + 1 > args.length)
            return false;

        var target = args[index];
        var was = false;

        for(var input : aliases) {
            if (!(input.equalsIgnoreCase(target)))
                continue;

            was = true;
            break;
        }

        return was;
    }

}
