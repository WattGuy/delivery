package me.wattguy.delivery.commands;

import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.initialize.Initializable;
import org.bukkit.command.CommandMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Класс, проводящий операции с командами в плагине
 * Дата создания: 14:57 25.08.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class CommandManager implements Initializable {

    /**
     * Список всех команд плагина
     * Формат - Map<[название команды], PluginCommand [реализация]>
     */
    private static final Map<String, PluginCommand> commands = new HashMap<>();

    /**
     * Инициализация всех команд
     */
    @Override
    public void initialize(){
        CommandMap commandMap = null;

        // Получаем регистратор команд сервера с помощью рефлексии
        try {
            var bukkitCommandMap = Main.getInstance().getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);

            commandMap = (CommandMap) bukkitCommandMap.get(Main.getInstance().getServer());
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ignored) {}

        // Если не удалось получить регистратор команд - уходим
        if (commandMap == null)
            return;

        // Инициализируем все команды
        for (var command : getAllCommands()) {
            PluginCommand instance = null;

            try {
                instance = command.getConstructor().newInstance();
            }catch(Exception ignored) { }

            // Если команда неверно реализована или уже инициализирована - уходим
            if (instance == null || commands.containsKey(instance.getName()))
                continue;

            // Регистрируем команду на сервере
            commandMap.register(instance.getName(), instance);

            // Заносим команду в хранилище
            commands.put(instance.getName(), instance);
        }
    }

    /**
     * Получить все классы-команды с помощью рефлексии
     * @return Список всех команд-классов
     */
    public static Set<Class<? extends PluginCommand>> getAllCommands(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(CommandManager.class.getPackage().getName(), 1), PluginCommand.class);

    }

}
