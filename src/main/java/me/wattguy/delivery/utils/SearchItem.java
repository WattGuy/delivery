package me.wattguy.delivery.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.lemar98.regionshops.model.Shop;
import ru.lemar98.regionshops.model.ShopItem;

@AllArgsConstructor
@Getter
public class SearchItem {

    private Shop shop;

    private ShopItem item;

}
