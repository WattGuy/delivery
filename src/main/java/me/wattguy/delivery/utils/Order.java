package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Messages;
import me.wattguy.delivery.extensions.GPSExtension;
import me.wattguy.delivery.extensions.RegionShopsExtension;
import me.wattguy.delivery.managers.CouriersManager;
import me.wattguy.delivery.managers.OrdersManager;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.plain.PlainComponentSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.lemar98.regionshops.model.Shop;
import ru.lemar98.regionshops.model.ShopItem;

import java.util.*;

@RequiredArgsConstructor
public class Order {

    @Getter
    private final String id;

    @Getter
    private final Customer customer;

    @Getter
    @Setter
    private String shopName = null;

    @Getter
    @Setter
    private Courier courier = null;

    @Getter
    private final List<Item> items = new ArrayList<>();

    @Getter
    @Setter
    private State state = State.BASKET;

    @Getter
    @Setter
    private Double deliveryPrice = null;

    @Getter
    @Setter
    private Location deliveryLocation = null;

    @Getter
    @Setter
    private Integer itemsPrice = null;

    public void sendCustomer(String path, boolean itemsVariable, boolean placeholderCourier){

        Utils.send(
                customer.getPlayer(),
                path,
                placeholderCourier && courier != null ? courier.getPlayer() : null,
                getVariables(this, customer.getPlayer().getLocation(), itemsVariable)
        );

    }

    public void sendCourier(String path, boolean itemsVariable, boolean placeholderCustomer){

        Utils.send(
                courier.getPlayer(),
                path,
                placeholderCustomer ? customer.getPlayer() : null,
                getVariables(this, customer.getPlayer().getLocation(), itemsVariable)
        );

    }

    public void sendAllCouriers(){
        for (var courier : CouriersManager.getInstances().values()) {
            if (courier == null || courier.getOrder() != null)
                continue;

            courier.send(this);
        }
    }

    public double getDeliveryPriceFromLocation(Location from){
        return RegionShopsExtension.getDeliveryPrice(shopName, from);
    }

    public Utils.P[] getVariables(Location from){
        return getVariables(this, from, true);
    }

    public Utils.P[] getVariables(){
        return getVariables(this);
    }

    public static Utils.P[] getVariables(Order order, Location from, boolean itemsVariable){
        var deliveryPrice = order != null ? (order.getDeliveryPrice() != null ? order.getDeliveryPrice() : order.getDeliveryPriceFromLocation(from)) : 0d;
        var itemsPrice = order != null ? order.getPrice() : 0;
        List<String> items_lore = new ArrayList<>();

        List<Order.Item> items = itemsVariable && order != null ? order.getItems() : null;

        if (itemsVariable) {
            var template = Utils.toColor(Messages.get().getString("order_items.template", "&7{name} x{amount} = {price}$"));
            var empty = Utils.toColor(Messages.get().getString("order_items.empty", "&7нет"));

            if (items != null && items.size() > 0) {
                for (var i : items) {
                    items_lore.add(
                            Utils.replace(
                                    template,
                                    new Utils.P("{name}", i.getName(i.getItem().getItemStack().getType().toString(), true)),
                                    new Utils.P("{price}", i.getPrice() + ""),
                                    new Utils.P("{amount}", i.getAmount() + "")
                            )
                    );
                }
            } else
                items_lore.add(Utils.toColor(empty));
        }

        return new Utils.P[] {
                new Utils.P("{items}", items_lore, true),
                new Utils.P("{order}", order != null ? order.getId() : "нет"),
                new Utils.P("{customer}", order != null ? order.getCustomer().getPlayer().getName() : "нет"),
                new Utils.P("{courier}", order != null && order.getCourier() != null ? order.getCourier().getPlayer().getName() : "нет"),
                new Utils.P("{delivery_price}", Utils.doubleToString(deliveryPrice)),
                new Utils.P("{items_price}", itemsPrice + ""),
                new Utils.P("{price}", Utils.doubleToString(deliveryPrice + (double) itemsPrice)),
                new Utils.P("{shop_name}", order != null && order.getShopName() != null ? order.getShopName() : "нет")
        };
    }

    public List<Item.Stack> getItemStacks(){
        List<Item.Stack> list = new ArrayList<>();

        for (var item : getItems()) {
            list.addAll(item.stacks());
        }

        return list;
    }

    public static Utils.P[] getVariables(Order order){
        return getVariables(order, null, true);
    }

    public int getPrice(){
        if (itemsPrice != null)
            return itemsPrice;

        var i = 0;

        for (var item : items)
            i += item.getPrice();

        return i;
    }

    public Optional<Shop> getShop(){
        return RegionShopsExtension.getShop(shopName);
    }

    public Optional<Item> getItem(ShopItem shopItem, boolean force){
        for (var i : items) {
            if (!i.getItem().equals(shopItem))
                continue;

            return Optional.of(i);
        }

        if (!force)
            return Optional.empty();

        var item = new Item(this, shopItem);
        items.add(item);

        return Optional.of(item);
    }

    public Map<ItemStack, Integer> getNeedMap(){
        Map<ItemStack, Integer> map = new HashMap<>();

        for (var item : items)
            map.put(item.getItem().getItemStack(), item.getAmount());

        return map;
    }

    public boolean hasItems(Player player){
        return Utils.hasItems(player.getInventory(), getNeedMap());
    }

    public boolean takeItemsIfHas(Player player){
        return Utils.takeItemsIfHas(player.getInventory(), getNeedMap());
    }

    public boolean isOutOfStock(){
        var shop = getShop().orElse(null);

        if (shop == null)
            return false;

        for (var item : getItems()) {
            if (shop.inStore(item.getItem(), item.getAmount()))
                continue;

            return true;
        }

        return false;
    }

    public void addItems(Player player){
        for (var item : items) {
            var is = build(item.getItem().getItemStack(), new ArrayList<>());

            List<ItemStack> list = new ArrayList<>();

            if (item.getAmount() > is.getType().getMaxStackSize()){
                var maxStack = item.getAmount() / is.getType().getMaxStackSize();
                var remain = item.getAmount() % is.getType().getMaxStackSize();

                for (var i = 0; i < maxStack; i++)
                    list.add(build(is, is.getType().getMaxStackSize()));

                if (remain > 0)
                    list.add(build(is, remain));
            }else {
                is.setAmount(item.getAmount());
                list.add(is);
            }

            for (var itemStack : list) {
                if (player.getInventory().firstEmpty() == -1)
                    player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
                else
                    player.getInventory().addItem(itemStack);
            }
        }
    }

    private static ItemStack build(ItemStack itemStack, int amount){
        var is = itemStack.clone();
        is.setAmount(amount);

        return is;
    }

    private static ItemStack build(ItemStack itemStack, List<String> lore) {
        List<Component> coloredLore;
        ItemStack stack = itemStack.clone();
        ItemMeta itemMeta = stack.getItemMeta();

        List<Component> list = coloredLore = itemMeta.lore() != null && itemMeta.hasLore() ? itemMeta.lore() : new ArrayList<>();
        if (!lore.isEmpty())
            lore.forEach(row -> coloredLore.add(
                    PlainComponentSerializer
                    .plain()
                    .deserialize(ChatColor.translateAlternateColorCodes('&', row)))
            );

        itemMeta.lore(coloredLore.isEmpty() ? null : coloredLore);
        stack.setItemMeta(itemMeta);
        return stack;
    }

    public void courierBack(){
        if (courier == null)
            return;

        Utils.send(courier.getPlayer(), "end.courier_back", new Utils.P("{price}", getPrice() + ""));

        Main.getEconomy().depositPlayer(courier.getPlayer(), getPrice());
        delete();

        courier.setOrder(null);

        Utils.send(courier.getPlayer(), "deliver.again");
        courier.sendSearchOrders();

        courier.reset();
    }

    public void customerBack(){
        if (courier != null)
            courier.reset();

        if (customer.getPlayer() != null)
            Utils.send(customer.getPlayer(), "end.customer_back", new Utils.P("{price}", getPrice() + ""));

        Main.getEconomy().depositPlayer(customer.getPlayer(), getPrice());
        delete();
    }

    public void endByTime(){
        if (courier != null) {
            if (courier.getPlayer() != null) {
                Utils.send(courier.getPlayer(), "end.time.courier", new Utils.P("{price}", getPrice() + ""));

                var name = courier.getPlayer().getName();

                GPSExtension.stop(courier.getPlayer());

                CouriersManager.block(name);
                CouriersManager.remove(courier.getPlayer());
            }

            courier.reset();
        }

        Utils.send(customer.getPlayer(), "end.time.customer", new Utils.P("{price}", getPrice() + ""));

        Main.getEconomy().depositPlayer(customer.getPlayer(), getPrice());
        delete();
    }

    public void delete(){
        OrdersManager.getInstances().remove(id.toLowerCase());
        OrdersManager.getCustomers().remove(customer.getPlayer().getName().toLowerCase());

        if (courier != null)
            OrdersManager.getCouriers().remove(courier.getPlayer().getName().toLowerCase());
    }

    @RequiredArgsConstructor
    public static class Item {

        @Getter
        private final Order order;

        @Getter
        private final ShopItem item;

        @Getter
        @Setter
        private int amount = 0;

        public String getName(String def, boolean strip){
            return Utils.getName(item.getItemStack(), def, strip);
        }

        public List<String> getLore(){
            return Utils.getLore(item.getItemStack());
        }

        public int getPrice(){
            var shop = order.getShop().orElse(null);

            if (shop == null)
                return 0;

            var price = shop.getStoredItemPrice(item.getItemStack());

            if (price == -1)
                return 0;

            return price * amount;
        }

        public List<Stack> stacks(){
            List<Stack> list = new ArrayList<>();

            var maxStackSize = item.getItemStack().getType().getMaxStackSize();

            var maxStack = amount / maxStackSize;
            var remain = amount % maxStackSize;

            for (var i = 0; i < maxStack; i++)
                list.add(new Stack(this, maxStackSize));

            if (remain > 0)
                list.add(new Stack(this, remain));

            return list;
        }

        @RequiredArgsConstructor
        public static class Stack {

            @Getter
            private final Item item;

            @Getter
            private final int amount;

            public int getPrice(){
                return item.getItem().getPrice() * amount;
            }

        }

    }

    public static enum State {
        BASKET, SEARCH_COURIER, COURIER_GOES_TO_STORE, COURIER_BUYING, COURIER_COMING_TO_CUSTOMER;
    }

}
