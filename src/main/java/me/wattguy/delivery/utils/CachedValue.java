package me.wattguy.delivery.utils;

import lombok.Getter;

import java.util.function.Function;

/**
 * Cached value
 * Creation date: 16:30 07.03.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public class CachedValue<K, V> {

    /**
     * Update once in x milliseconds
     */
    @Getter
    private final long time;

    /**
     * Callable function
     */
    @Getter
    private final Function<K, V> function;

    /**
     * Cached value
     */
    private V value = null;

    /**
     * Last update
     */
    private long lastUpdate = -1;

    /**
     * Constructor
     * @param time Once update in x milliseconds
     * @param function Function
     */
    public CachedValue(long time, Function<K, V> function){
        this.time = time;
        this.function = function;
    }

    /**
     * Get value
     * @return Value
     */
    public V getValue(K key) {
        if (value == null || isInactive())
            update(key);

        return value;
    }

    /**
     * Is inactive or not
     * @return Inactive or not
     */
    public boolean isInactive(){
        return (System.currentTimeMillis() - lastUpdate) >= time;
    }

    /**
     * Update value
     * @return New value
     */
    public V update(K key){
        lastUpdate = System.currentTimeMillis();

        try {
            value = function.apply(key);
        } catch (Exception ignored) { }

        return value;
    }

    /**
     * Clear cache
     */
    public void clear(){
        value = null;
        lastUpdate = -1;
    }

}
