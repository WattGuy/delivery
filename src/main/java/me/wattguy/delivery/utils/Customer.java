package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.Player;

@RequiredArgsConstructor
public class Customer {

    @Getter
    private final Player player;

    @Getter
    @Setter
    private Order order = null;

}
