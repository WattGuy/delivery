package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.listeners.PluginListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Слушатель чата, привязанный к игроку
 * Дата создания: 08:57 06.11.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class Input extends PluginListener {

    /**
     * Все экземпляры все слушатели чата
     */
    @Getter
    private static final Set<Input> instances = new HashSet<>();

    /**
     * Содержит ли слушатель чата на игрока
     * @param player Игрок
     * @return Если содержит - слушатель, если нет - null
     */
    public static Input get(Player player){
        for (var c : instances) {
            if (c.getPlayer() == null || !c.getPlayer().equals(player))
                continue;

            return c;
        }

        return null;
    }

    /**
     * Дерегистрировать все слушатели чата
     */
    public static void unregisterAll(){

        for (var c : new HashSet<>(instances)) {
            c.unregister();
        }

    }

    /**
     * Игрок
     */
    @Getter
    private final Player player;

    /**
     * Действие при вводе в чат
     */
    @Getter
    @Setter
    private Consumer<AsyncPlayerChatEvent> chatAction = null;

    /**
     * Действие при вводе команды
     */
    @Getter
    @Setter
    private Consumer<PlayerCommandPreprocessEvent> commandAction = null;

    /**
     * Действие при отмене ввода
     */
    @Getter
    @Setter
    private Runnable cancelAction = null;

    /**
     * Конструктор с авторегистрацией слушателя
     * @param player Игрок
     */
    public Input(Player player){
        this.player = player;

        register();
    }

    /**
     * Конструктор с авторегистрацией слушателя
     * @param player Игрок
     * @param chatAction Действие с проверкой на ввод в чат
     * @param commandAction Действие с проверкой на ввод команды
     */
    public Input(Player player, Consumer<AsyncPlayerChatEvent> chatAction, Consumer<PlayerCommandPreprocessEvent> commandAction){
        this.player = player;
        this.chatAction = chatAction;
        this.commandAction = commandAction;

        register();
    }

    /**
     * Ивент, отслеживающий ввод команд
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent e){
        if (!e.getPlayer().equals(player))
            return;

        if (e.getMessage().trim().equalsIgnoreCase("/cancel")) {
            Utils.send(player, "input.cancelled");
            e.setCancelled(true);
            unregister();

            if (cancelAction != null)
                cancelAction.run();

            return;
        }

        if (commandAction == null)
            return;

        commandAction.accept(e);
    }

    /**
     * Ивент, отслеживающий чат игрока
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent e){
        if (!e.getPlayer().equals(player))
            return;

        if (chatAction == null)
            return;

        chatAction.accept(e);
    }

    /**
     * Ивент, отслеживающий выход игрока
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent e){
        if (!(player != null && player.equals(e.getPlayer())))
            return;

        unregister();
    }

    /**
     * Оверрайд на занесение экземпляра при регистрации
     */
    @Override
    public void register(){
        super.register();

        instances.add(this);
    }

    /**
     * Оверрайд на удаление экземпляра при дерегистрации
     */
    @Override
    public void unregister(){
        super.unregister();

        instances.remove(this);
    }

}
