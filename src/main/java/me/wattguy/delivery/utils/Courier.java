package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.extensions.GPSExtension;
import me.wattguy.delivery.extensions.RegionShopsExtension;
import me.wattguy.delivery.managers.OrdersManager;
import org.bukkit.entity.Player;

import java.util.*;

@RequiredArgsConstructor
public class Courier {

    @Getter
    private final Player player;

    @Getter
    @Setter
    private Order order = null;

    @Getter
    @Setter
    private Long until = null;

    @Getter
    private final Map<String, Long> timestamps = Collections.synchronizedMap(new HashMap<>());

    public void sendSearchOrders(){
        for (var order : OrdersManager.getSearchOrders()) {
            if (isInCooldown(order))
                continue;

            send(order);
        }
    }

    public void send(Order order){
        if (order == null || isInCooldown(order))
            return;

        Utils.send(player, "deliver.order", order.getCustomer().getPlayer(),
                order.getVariables()
        );
    }

    public void checkTime(){
        if (until == null)
            return;

        if (order == null) {
            until = null;
            return;
        }

        if (until > System.currentTimeMillis())
            return;

        order.endByTime();
    }

    public void checkCooldowns(){
        List<String> remove = new ArrayList<>();

        for (var entry : timestamps.entrySet()) {
            if (entry.getValue() < entry.getValue())
                continue;

            remove.add(entry.getKey());
        }

        for (var s : remove)
            timestamps.remove(s);
    }

    public void checkCompletion(){
        if (order == null || order.getState() == Order.State.COURIER_COMING_TO_CUSTOMER || !order.hasItems(player))
            return;

        var order = getOrder();

        order.setState(Order.State.COURIER_COMING_TO_CUSTOMER);

        if (Config.get().isSet("time.to_customer"))
            setUntil(System.currentTimeMillis() + (Config.get().getInt("time.to_customer") * 1000L));

        gpsToCustomer();

        order.sendCustomer("order.coming", true, true);
        order.sendCourier("deliver.coming", true, true);
    }

    public boolean isInCooldown(Order order){
        return isInCooldown(order.getId());
    }

    public boolean isInCooldown(String orderId){
        orderId = orderId.toLowerCase();

        var until = timestamps.getOrDefault(orderId, null);

        if (until == null)
            return false;

        var b = System.currentTimeMillis() < until;

        if (!b)
            timestamps.remove(orderId);

        return b;
    }

    public void cooldown(Order order){
        if (order == null)
            return;

        cooldown(order.getId());
    }

    public void stopGPS(){
        GPSExtension.stop(player);
    }

    public void gpsToStore(){
        if (order == null || order.getShopName() == null)
            return;

        var location = RegionShopsExtension.getShopLocation(order.getShopName()).orElse(null);

        if (location == null)
            return;

        GPSExtension.gps(
                player,
                location,
                Utils.replace(
                        Config.get().getString("gps.shop", "Магазин"),
                        Order.getVariables(order, null, false)
                ),
                true
        );
    }

    public void gpsToCustomer(){
        if (order == null || order.getDeliveryLocation() == null)
            return;

        GPSExtension.gps(
                player,
                order.getDeliveryLocation(),
                Utils.replace(
                        Config.get().getString("gps.customer", "Заказчик"),
                        Order.getVariables(order, null, false)
                ),
                true
        );
    }

    public void cooldown(String orderId){
        timestamps.put(orderId.toLowerCase(), System.currentTimeMillis() + (Config.get().getLong("repeat_order") * 1000L));
    }

    public void reset(){
        order.setCourier(null);
        order = null;
        until = null;
    }

}
