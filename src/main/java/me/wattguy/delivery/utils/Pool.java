package me.wattguy.delivery.utils;

import com.tcoded.folialib.wrapper.task.WrappedTask;
import lombok.Getter;
import lombok.Setter;
import me.wattguy.delivery.Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Пул задач
 * Дата создания: 12:56 04.09.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class Pool<T> {

    /**
     * Метод-исполнитель
     */
    private final Consumer<List<T>> runner;

    /**
     * Главный шедулер
     */
    @Getter
    private WrappedTask scheduler = null;

    /**
     * Объекты-цели задач
     */
    @Getter
    private final List<T> objects = Collections.synchronizedList(new ArrayList<>());

    /**
     * Промежуток выполнения
     */
    @Getter
    @Setter
    private long delay;

    /**
     * Единица измерения времени промежутка выполнения
     */
    @Getter
    @Setter
    private long unit;

    /**
     * Остановлен ли пул
     */
    @Getter
    private boolean stopped = true;

    @Getter
    private boolean async = false;

    /**
     * Конструктор
     * @param delay Задержка
     * @param unit Вес единицы задержки
     * @param runner Главный метод-обработчик с целями
     */
    public Pool(long delay, long unit, Consumer<List<T>> runner){
        this(delay, unit, true, runner);
    }

    /**
     * Конструктор
     * @param delay Задержка
     * @param unit Вес единицы задержки
     * @param runner Главный метод-обработчик с целями
     */
    public Pool(long delay, long unit, boolean async, Consumer<List<T>> runner){
        this.runner = runner;
        this.delay = delay;
        this.unit = unit;
        this.async = async;

        start();
    }

    /**
     * Запустить выполнение пула
     */
    public void start(){
        if (!stopped)
            return;

        if (async)
            scheduler = Main.getFoliaLib().getImpl().runTimerAsync(Pool.this::run, delay, delay);
        else
            scheduler = Main.getFoliaLib().getImpl().runTimer(Pool.this::run, delay, delay);
    }

    public Pool<T> setAsync(boolean async) {
        this.async = async;

        return this;
    }

    /**
     * Запущен ли пул
     * @return Запущен или нет
     */
    public boolean isStarted(){
        return !stopped;
    }

    /**
     * Метод-исполнитель метода-обработчика
     */
    private void run(){
        try {
            // Запускаем метод-обработчик
            runner.accept(objects);

            // Очищаем
            objects.clear();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Добавить объекты в задачи
     * @param os Объекты
     */
    public void add(T... os){

        // Добавляем объекты если они не были добавлены
        for (T o : os) {
            // Если этот объект уже добавлен - уходим
            if (objects.contains(o))
                continue;

            // Добавляем
            objects.add(o);
        }

    }

    /**
     * Остановить выполнение пула
     */
    public void stop(){
        // Если уже остановлен - уходим
        if (stopped)
            return;

        // Останавливаем главный шедулер
        if (scheduler != null && !scheduler.isCancelled())
            scheduler.cancel();

        scheduler = null;

        // Заносим что остановлен
        stopped = true;
    }

}
