package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.ConfigItemStack;
import me.wattguy.delivery.managers.PersistenceManager;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Item extends ConfigItemStack {

    @Getter
    private Object slot = null;

    @Getter
    @Setter
    private int amount = 1;

    public ItemStack get(Player player){

        if (slot instanceof EquipmentSlot)
            return player.getInventory().getItem((EquipmentSlot) slot);
        else if (slot instanceof Integer)
            return player.getInventory().getItem((Integer) slot);
        else
            return null;

    }

    public void set(Player player, Utils.P... ps){
        if (player == null)
            return;

        var is = itemStack(amount, player, ps);

        if (slot instanceof EquipmentSlot)
            player.getInventory().setItem((EquipmentSlot) slot, is);
        else if (slot instanceof Integer)
            player.getInventory().setItem((Integer) slot, is);
    }

    @Override
    public void extendedItemMetaModification(Player player, ItemMeta meta) {
        PersistenceManager.set(meta, PersistentDataType.BYTE, "delivery_item", (byte) 0);
    }

    @Override
    public List<String> extendedInitialization(Map<?, ?> map) {
        // Слот
        if (map.containsKey("slot")) {
            var o = map.get("slot");
            var was = false;

            if (o instanceof String){
                var equipmentSlot = Utils.getEnumByName((String) o, EquipmentSlot.class, null);

                if (equipmentSlot != null) {
                    was = true;
                    slot = equipmentSlot;
                }
            }

            if (!was)
                slot = Utils.getInteger(o, 1) - 1;
        }

        // Количество
        if (map.containsKey("amount")){
            amount = Utils.getInteger(map.get("amount"), 1);

            if (amount < 1)
                amount = 1;
        }

        return Arrays.asList("slot", "amount");
    }

    public static boolean is(ItemStack itemStack){
        Byte o = PersistenceManager.get(itemStack, PersistentDataType.BYTE, "delivery_item");

        return o != null && o == (byte) 0;
    }

}
