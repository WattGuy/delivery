package me.wattguy.delivery.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.guis.PluginGUI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Ивент команды
 */
@RequiredArgsConstructor
public class CommandEvent {

    /**
     * Перечень префиксов в командах
     */
    public enum Prefix {
        PLAYER, CONSOLE
    }

    /**
     * Команды
     */
    @Getter
    private final List<String> commands;

    /**
     * GUI
     */
    @Getter
    private PluginGUI gui = null;

    /**
     * Предмет, который спровоцировал
     */
    @Getter
    private PluginGUI.Item item = null;

    /**
     * Отменён ли ивент
     */
    @Getter
    @Setter
    private boolean cancelled = false;

    /**
     * Constructor
     * @param commands Commands
     * @param gui GUI
     * @param item Item
     */
    public CommandEvent(List<String> commands, PluginGUI gui, PluginGUI.Item item){
        this.commands = commands;
        this.gui = gui;
        this.item = item;
    }

    /**
     * Конструктор по одной команде
     * @param command Команда
     * @param item Предмет, который спровоцировал
     */
    public CommandEvent(String command, PluginGUI gui, PluginGUI.Item item){
        this(new ArrayList<String>(){{ add(command); }}, gui, item);
    }

    /**
     * Конструктор по одной команде
     * @param command Команда
     */
    public CommandEvent(String command){
        this.commands = new ArrayList<String>(){{ add(command); }};
    }

    /**
     * Исполнение команд от игрока
     * @param p Игрок
     */
    public void execute(Player p){
        if (commands == null || commands.size() <= 0)
            return;

        for (var cmd : commands) {

            execute(p, cmd);

        }
    }

    /**
     * Исполнение команды от игрока
     * @param p Игрок
     * @param rawCommand Сырая команда (с префиксами)
     */
    private void execute(Player p, String rawCommand){
        var prefix = Prefix.PLAYER;
        var command = rawCommand;

        if (rawCommand.contains(":")) {
            var splitted = rawCommand.split(":");
            Prefix tempPrefix = null;

            try {
                tempPrefix = Prefix.valueOf(splitted[0].toUpperCase());
            }catch(Exception ignored) { }

            if (tempPrefix != null) {
                prefix = tempPrefix;
                command = rawCommand.substring(rawCommand.indexOf(":") + 1).trim();
            }
        }

        if (gui != null && item != null)
            command = Utils.replace(command, gui.commandVariables(p, prefix, command, item));

        command = Utils.replace(command, new Utils.P("{name}", p.getName()));

        switch (prefix) {

            case CONSOLE: {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                break;
            }

            case PLAYER:
            default: {
                Bukkit.dispatchCommand(p, command);
                break;
            }

        }
    }

}