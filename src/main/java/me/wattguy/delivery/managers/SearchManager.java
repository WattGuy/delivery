package me.wattguy.delivery.managers;

import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.extensions.RegionShopsExtension;
import me.wattguy.delivery.guis.interceptors.SearchItems;
import me.wattguy.delivery.utils.Input;
import me.wattguy.delivery.utils.SearchItem;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SearchManager {

    public static void input(Player p){
        if (Input.get(p) != null)
            return;

        Utils.send(p, "input.search.process", p, new Utils.P("{player}", p.getName()));

        var i = new Input(p);

        i.setChatAction((e) -> {
            e.setCancelled(true);

            // Получаем текст объявления
            var text = e.getMessage().trim().toLowerCase();

            if (text.isEmpty() || text.length() < Config.get().getInt("chars.min") || text.length() > Config.get().getInt("chars.max")){
                Utils.send(p, "input.search.wrong_format");
                return;
            }

            i.unregister();

            Utils.send(p, "input.search.success", p, new Utils.P("{player}", p.getName()), new Utils.P("{text}", text));

            SearchItems.open(p, text);
        });

        Utils.closeInventory(p);
    }

    public static List<SearchItem> search(String s){
        s = s.toLowerCase();
        List<SearchItem> items = new ArrayList<>();

        for (var shop : RegionShopsExtension.getShops().values()) {
            mainFor: for (var sellItem : shop.getSellItems()) {
                if (Utils.getStoredAmount(shop, sellItem) <= 0)
                    continue;

                var is = sellItem.getItemStack();

                var name = Utils.getName(is, "", false).toLowerCase();

                if (name.contains(s)){
                    items.add(new SearchItem(shop, sellItem));
                    continue;
                }

                var lore = sellItem.getLore();

                for (var line : lore) {
                    if (line.toLowerCase().contains(s)){
                        items.add(new SearchItem(shop, sellItem));
                        continue mainFor;
                    }
                }
            }
        }

        return items;
    }

}
