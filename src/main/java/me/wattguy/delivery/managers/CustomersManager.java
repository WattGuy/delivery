package me.wattguy.delivery.managers;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.listeners.PluginListener;
import me.wattguy.delivery.utils.Customer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CustomersManager {

    @Getter
    private static final Map<String, Customer> instances = new HashMap<>();

    public static Optional<Customer> get(Player player, boolean force){
        var customer = instances.getOrDefault(player.getName().toLowerCase(), null);

        if (customer != null)
            return Optional.of(customer);

        if (!force)
            return Optional.empty();

        customer = new Customer(player);
        instances.put(player.getName().toLowerCase(), customer);

        return Optional.of(customer);
    }

    public static Customer remove(String name){
        return instances.remove(name.toLowerCase());
    }

    public static class Listener extends PluginListener {

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onQuit(PlayerQuitEvent e){
            remove(e.getPlayer().getName());
        }

    }

}
