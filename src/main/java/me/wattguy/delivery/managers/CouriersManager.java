package me.wattguy.delivery.managers;

import com.tcoded.folialib.wrapper.task.WrappedTask;
import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.events.DeliveryCompleteEvent;
import me.wattguy.delivery.extensions.GPSExtension;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;
import me.wattguy.delivery.listeners.PluginListener;
import me.wattguy.delivery.utils.Courier;
import me.wattguy.delivery.utils.Order;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.lemar98.regionshops.events.OpenShopEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CouriersManager implements Initializable, Unregistrable {

    @Getter
    private static final Map<String, Courier> instances = new HashMap<>();

    @Getter
    private static final Map<String, Long> block = new HashMap<>();

    private static WrappedTask scheduler = null;

    @Override
    @Settings(behaviour = Settings.Behaviour.ONCE)
    public void initialize() {

        if (scheduler == null || scheduler.isCancelled())
            scheduler = Main.getFoliaLib().getImpl().runTimer(CouriersManager::check, 20L, 20L);

    }

    @Override
    public void unregister() {
        if (scheduler != null && !scheduler.isCancelled())
            scheduler.cancel();

        scheduler = null;
    }

    public static boolean isBlocked(String s){
        return block.getOrDefault(s.toLowerCase(), 0L) > System.currentTimeMillis();
    }

    public static void block(String s){
        if (!Config.get().isSet("time.block"))
            return;

        block.put(s.toLowerCase(), System.currentTimeMillis() + (Config.get().getInt("time.block") * 1000L));
    }

    public static void check(){
        for (var courier : new ArrayList<>(instances.values())) {
            courier.checkCooldowns();
            courier.checkTime();
            courier.checkCompletion();
        }
    }

    public static void add(Player player){
        var courier = get(player, true).orElse(null);

        ItemsManager.give(player);
        Utils.send(player, "deliver.started");

        courier.sendSearchOrders();
        Utils.closeInventory(player);
    }

    public static Optional<Courier> get(Player player, boolean force){
        var courier = instances.getOrDefault(player.getName().toLowerCase(), null);

        if (courier != null)
            return Optional.of(courier);

        if (!force)
            return Optional.empty();

        courier = new Courier(player);
        instances.put(player.getName().toLowerCase(), courier);

        return Optional.of(courier);
    }

    public static Courier remove(Player player){
        GPSExtension.stop(player);
        ItemsManager.take(player);

        return remove(player.getName());
    }

    public static Courier remove(String name){
        return instances.remove(name.toLowerCase());
    }

    public static class Listener extends PluginListener {

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onQuit(PlayerQuitEvent e){
            remove(e.getPlayer());
        }

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onJoin(PlayerJoinEvent e){
            remove(e.getPlayer());
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onClick(PlayerInteractEntityEvent e){
            if (!(e.getRightClicked() instanceof Player))
                return;

            var player = e.getPlayer();
            var courier = CouriersManager.get(player, false).orElse(null);

            if (courier == null || courier.getOrder() == null)
                return;

            var order = courier.getOrder();
            var clickedAt = (Player) e.getRightClicked();

            if (!order.getCustomer().getPlayer().getName().equalsIgnoreCase(clickedAt.getName()))
                return;

            if (!order.takeItemsIfHas(player)){
                order.sendCourier("deliver.no_items", true, true);
                return;
            }

            courier.stopGPS();

            order.addItems(clickedAt);
            Main.getEconomy().depositPlayer(courier.getPlayer(), order.getDeliveryPrice() + order.getItemsPrice());

            order.sendCustomer("order.end", true, true);
            order.sendCourier("deliver.end", true, true);

            CouriersManager.remove(courier.getPlayer().getName());
            ItemsManager.take(courier.getPlayer());

            order.delete();

            Bukkit.getPluginManager().callEvent(new DeliveryCompleteEvent(player, courier.getPlayer(), order));
        }

        @EventHandler
        public void onOpenShop(OpenShopEvent e){
            var p = e.getPlayer();
            var courier = CouriersManager.get(p, false).orElse(null);

            if (courier == null || courier.getOrder() == null || courier.getOrder().getShopName() == null || courier.getOrder().getState() != Order.State.COURIER_GOES_TO_STORE)
                return;

            var order = courier.getOrder();

            if (!order.getShopName().equalsIgnoreCase(e.getShop().getName()))
                return;

            order.setState(Order.State.COURIER_BUYING);

            order.sendCourier("deliver.buying", true, true);
            order.sendCustomer("order.buying", true, true);

            GPSExtension.stop(p);
        }

    }

}
