package me.wattguy.delivery.managers;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.listeners.PluginListener;
import me.wattguy.delivery.utils.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

import java.util.ArrayList;
import java.util.List;

public class ItemsManager implements Initializable {

    @Getter
    private final static List<Item> items = new ArrayList<>();

    @Override
    public void initialize() {
        items.clear();

        if (!Config.get().isSet("items"))
            return;

        for (var map : Config.get().getMapList("items")) {
            items.add((Item) new Item().fromConfig(map));
        }
    }

    public static void give(Player player){

        for (var item : items)
            item.set(player);

    }

    public static boolean can(Player player){
        for (var item : items) {
            var is = item.get(player);

            if (is != null && !is.getType().isAir())
                return false;
        }

        return true;
    }

    public static void take(Player player){
        Utils.takeItems(player.getInventory(), player, Item::is);
    }

    public static class Listener extends PluginListener {

        @EventHandler
        public void onJoin(PlayerJoinEvent e){
            take(e.getPlayer());
        }

        @EventHandler(priority = EventPriority.LOWEST)
        public void onInteract(PlayerInteractEvent e){
            if (e.getItem() == null || !Item.is(e.getItem()))
                return;

            e.setUseItemInHand(Event.Result.DENY);
        }

        @EventHandler(priority = EventPriority.LOWEST)
        public void onSwap(PlayerSwapHandItemsEvent e){
            if (!Item.is(e.getMainHandItem()) && !Item.is(e.getOffHandItem()))
                return;

            e.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onInventoryClick(InventoryClickEvent e){
            if (e.getCurrentItem() == null || !Item.is(e.getCurrentItem()))
                return;

            e.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onDrop(PlayerDropItemEvent e){
            if (!Item.is(e.getItemDrop().getItemStack()))
                return;

            e.setCancelled(true);
        }

        @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
        public void onDeath(PlayerDeathEvent e){
            take(e.getEntity());
        }

    }

}
