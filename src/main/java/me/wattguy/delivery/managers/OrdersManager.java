package me.wattguy.delivery.managers;

import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.Main;
import me.wattguy.delivery.Utils;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.listeners.PluginListener;
import me.wattguy.delivery.utils.Courier;
import me.wattguy.delivery.utils.Customer;
import me.wattguy.delivery.utils.Order;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class OrdersManager {

    public static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @Getter
    private static final Map<String, Order> customers = new HashMap<>();

    @Getter
    private static final Map<String, Order> couriers = new HashMap<>();

    @Getter
    private static final Map<String, Order> instances = new HashMap<>();

    public static void confirmOrder(Player player){
        var order = get(player, true).orElse(null);

        if (order == null || order.getState() != Order.State.BASKET)
            return;

        if (CouriersManager.get(player, false).orElse(null) != null){
            order.sendCustomer("order.you_are_courier", false, false);
            return;
        }

        if (order.getShopName() == null || order.getItems().size() <= 0 || order.getPrice() <= 0){
            order.sendCustomer("order.no_items", false, false);
            return;
        }

        if (order.isOutOfStock()){
            order.sendCustomer("order.out_of_stock", true, true);
            return;
        }

        var deliveryPrice = order.getDeliveryPriceFromLocation(player.getLocation());
        var itemsPrice = order.getPrice();
        var price = (double) itemsPrice + deliveryPrice;

        if (!Main.getEconomy().has(player, price) || !Main.getEconomy().withdrawPlayer(player, price).transactionSuccess()){
            order.sendCustomer("order.not_enough_money", false, false);
            return;
        }

        order.setDeliveryLocation(player.getLocation());
        order.setDeliveryPrice(deliveryPrice);
        order.setItemsPrice(itemsPrice);
        order.setState(Order.State.SEARCH_COURIER);

        order.sendCustomer("order.confirmed", true, false);
        Utils.closeInventory(player);

        order.sendAllCouriers();
    }

    public static void denyOrder(Player player){
        var order = get(player, true).orElse(null);

        if (order == null)
            return;

        if (order.getCourier() != null){
            order.sendCustomer("cancel.customer.cant_cancel", true, true);
        }else if (order.getState() != Order.State.BASKET){
            order.delete();
            var price = (double) order.getPrice();

            if (order.getDeliveryPrice() != null)
                price += order.getDeliveryPrice();

            Main.getEconomy().depositPlayer(player, price);
            order.sendCustomer("cancel.customer.cancelled", true, false);
        }else if (order.getCourier() == null && order.getState() == Order.State.BASKET){
            order.sendCustomer("cancel.customer.no_confirmed", false, false);
            return;
        }

        Utils.closeInventory(player);
    }

    public static List<Order> getSearchOrders(){
        return instances.values().stream().filter(order -> order.getCourier() == null && order.getState() == Order.State.SEARCH_COURIER).collect(Collectors.toList());
    }

    public static Optional<Order> get(Player p, boolean force){
        var order = OrdersManager.getByCustomer(p).orElse(null);

        if (order != null)
            return Optional.of(order);

        if (force){
            var customer = CustomersManager.get(p, true).orElse(null);
            order = OrdersManager.create(customer);
        }

        return Optional.ofNullable(order);
    }

    public static Order create(Customer customer){
        var id = randomUniqueId(6);
        var order = new Order(id, customer);

        instances.put(id.toLowerCase(), order);
        customers.put(customer.getPlayer().getName().toLowerCase(), order);

        return order;
    }

    public static void addCourierToOrder(Order order, Courier courier){
        courier.setOrder(order);
        courier.setUntil(null);

        if (Config.get().isSet("time.to_shop")) {
            courier.setUntil(System.currentTimeMillis() + (Config.get().getInt("time.to_shop") * 1000L));
        }

        order.setCourier(courier);
        couriers.put(courier.getPlayer().getName().toLowerCase(), order);
    }

    public static Optional<Order> get(String id){
        return Optional.ofNullable(instances.getOrDefault(id.toLowerCase(), null));
    }

    public static Optional<Order> getByCustomer(Player customer){
        return Optional.ofNullable(customers.getOrDefault(customer.getName().toLowerCase(), null));
    }

    public static Optional<Order> getByCourier(Player courier){
        return Optional.ofNullable(couriers.getOrDefault(courier.getName().toLowerCase(), null));
    }

    public static Order remove(String id){
        return instances.remove(id.toLowerCase());
    }

    public static String randomUniqueId(int size){
        String id;

        do {
            id = randomId(size);
        } while (instances.containsKey(id.toLowerCase()));

        return id;
    }

    public static String randomId(int size){
        StringBuilder sb = new StringBuilder();

        for (var i = 0; i < size; i++)
            sb.append(ALPHABET.charAt(Utils.rand(0, ALPHABET.length() - 1)));

        return sb.toString();
    }

    public static class Listener extends PluginListener {

        @EventHandler
        public void onQuit(PlayerQuitEvent e){
            var p = e.getPlayer();
            var order = getByCustomer(p).orElse(null);

            if (order != null) {
                order.delete();

                if (order.getCourier() != null)
                    order.courierBack();
            }

            order = getByCourier(p).orElse(null);

            if (order != null) {
                order.delete();

                order.customerBack();
            }
        }

    }

}
