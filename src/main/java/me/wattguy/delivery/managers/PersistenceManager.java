package me.wattguy.delivery.managers;

import lombok.var;
import me.wattguy.delivery.Main;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataHolder;
import org.bukkit.persistence.PersistentDataType;

/**
 * Менеджер по работе с кастомными значениями (PersistentData)
 * Дата создания: 23:35 27.10.2021
 *
 * @author WattGuy
 * @version 1.0
 */
public class PersistenceManager {

    /**
     * Достать значение
     * @param container Контейнер
     * @param type Тип поля
     * @param name Имя поля
     * @return Значение, если не найдено - null
     */
    public static <Z> Z get(PersistentDataContainer container, PersistentDataType<?, Z> type, String name){
        if (container == null)
            return null;

        var key = new NamespacedKey(Main.getInstance(), name);

        if (!container.has(key, type))
            return null;

        return container.get(key, type);
    }

    /**
     * Достать значение
     * @param holder Носитель
     * @param type Тип поля
     * @param name Имя поля
     * @return Значение, если не найдено - null
     */
    public static <Z> Z get(PersistentDataHolder holder, PersistentDataType<?, Z> type, String name){
        if (holder == null)
            return null;

        return get(holder.getPersistentDataContainer(), type, name);
    }

    /**
     * Достать значение
     * @param is Айтемстак
     * @param type Тип поля
     * @param name Имя поля
     * @return Значение, если не найдено - null
     */
    public static <Z> Z get(ItemStack is, PersistentDataType<?, Z> type, String name){
        if (is == null)
            return null;

        var meta = is.getItemMeta();

        if (meta == null)
            return null;

        return get(meta, type, name);
    }

    /**
     * Установить значение
     * @param container Контейнер
     * @param type Тип поля
     * @param name Имя поля
     */
    public static void set(PersistentDataContainer container, PersistentDataType type, String name, Object z){
        if (container == null)
            return;

        var key = new NamespacedKey(Main.getInstance(), name);

        container.set(key, type, z);
    }

    /**
     * Установить значение
     * @param holder Носитель
     * @param type Тип поля
     * @param name Имя поля
     */
    public static void set(PersistentDataHolder holder, PersistentDataType type, String name, Object z){
        if (holder == null)
            return;

        set(holder.getPersistentDataContainer(), type, name, z);
    }

    /**
     * Установить значение
     * @param is Айтемстак
     * @param type Тип поля
     * @param name Имя поля
     */
    public static void set(ItemStack is, PersistentDataType type, String name, Object z){
        if (is == null)
            return;

        var meta = is.getItemMeta();

        if (meta == null)
            return;

        set(meta, type, name, z);

        is.setItemMeta(meta);
    }

}
