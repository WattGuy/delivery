package me.wattguy.delivery.initialize;

/**
 * Интерфейс дерегистрации
 * Дата создания: 12:40 10.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public interface Unregistrable {

    /**
     * Дерегистрировать всё
     */
     void unregister();

}
