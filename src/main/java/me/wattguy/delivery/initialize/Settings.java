package me.wattguy.delivery.initialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Настройки инициализации или выключения
 * Дата создания: 12:46 10.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Settings {

    /**
     * Приоритет выполнения
     */
    Priority priority() default Priority.NORMAL;

    /**
     * Поведение включения
     */
    Behaviour behaviour() default Behaviour.MULTIPLE;

    /**
     * Нумерация приоритета, где LOWEST - выполняется самый первый, а HIGHEST - самый последний
     */
    enum Priority {
        LOWEST, LOW, NORMAL, HIGH, HIGHEST
    }

    /**
     * Нумерация поведения, где MULTIPLE - выполнять метод каждый раз, а ONCE - только однажды за включение плагина
     */
    enum Behaviour {
        MULTIPLE, ONCE
    }

}
