package me.wattguy.delivery.initialize;

import lombok.var;
import me.wattguy.delivery.Utils;

import java.util.*;

/**
 * Менеджер инициализации
 * Дата создания: 12:43 10.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public class InitializeManager {

    /**
     * Выполненные классы
     */
    private static final Map<String, Set<Class<?>>> done = new HashMap<>();

    /**
     * Получить список выполненных классов по уникальному ключу
     * @param key Уникальный ключ
     * @return Список выполненных классов, если нет - создаёт новый и заносит его
     */
    private static Set<Class<?>> getDone(String key){
        var set = done.getOrDefault(key.toLowerCase(), null);

        if (set != null)
            return set;

        set = new HashSet<>();
        done.put(key.toLowerCase(), set);

        return set;
    }

    /**
     * Инициализация всех инициализируемых классов
     */
    public static void initialize() {
        var map = new HashMap<Settings.Priority, List<Class<? extends Initializable>>>();

        for (var c : getInitializableClasses()) {
            var priority = getPriority(c, "initialize");

            if (map.containsKey(priority)){
                var list = map.get(priority);

                if (!list.contains(c))
                    list.add(c);
            }else{
                var list = new ArrayList<Class<? extends Initializable>>();

                list.add(c);

                map.put(priority, list);
            }
        }

        for(int i = 0; i < Settings.Priority.values().length; i++){
            var list = map.getOrDefault(Settings.Priority.values()[i], null);

            if (list == null)
                continue;

            for (var c : list) {
                initialize(c);
            }
        }
    }

    /**
     * Получить все инициализируемые классы
     * @return Все инициализируемые классы
     */
    public static Set<Class<? extends Initializable>> getInitializableClasses(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(InitializeManager.class.getPackage().getName(), 1), Initializable.class);

    }

    /**
     * Инициализировать класс
     * @param c Класс
     */
    public static void initialize(Class<? extends Initializable> c){
        var behaviour = getBehaviour(c, "initialize");

        if (behaviour == Settings.Behaviour.ONCE && getDone("initialize").contains(c))
            return;

        try{
            var instance = c.getConstructor().newInstance();
            c.getMethod("initialize").invoke(instance);
        }catch(Exception ignored) {}

        if (behaviour == Settings.Behaviour.ONCE)
            getDone("initialize").add(c);
    }

    /**
     * Дерегистрация всех дерегистрируемых классов
     */
    public static void unregister() {
        var map = new HashMap<Settings.Priority, List<Class<? extends Unregistrable>>>();

        for (var c : getUnregistrableClasses()) {
            var priority = getPriority(c, "unregister");

            if (map.containsKey(priority)){
                var list = map.get(priority);

                if (!list.contains(c))
                    list.add(c);
            }else{
                var list = new ArrayList<Class<? extends Unregistrable>>();

                list.add(c);

                map.put(priority, list);
            }
        }

        for(int i = 0; i < Settings.Priority.values().length; i++){
            var list = map.getOrDefault(Settings.Priority.values()[i], null);

            if (list == null)
                continue;

            for (var c : list) {
                unregister(c);
            }
        }

    }

    /**
     * Дерегистрировать класс
     * @param c Дерегистрируемый класс
     */
    public static void unregister(Class<? extends Unregistrable> c){
        var behaviour = getBehaviour(c, "unregister");

        if (behaviour == Settings.Behaviour.ONCE && getDone("unregister").contains(c))
            return;

        try{
            var instance = c.getConstructor().newInstance();
            c.getMethod("unregister").invoke(instance);
        }catch(Exception ignored) {}

        if (behaviour == Settings.Behaviour.ONCE)
            getDone("unregister").add(c);
    }

    /**
     * Получить приоритет метода в классе
     * @param c Класс
     * @param method Имя метода
     * @return Приоритет, если не найден - NORMAL
     */
    private static Settings.Priority getPriority(Class<?> c, String method){
        try{
            var p = c.getMethod(method).getAnnotation(Settings.class);

            if (p != null)
                return p.priority();
        }catch(Exception ignored) {}

        return Settings.Priority.NORMAL;
    }

    /**
     * Получить поведение метода в классе
     * @param c Класс
     * @param method Имя метода
     * @return Поведение, если не найден - MULTIPLE
     */
    private static Settings.Behaviour getBehaviour(Class<?> c, String method){
        try{
            var p = c.getMethod(method).getAnnotation(Settings.class);

            if (p != null)
                return p.behaviour();
        }catch(Exception ignored) {}

        return Settings.Behaviour.MULTIPLE;
    }

    /**
     * Получить все дерегистрируемые классы
     * @return Все дерегистрируемые классы
     */
    public static Set<Class<? extends Unregistrable>> getUnregistrableClasses(){

        return Utils.getAllClassesInPackage(Utils.getParentPackage(InitializeManager.class.getPackage().getName(), 1), Unregistrable.class);

    }

}
