package me.wattguy.delivery.initialize;

/**
 * Интерфейс инициализации
 * Дата создания: 12:40 10.01.2022
 *
 * @author WattGuy
 * @version 1.0
 */
public interface Initializable {

    /**
     * Инициализировать всё
     */
    void initialize();

}
