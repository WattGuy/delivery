package me.wattguy.delivery;

import com.tcoded.folialib.FoliaLib;
import lombok.Getter;
import lombok.var;
import me.wattguy.delivery.initialize.InitializeManager;
import me.wattguy.delivery.utils.Pool;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

/**
 * Главный класс-плагин
 * @author WattGuy
 * @version 1.0
 */
public class Main extends JavaPlugin {

    public static void main(String[] args){



    }

    /**
     * Синхронный пул с периодом 25мс (0.5tick) + задержка запуска 1tick = ~1.5tick
     */
    @Getter
    public static Pool<Runnable> syncPool = null;

    /**
     * Экземпляр класса
     */
    @Getter
    private static Main instance = null;

    /**
     * Экономика Vault
     */
    @Getter
    private static Economy economy = null;

    @Getter
    private static FoliaLib foliaLib;

    /**
     * Инициализация при включении плагина
     */
    @Override
    public void onEnable(){
        // Заносим инстанс
        instance = this;
        foliaLib = new FoliaLib(this);

        syncPool = new Pool<>(1L, 1L, false, rs -> {
            if (rs.size() <= 0)
                return;

            for (var r : new ArrayList<>(rs)) {
                r.run();
            }
        });

        // Заносим экономику Vault
        economy = getServer().getServicesManager().getRegistration(Economy.class).getProvider();

        // Инициализируем
        InitializeManager.initialize();
    }

    /**
     * Деинициализация при выключении плагина
     */
    @Override
    public void onDisable(){

        // Дерегистрируем
        InitializeManager.unregister();

    }

}
