package me.wattguy.delivery.extensions;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import me.clip.placeholderapi.PlaceholderAPI;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlaceholderAPIExtension implements Initializable, Unregistrable {

    /**
     * Включено или выключено дополнение по замене переменных
     */
    @Getter
    @Setter
    private static boolean enabled = false;

    @Override
    @Settings(priority = Settings.Priority.LOWEST, behaviour = Settings.Behaviour.ONCE)
    public void initialize() {

        // Инициализируем дополнение для PlaceholderAPI
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            PlaceholderAPIExtension.setEnabled(true);
        }

    }

    @Override
    public void unregister() {
        // Выключаем дополнение на обработку переменных PlaceholderAPI
        PlaceholderAPIExtension.setEnabled(false);
    }

    /**
     * Замена переменных по PlaceholderAPI
     * @param p Игрок
     * @param input Входящая строка
     * @return Выходная строка
     */
    public static String replace(Player p, String input){
        if (!enabled)
            return input;

        return PlaceholderAPI.setPlaceholders(p, input);
    }

    /**
     * Замена переменных по PlaceholderAPI для массива
     * @param p Игрок
     * @param input Входящий массив
     * @return Выходящий массив
     */
    public static List<String> replace(Player p, List<String> input){
        if (!enabled)
            return input;

        List<String> result = new ArrayList<>();

        for (var s : input) {
            result.add(replace(p, s));
        }

        return result;
    }

}
