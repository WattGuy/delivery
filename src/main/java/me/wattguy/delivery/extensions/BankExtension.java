package me.wattguy.delivery.extensions;

import lombok.Getter;
import lombok.Setter;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import ua.abcik.RPMBank.Main;

public class BankExtension implements Initializable, Unregistrable {

    @Getter
    @Setter
    private static boolean enabled = false;

    @Override
    @Settings(priority = Settings.Priority.LOWEST, behaviour = Settings.Behaviour.ONCE)
    public void initialize() {
        setEnabled(Bukkit.getPluginManager().isPluginEnabled("RPMBank"));
    }

    @Override
    public void unregister() {
        setEnabled(false);
    }

    public static boolean has(OfflinePlayer player, double d){
        if (!enabled || player == null || d < 0)
            return false;

        return Main.getInstance().bankManager.getBalance(player.getName()) >= (int) d;
    }

    public static void withdraw(OfflinePlayer player, double d){
        if (!enabled || player == null || d < 0)
            return;

        Main.getInstance().bankManager.takeBalanceByAdmin(player.getName(), (int) d);
    }

    public static void deposit(OfflinePlayer player, double d){
        if (!enabled || player == null || d < 0) {
            return;
        }

        Main.getInstance().bankManager.addBalanceByAdmin(player.getName(), (int) d);
    }

}
