package me.wattguy.delivery.extensions;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.delivery.initialize.Settings;
import me.wattguy.delivery.initialize.Unregistrable;
import me.wattguy.delivery.managers.CouriersManager;
import me.wattguy.delivery.utils.Order;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;
import ua.abcik.RPMGPS.Main;
import ua.abcik.RPMGPS.config.sections.utils.SimpleItem;
import ua.abcik.RPMGPS.data.Destination;
import ua.abcik.RPMGPS.events.ArriveToDestinationEvent;

import java.util.Arrays;
import java.util.UUID;

public class GPSExtension implements Initializable, Unregistrable {

    @Getter
    @Setter
    private static boolean enabled = false;

    @Getter
    private static Listener listener = null;

    @Override
    @Settings(priority = Settings.Priority.LOWEST, behaviour = Settings.Behaviour.ONCE)
    public void initialize() {
        GPSExtension.setEnabled(Bukkit.getPluginManager().isPluginEnabled("RPMGPS"));

        if (isEnabled()){
            listener = new Listener();
            Bukkit.getPluginManager().registerEvents(listener, me.wattguy.delivery.Main.getInstance());
        }
    }

    @Override
    public void unregister() {
        GPSExtension.setEnabled(false);

        if (listener != null)
            HandlerList.unregisterAll(listener);

        listener = null;
    }

    public static void gps(Player player, Location location, String name, boolean light){
        if (!enabled)
            return;

        var destination = new Destination(
                UUID.randomUUID().toString(),
                name,
                new SimpleItem(
                        Material.PLAYER_HEAD,
                        "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDVjMGNkNzE3Y2E5MjI4ODQ5NjUyYThjY2RmNTI4MWE4NjBjYjJlNzk2NDZiNTZhMjJiYzU5MTEwZjM2MWRhIn19fQ==",
                        "§f" + name,
                        Arrays.asList()
                ),
                location.getX(),
                location.getY(),
                location.getZ()
        );

        destination.light = light;

        Main.getInstance().gpsManager.go(player, destination);
    }

    public static void stop(Player player){
        if (!enabled)
            return;

        ua.abcik.RPMGPS.Main.getInstance().gpsManager.stop(player);
    }

    public static class Listener implements org.bukkit.event.Listener {

        @EventHandler
        public void onArrive(ArriveToDestinationEvent e){
            var p = e.getPlayer();
            var courier = CouriersManager.get(p, false).orElse(null);

            if (courier == null || courier.getOrder() == null || courier.getOrder().getShopName() == null || courier.getOrder().getState() != Order.State.COURIER_GOES_TO_STORE)
                return;

            var order = courier.getOrder();
            var shopLocation = RegionShopsExtension.getShopLocation(order.getShopName()).orElse(null);

            if (shopLocation == null)
                return;

            var vector = new Vector(e.getDestination().x, e.getDestination().y, e.getDestination().z);

            if (!(vector.getBlockX() == shopLocation.getBlockX() && vector.getBlockY() == shopLocation.getBlockY() && vector.getBlockZ() == shopLocation.getBlockZ()))
                return;

            order.setState(Order.State.COURIER_BUYING);

            order.sendCourier("deliver.buying", true, true);
            order.sendCustomer("order.buying", true, true);

            GPSExtension.stop(p);
        }

    }

}
