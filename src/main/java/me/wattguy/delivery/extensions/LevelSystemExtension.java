package me.wattguy.delivery.extensions;

import lombok.Getter;
import me.wattguy.delivery.initialize.Initializable;
import me.wattguy.levelsystem.infos.AbstractLevelInfo;
import me.wattguy.levelsystem.managers.InfoManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class LevelSystemExtension implements Initializable {

    @Getter
    private static boolean enabled = false;

    @Override
    public void initialize() {
        enabled = Bukkit.getPluginManager().isPluginEnabled("RPMLevelSystem");
    }

    public static int getLevel(OfflinePlayer player, int def){
        if (!enabled || player == null)
            return def;

        AbstractLevelInfo li = InfoManager.get(player.getName());

        if (li == null)
            return def;

        return li.getCurrentLevel();
    }

}

