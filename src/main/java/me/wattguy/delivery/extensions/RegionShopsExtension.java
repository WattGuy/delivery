package me.wattguy.delivery.extensions;

import lombok.var;
import me.wattguy.delivery.configs.list.Config;
import me.wattguy.delivery.utils.CachedValue;
import org.bukkit.Location;
import ru.lemar98.regionshops.Main;
import ru.lemar98.regionshops.database.ShopFile;
import ru.lemar98.regionshops.model.Shop;

import java.io.File;
import java.util.*;

public class RegionShopsExtension {

    public static CachedValue<Void, Map<String, Shop>> shopsCached = new CachedValue<>(1000L, unused -> {
        Map<String, Shop> list = new HashMap<>();

        for (var shopFile : getShopFiles()) {
            var shop = shopFile.getShop();

            if (shop == null || shop.getSellItems().size() <= 0)
                continue;

            list.put(shop.getName().toLowerCase(), shop);
        }

        return list;
    });

    public static Optional<Location> getShopLocation(String shopName){
        if (shopName == null)
            return Optional.empty();

        var shop = getShop(shopName).orElse(null);

        if (shop == null)
            return Optional.empty();

        return Optional.ofNullable(shop.getLocation());
    }

    public static double getDeliveryPrice(String shopName, Location from){
        if (shopName == null || from == null)
            return 0d;

        var shop = getShop(shopName).orElse(null);

        if (shop == null)
            return 0d;

        if (!from.getWorld().getName().equalsIgnoreCase(shop.getLocation().getWorld().getName()))
            return Double.MAX_VALUE;

        return shop.getLocation().distance(from) * Config.get().getDouble("delivery.price", 0d);
    }

    public static Optional<Shop> getShop(String id){
        if (id == null)
            return Optional.empty();

        return Optional.ofNullable(getShops().getOrDefault(id.toLowerCase(), null));
    }

    public static Map<String, Shop> getShops(){
        return shopsCached.getValue(null);
    }

    public static List<ShopFile> getShopFiles(){
        List<ShopFile> list = new ArrayList<>();

        for (var shopName : getShopNames())
            list.add(ShopFile.get(shopName, true));

        return list;
    }

    public static List<String> getShopNames(){
        List<String> list = new ArrayList<>();

        var database = new File(Main.getInstance().getDataFolder() + "/database/");
        var files = database.listFiles();

        if (files == null)
            return list;

        for (var file : files) {
            var name = file.getName();

            if (!name.contains(".yml"))
                continue;

            list.add(name.replace(".yml", ""));
        }

        return list;
    }

}
