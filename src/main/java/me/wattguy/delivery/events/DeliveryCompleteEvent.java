package me.wattguy.delivery.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.wattguy.delivery.utils.Order;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@RequiredArgsConstructor
public class DeliveryCompleteEvent extends Event {

    private final Player to;

    private final Player courier;

    private final Order order;

    // BUKKIT

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    // /BUKKIT
}
